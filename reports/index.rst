:orphan:

.. Include this file only when generating all reports, otherwise skip it.
   See conf.py "master_doc" and "exclude_patterns" variables.
.. If this file is used it means that each directory (thesis, statusreports,
   etc) can be considered to be a part. Check the "latex_use_parts" and
   "html_use_parts" variables of conf.py.


Collection of thesis reports
============================


Contents:

.. toctree::
   :maxdepth: 3
   :numbered:

   papers/index
   presentations/index
   statusreports/index
   thesis/index
   glossary
