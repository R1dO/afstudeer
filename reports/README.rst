===================
Report source files
===================
This folder hold the source files (e.g. ``*.lyx``, ``*.bib``, etc) for
presentable documents like reports and presentations.

Each document type folder will contain the configuration files to allow building
of the documents using ``Sphinx`` suitable for online presentation.

Folder structure
----------------

+----------------------+---------------------------------------------------------+
| Folders              | Contents                                                |
+======================+=========================================================+
| ``images/``          | Stores all the images used in the documents.            |
|                      | It is intended to serve as a linking farm. Both         |
|                      | external repositories and internal image folders will   |
|                      | link to images inside this folder.                      |
+----------------------+---------------------------------------------------------+
| ``papers/``          | OK \.. if you are up to it, this is where those         |
|                      | documents will come into existence shouting:            |
|                      |                                                         |
|                      |    **Here I am ...**                                    |
|                      |         **Look at my fabulous research**                |
+----------------------+---------------------------------------------------------+
| ``presentations/``   | Files which create a nice slide show.                   |
+----------------------+---------------------------------------------------------+
| ``resources/``       | External documents and the master ``bibtex`` library    |
|                      | file.                                                   |
|                      |                                                         |
|                      | =========================  ============================ |
|                      | Folders and files          Contents                     |
|                      | =========================  ============================ |
|                      | ``books/``                 Textbooks                    |
|                      | ``papers/``                Research papers              |
|                      | ``etc/``                   Anything else                |
|                      | ``citations_library.bib``  Thesis citation library      |
|                      | =========================  ============================ |
|                      |                                                         |
+----------------------+---------------------------------------------------------+
| ``statusreports/``   | Files for intermediate progress reports                 |
+----------------------+---------------------------------------------------------+
| ``thesis/``          | An external repository (**submodule**) that contains    |
|                      | all files needed to create the thesis document.         |
|                      |                                                         |
|                      | Expect a considerable amount of time to be spend here.  |
+----------------------+---------------------------------------------------------+

=================================  ==============================================
Scripts                            Description
=================================  ==============================================
``extract_resources.sh``           Create the resources folder and extract its
                                   contents from the cloud archives folder under:

                                   ``//data/published/distributed/...``

``create_resources_archive.sh``    Generate a backup of the resources folder.
=================================  ==============================================

.. note:: Those scripts are symlinks to a target folder of the parent
          repository afstudeer_ since they are deemed to only be usefull
          when this repository is used as a child of that repository.

Image handling
--------------
When needed a folder named images will be put at the same location of the 
calling source file. This folder wil link to the opropriate folde under
the first level of this repository.
Contrary to the case before migrating to a standalone repository
all figures will now be stored here and act as symlink targets for the
specific folderis inside ``//data/`` from the parent repo (since it is
possible to regenerate the images from within that repository, which is not
possible from within this one).

Notes on specific image types are in the next subsections.

Vector images
+++++++++++++
* Use Scalable Vector Graphics ``.svg``.
* If needed they can be post-processed by the program ``inkscape``.

Raster images
+++++++++++++
* Prefer usage of ``.png``.
* Only use ``.tiff`` when required.

.. Hyperlinks
.. _afstudeer: https://bitbucket.org/R1dO/afstudeer
