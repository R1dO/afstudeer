A robust dairy based research environment.
==========================================
Although not directly related to the research project this section is still
deemed important enough to be included here. On the one hand since it explains a
bit how i managed to get back on track (by restoring faith) and on the other
hand this idea might help others setting up similar environments or those
willing to use this project as a starting point for further research.

The idea behind this setup was to devise a work environment/ethos making it
almost impossible to lose previous work while simultaniously provide the ability
to be indifferent regarding who is working at it and the location where work is
performed.

The main goals are:

* Access must be available to the complete research history (all documents and
  data) regardless of the location.
* When needed it must be possible to give full access to anyone interested
  in the research.

The first goal is to prevent loss of previous work. While the second goal is
aimed to facilitate those that might be interested in the research due too one
of the following reasons.

* Progress tracking
* Reusing (part) of the work.
* Check results/work and comment on them.
* Collaborate on a part of the work.

Implementation
--------------
To meet the goals a combination of (Decentralized) Version Control Systems
**(D)VCS** ``git`` and cloud storage is used as it was deemed to be the most
effective combination.

The (D)VCS is used to store most of the research results (small data [#]_ and
writing) and tools which are usefull for a participant (scripts and possibly
program code).
The benefit of a repository storage approach is that it provides the ability to
easily navigate to any previous point in time (it acts like a sort of diary).
Since it is easy to transfer a repository, not only can one become indifferent
to working location it also allows easy sharing of the full research.

Cloud storage is used to store (potential huge) datasets. The reason for keeping
large amounts of data outside a repository is 2 fold.

#. Huge (binary) data is not well suited for storage in a repository
   (efficiency an speed considerations)
#. It gives the possibility to retract (or limit) access to part of the data
   (for instance if it is not certain that some of the original data can be
   redistributed).

.. note::
   Currently access to both the repositories (2 of them) and data from the cloud
   provider are invite only. In order to keep some control.

.. [#] When large data is stored outside the (D)VCS repository, it is good
       practice to provide scripts in order to obtain the data and put them in
       the correct location.
