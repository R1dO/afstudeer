General information on the LST dataset.
=======================================
Here we show information of the dataset. This includes:

*  Average temperatures.
*  Temperature distributions
*  Missing images
*  Gap percentages
*  Maximum duration of continuous gaps
*  Maximum duration of continuous signal
*  etc.

All plots are shown only for the month of November 2008 and for the year 2008.
Although data is available to plot them for different months (and years) for
the period January 2008 until October 2010 [1]_.

Please note that in all the plots outliers (for example 30K and 500K :term:`LST`
temperatures) were not discarded before calculating any (derivative) values.

Temperature of the study area
-----------------------------
To get an overview of the temperatures to be expected we start with the average
temperature (missing values were discarded from the calculation), see
:numref:`fig:LST-histogram-2008` until :numref:`fig:LST-log-histogram-2008-11`.
Those figures shows clearly that there are 2 distinct sets of temperatures
present. We have a large area on the southwestern part of the study area where
the :term:`LST` is substantially larger than the (below zero degrees Celsius)
part of the dataset which should be of more interest since that contains the
highlands.

.. _`fig:mean-LST-values-2008`:
.. figure:: images/lstMean2008-all.*

   Mean LST over study area for the year 2008

.. _`fig:mean-LST-values-2008-11`:
.. figure:: images/lstMean2008-11.*

   Mean LST over study area for November 2008

.. _`fig:LST-histogram-2008`:
.. figure:: images/lstHistogram2008-all.*

   LST histogram for the year 2008

.. _`fig:LST-histogram-2008-11`:
.. figure:: images/lstHistogram2008-11.*

   LST histogram for November 2008

.. _`fig:LST-log-histogram-2008`:
.. figure:: images/lstLogHistogram2008-all.*

   LST logarithmic histogram for the year 2008

.. _`fig:LST-log-histogram-2008-11`:
.. figure:: images/lstLogHistogram2008-11.*

   LST logarithmic histogram for November 2008


The 2 distinct areas do can also be clearly seen in the normal histograms of
:numref:`fig:allowed_range_210_320` and
:numref:`fig:allowed_range_220_310` which show that the temperature
distribution has 2 distinct peaks, one just below zero degrees Celsius and the
other one well above. The histogram for the year 2008 suggest that the
temperature range is in between 220K and 340K. The logarithmic histograms are
shown to support the notion that we can expect quite some outlier LST values to
be present in our dataset. [2]_

Extend of missing pixels per time-point
---------------------------------------
To determine the spatial extend of missing values
:numref:`fig:percentage-missing-pixels-2008` and
:numref:`fig:percentage-missing-pixels-2008-11`. show the percentage of missing
pixels per time-point. For November this ranges from 10.4 until 62.6%, while for
the complete year of 2008 ranges from 1.4% until 95.7% (not counting the missing
images). These images seem to suggest that the amount of missing data follow
some periodic trend (daily?).

.. _`fig:percentage-missing-pixels-2008`:
.. figure:: images/missingPixelsPercentage2008-all.*

   Percentage of missing pixels per time-point for the year 2008

.. _`fig:percentage-missing-pixels-2008-11`:
.. figure:: images/missingPixelsPercentage2008-11.*

   Percentage of missing pixels per time-point for November 2008

The yearly plot show evidence of missing images (100% missing pixels). Since
their extend can not be easily grasped by looking at the figures
:numref:`tab:missing-images` is provided which summarizes the number of missing
images for the complete dataset we had at our disposal.

It is from this table that it is advisable to change the focus from January 2008
towards November 2008 in order to asses the procedures for filling gaps in the
time-series, since this is the only month were all images are present.

The existence of missing images (which show up as 100% in the yearly plot)
combined with the large amount of time points having a high percentage of
missing pixels is a strong indicator that a spatial information only
gap-filling mechanism (e.g. per time-point filling complete images) will not be
able to complete estimate the values of the missing data-values sufficiently in
an equally sampled (time domain) reconstruction.

.. _`tab:missing-images`:
.. table:: Missing images

   =========  ====  ====  =================
   Period     2008  2009  2010
   =========  ====  ====  =================
   January    1     2     1
   February   14    54    4
   March      65    66    60
   April      108   18    28
   May        3     1     1
   June       1     4     0
   July       43    1     17
   August     9     15    9
   September  70    83    61
   October    37    29    71 [3]_
   November   0     6     No data available
   December   2     9     No data available
   Total      353   288   252 + 1464
   =========  ====  ====  =================

Extend of missing time-points per channel (pixel)
-------------------------------------------------
In order to see the severeness of the number of missing observations per
time-series the percentage with respect to the total number of observations is
shown in :numref:`fig:percentage-missing-timepoints-2008` and
:numref:`fig:percentage-missing-timepoints-2008-11` for the whole study-area.
For November this ranges from 0 until 78.9%, while for the complete year of
2008 ranges from 14.8% until 77.0%.

.. _`fig:percentage-missing-timepoints-2008`:
.. figure:: images/missingTimepointsPercentage2008-all.*

   Percentage of missing time points per pixel for the year 2008

.. _`fig:percentage-missing-timepoints-2008-11`:
.. figure:: images/missingTimepointsPercentage2008-11.*

   Percentage of missing time points per pixel for November 2008

Maximum continuous signal and gap lengths
-----------------------------------------
To get an understanding of the worst case gap and best case signal duration
:numref:`fig:max-continuous-gap-2008` until
:numref:`fig:max-continuous-signal-2008-11` show the maximum encountered
gap/signal length per pixel.

For November 2008 the maximum continuous signal length ranges from 9 hours
(1.25%) until 720 hours (100%), the maximum continuous gap length ranges from 0
hours (0%) until 347 hours (48.19%). For the complete year of 2008 the maximum
continuous signal length ranges from 24 hours (0.27%) until 997 hours (11.35%),
the maximum continuous gap length ranges from 79 hours (0.90%) until 410 hours
(4.70%).

.. _`fig:max-continuous-gap-2008`:
.. figure:: images/maxGapLength2008-all.*

   Maximum continuous Gap duration (in hours) for the year 2008

.. _`fig:max-continuous-gap-2008-11`:
.. figure:: images/maxGapLength2008-11.*

   Maximum continuous Gap duration (in hours) for November 2008

.. _`fig:max-continuous-signal-2008`:
.. figure:: images/maxSignalLength2008-all.*

   Maximum continuous signal duration (in hours) for the year 2008

.. _`fig:max-continuous-signal-2008-11`:
.. figure:: images/maxSignalLength2008-11.*

   Maximum continuous signal duration (in hours) for November 2008


Number of gaps (and continuous signal) per pixel
------------------------------------------------
Another interesting statistic regarding the distribution of gaps (or signal
fragments) is how much of them there are in the time-series. It is observed
that the maximum number of gaps (and signal fragments) equals\
:math:`\lceil \frac{N}{2} \rceil`, where
:math:`N=Number\,of\,timepoints`, the percentage values will be given in
relation to this theoretical maximum (360 for November 2008, and 4392 for the
year 2008). Since the amount of gaps and the amount of signal fragments can
only deviate by +1 or -1 only the first is presented.
:numref:`fig:number-of-gaps-2008` and :numref:`fig:number-of-gaps-2008-11`
show the distribution of the number of gaps over the entire study-area.

.. _`fig:number-of-gaps-2008`:
.. figure:: images/amountGaps2008-all.*

   Number of gaps per pixel for the year 2008. The theoretical maximum is 4392.

.. _`fig:number-of-gaps-2008-11`:
.. figure:: images/amountGaps2008-11.*

   Number of gaps per pixel for November 2008. The theoretical maximum is 360.

For November 2008 the number of gaps ranges from 0 (0%) until 138 (38.33%). For
the complete year of 2008 the number of gaps ranges from 371 (8.45%) until 1113
(25.34%).

Average gap and signal length per pixel
---------------------------------------
Combining the number of gaps with the total number of missing time points gives
the average gap length. This gives an indication of expected cloud duration. In
the same manner the average signal length can be computed, this should provide
some insight what the largest signal can be that has full sampling support.
:numref:`fig:average-gap-duration-2008` until
:numref:`fig:average-signal-duration-2008-11` [4]_ show those average gap and
signal lengths for the complete study-area. The importance of this number is
that signals with a periodicity smaller than the average gap length will be
difficult to reconstruct using only time based reconstruction methods.

.. _`fig:average-gap-duration-2008`:
.. figure:: images/averageGapLength2008-all.*

   Average gap duration (in hours) for the year 2008

.. _`fig:average-gap-duration-2008-11`:
.. figure:: images/averageGapLength2008-11.*

   Average gap duration (in hours) for November 2008

.. _`fig:average-signal-duration-2008`:
.. figure:: images/averageSignalLength2008-all.*

   Average signal duration (in hours) for the year 2008

.. _`fig:average-signal-duration-2008-11`:
.. figure:: images/averageSignalLength2008-11.*

   Average signal duration (in hours) for November 2008

For November 2008 the average gap ranges from 0 [5]_ hours until 27.89 hours,
the average signal duration ranges from 2.31 hours until 720 hours. For the
complete year of 2008 the average gap ranges from 2.65 hours until 15.77 hours
and the average signal duration ranges from 3.29 hours until 19.54 hours.

Raw estimation of outlier values
--------------------------------
Using the histograms of :numref:`fig:allowed_range_210_320` and
:numref:`fig:allowed_range_220_310` crude bounds for acceptable
temperature ranges can be obtained, to check if a limit is to stringent the
plots were used to visualize that the accepted limits of 210 K and 310 K
degrees does not lead to discarding connected areas.

.. _`fig:allowed_range_210_320`:
.. figure:: images/allowed_range_210-320.*

   Outliers spatial distribution for given temperature limits of 210 K and 320 K

.. _`fig:allowed_range_220_310`:
.. figure:: images/allowed_range_220-310.*

   Outliers spatial distribution for given temperature limits of 220 K and 310 K

Local time of extrema temperatures.
-----------------------------------
Since there is a time difference over the dataset between 2,5 hours (bottom
part) and 3 hours (top part) [6]_, it was expected that this would be visible
when plotting the time of maximum temperature per pixel. Using a fully
reconstructed (by SSA) time-series as seen in
:numref:`fig:average-time-of-max-LST-2008-11` until
:numref:`fig:row200_profiles_of_extrema_temperatures_2008-11`.
This clearly shows up for the maximum temperature, unfortunately the minimum
temperature is less accurate with this respect. A possible explanation is that
warming is correlated with incoming solar radiation, while cooling is more
influenced by other effects.

.. _`fig:average-time-of-max-LST-2008-11`:
.. figure:: images/average_time_of_max_LST_2008-11.*

   Average time of maximum LST for November 2008

.. _`fig:average-time-of-min-LST-2008-11`:
.. figure:: images/average_time_of_min_LST_2008-11.*

   Average time of minimum LST for November 2008

.. _`fig:average-time-of-max-LST-2008-11-10`:
.. figure:: images/average_time_of_max_LST_2008-11-10.*

   Average time of maximum LST for 10 November 2008

.. _`fig:average-time-of-min-LST-2008-11-10`:
.. figure:: images/average_time_of_min_LST_2008-11-10.*

   Average time of minimim LST for 10 November 2008

.. _`fig:row200_profiles_of_extrema_temperatures_2008-11`:
.. figure:: images/row200_profiles_2008-11.*

   Time of extrema temperatures profiles for November 2008.
   Using row 200.

.. note::
   In a previous version of this document the expected behavior did not show up in
   these plots. This was caused by the usage of the original dataset, the presence
   of gaps obscured the true max/min times. In effect this meant that any time
   could become a min/max depending on the gap distribution.

.. [1] With the exception of the last day of October 2010.
.. [2] Note that during the creating of those histograms all values below 100K
       and above 400K were already cast into the first and last bins.
.. [3] This number includes the last missing day.
.. [4] See :numref:`fig:average-signal-duration-2008-11-log` for a variant of
       :numref:`fig:average-signal-duration-2008-11`.
.. [5] To calculate this number 2.2251e-308 (smallest number allowed) was added
       to the number of gaps to prevent division by zero.
.. [6] Crude estimation based on the longitudinal differences between the image
       corner points defined in the header files.
