Gap filling
===========
Tests related to gap-filling (those that were not lost)

HANTS
------
Using a single pixel time-series (coordinates row 284, column 301) of the input
dataset with a length of 25 days and a minimum of gaps see
:numref:`fig:HANTS-cap_temp_diff` and :numref:`fig:HANTS-cap_temp_corr`
demonstrate the capabilities of :term:`HANTS`.

.. _`fig:HANTS-cap_temp_diff`:
.. figure:: images/HANTS_capability_temperature_differences.*

   HANTS 25 day reconstruction temperature differences, small amount of gaps.

.. _`fig:HANTS-cap_temp_corr`:
.. figure:: images/HANTS_capability_temperature_correlation.*

   HANTS 25 day reconstruction temperature correlation, small amount of gaps.


SSA
---
Singular Spectral Analysis reconstructed images.

First the influence of the two most important parameters are shown, followed by
two plots showing the reconstruction results for 2 cases of gaps.

Influence of parameters
^^^^^^^^^^^^^^^^^^^^^^^
Two parameters have the most influence on the reconstruction results:

*  The window size used for creating the trajectory matrix.

   * Defines the largest period that can be captured.
   * Gives rise to ’jumps’ in the reconstructed signal.
*  The number of eigenvalues used in the reconstruction.

   * A higher number means more noise included.

To demonstrate those effects a time-series (without gaps) with a sampling
interval of 10 minutes was used, see :numref:`fig:rec_200801_s7w432` until
:numref:`fig:rec_200801_s14w1750`.

.. _`fig:rec_200801_s7w432`:
.. figure:: images/2008-01-rec-s7w432.*

   Reconstruction results using 7 components with a 3 day window.

.. _`fig:rec_200801_s7w1750`:
.. figure:: images/2008-01-rec-s7w1750.*

   Reconstruction results using 7 components with a 12,15 day (2/5 of a month) window.

.. _`fig:rec_200801_s14w432`:
.. figure:: images/2008-01-rec-s14w432.*

   Reconstruction results using 14 components with a 3 day window.

.. _`fig:rec_200801_s14w1750`:
.. figure:: images/2008-01-rec-s14w1750.*

   Reconstruction results using 14 components with a 12,15 day (2/5 of a month) window.

Not show here is that when the number of components (eigenvalues) equal the
window size, the reconstructed signal resembles the original one
(:math:`\epsilon \sim 6 \cdot 10^{-4} K`).

Reconstruction results for two single channel cases.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The reconstruction of a single pixel are shown for the following 2 cases, based
on the LST hourly dataset.

*  The pixel with the most amount of gaps in the time-range.
*  The pixel with the least amount of gaps in the time-range.

.. _`fig:rec_compare_min_gap_channel`:
.. figure:: images/SingleChannel_Min_Kol529_comparison.*

   Reconstruction results for the channel with minimum of gaps.

.. _`fig:rec_correlation_min_gap_channel`:
.. figure:: images/SingleChannel_Min_Kol529_correlation.*

   Reconstruction correlation for the channel with minimum of gaps.

.. _`fig:rec_compare_max_gap_channel`:
.. figure:: images/SingleChannel_Max_Kol708_comparison.*

   Reconstruction results for the channel with maximum of gaps.

.. _`fig:rec_correlation_max_gap_channel`:
.. figure:: images/SingleChannel_Max_Kol708_correlation.*

   Reconstruction correlation for the channel with maximum of gaps.
