****************************************
Recap of what has been done up until now
****************************************
A series of pages mostly with images, that presents the things done since the
beginning of my master thesis. The general location of the dataset is shown in
:numref:`fig-StudyArea`.


.. _`fig-StudyArea`:
.. figure:: images/GoogleMaps_dataset.*
   :alt: General location of study area
   :width: 50%

   General location of study area

.. toctree::
   :maxdepth: 2

   sec-dataset_information
   sec-gap_filling
   sec-research_environment
