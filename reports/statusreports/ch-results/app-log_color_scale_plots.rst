Logarithmic color scale plots
=============================
For some plots readability can be enhanced if we don’t use the standard linear
color scaling. This is especially useful for plots where the colorbar spans a
large domain, while most information is contained in only a few parts as is the
case in :numref:`fig:average-signal-duration-2008-11-log`.

.. _`fig:average-signal-duration-2008-11-log`:
.. figure:: images/averageSignalLengthLog2008-11.*

   Average signal duration (in hours) for november 2008 using a logarithmic
   color scale.
