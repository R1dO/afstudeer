#############
Status report
#############

Contents:

.. toctree::
   :maxdepth: 1

   ch-introduction/research_ideas
   ch-results/index
   appendices/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

