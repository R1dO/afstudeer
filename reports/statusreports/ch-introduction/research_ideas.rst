***************************
Research ideas / questions.
***************************

A (bulleted) summary of different ideas applicable to for my thesis. Based on
the following problem description.

    Given a huge dataset which suffers from enormous amounts of missing
    values (including outliers) in both the time and spatial domain, determine
    the best approach for solving this defiency using the :term:`(M)SSA`
    technique originally develloped for short and noisy datasets. [1]_

And the 3 derived goals:

-  Asses capability of the implicit :term:`(M)SSA` technique for estimating
   missing :term:`LST` in large datasets. [2]_

-  Asses usability of principal components (eigenvectors) for describing
   natural :term:`LST` variability over the study area.

-  Provide a trade-off between temporal dimension versus the spatial
   dimension of a subset for performing :term:`(M)SSA` gap filling.

Hence the focus of this thesis lays on estimation the signal of a time series
large data gaps. The challenge lies in the the enormous amount of data
available, which makes it impractical to process the data in one go, making it
necessary to do a trade-off between dictionary [3]_ size and (expected
accuracy) of the result.

Despite the focus this section does allow ideas pointing into another direction.

General
-------

#. For a given technique identify the impact of different dictionary split
   dividing mechanism (spatial vs time domain, longitudal vs lattitudal).

   -  Determine accuracy of the splitted dictionary vs a “full” set.

   -  In the time domain determine the minimim size of a split (e.g. is it
      allowed to split a year)

#. Is it possible to combine the strength of the two methods. :term:`SSA` for
   data-driven filling and :term:`HANTS` / :term:`FFT` for parameter-driven
   identification of periodic signals.

#. How does a technique cope with the following error sources:

   -  Blunder values /outliers (or should they be removed a-priori)?

   -  Time series dominated by temporal gaps.

   -  Time series dominated by spatial gaps.

   -  Correction of white noise by smoothing (e.g. effect of using only a
      minimum amount of components for reconstructing).

SSA related
-----------

#. Determine the responce of different inputs on the filling result.

   -  Effect of sawtooth and/or step behaviour

   -  Effect of blunder values (gross errors), and if it is allowed to
      correct beforehand.

#. Understand the principle components, e.g. identify / classify those
   eigenvectors to get an understanding what kind of behaviour they can
   represent (oscillatory, trend, exponential , steps, etc).

   -  How do they look in the 1D time domain (SSA)

   -  And more importantly how does that translate to the 2D, spatial
      domain (MSSA)

   -  Compare the reconstructed component (probably per eigenvalue) with
      other methods for instance to find ’hidden’ periodic signals if a
      reconstructed component contains multiple signals.

#. Related to 2. Check if it is possible to determine the resulting beahviour
   based on those components without the need to reconstruct them.

#. Determine the effect of the major parameters on the filling result. [4]_

   -  Effect of window size.

      -  How well does it capture a signal

      -  Does it show up in a reconstruction (for instance do we
         observe the window size due to “jumps” in the reconstructed signal).

   -  Effect of lag-covariance matrix (frequency power leakage, behaviour
      on pure oscillations, computational effort)

      -  This is probably not something that is of high value for this
         research.

#. How suitable is SSA for forecasting (e.g. projecting the components of a
   small timespan on a larger one). [5]_

HANTS related
-------------

#. Create a (GUI) toolbox that allows selection of the algorithm version, and
   incorporates Monte-carlo simulations. [6]_

   -  What language (matlab, C, python), what license (closed vs open),
      what intention (sharing, project only, sale), what audience, etc?

.. [1]
   (and probably in comparison with :term:`HANTS`).

.. [2]
   Within the spirit of this goals it is possible to identify the
   difference between two methods for spatial-temporal gap filling,
   namely **HANTS** and **(M)SSA**, and to quantify the difference
   between them.

.. [3]
   Alternative wording for dataset, since the methods i use basicaly try
   to fill gaps by comparing gappy signals with “known” signals.

.. [4]
   Originally this was not intended, and use was made of expert opinion
   regarding the window size, however the available reconstruction see
   seems to indicate this influence might be important.

.. [5]
   Projecting as in the same method that SSA uses to get from the
   components back to a timeseries.

.. [6]
   This is from Hamid, but is it really thesis like material or more
   operational business like devellopment?
