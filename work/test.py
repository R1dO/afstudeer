# -*- coding: utf-8 -*-
"""@package <filename without extention>
<Brief description>

@date Created on Mon Jan 11 15:10:06 2016
@author: hdenouden
"""

import os
from osgeo import gdal

path = "data/preprocessed/equidistant_timepoints/FY2C/LST.raw/by-month/01/2008"
file = "FY2C_FDI_ALL_NOM_200801010000LST.raw"

fullpath = os.path.dirname(os.path.abspath(__file__)) + '/' + path + '/' + file
try:
    dataset = gdal.Open(fullpath)
except RuntimeError as err:
    print( "Exception: ", err)
    exit(1)

if dataset is not None:
    print("file opened")
    print(fullpath)

    print('\n-----\nDriver INFO\n-----',)
    print( 'Driver: ', dataset.GetDriver().ShortName,'/', \
          dataset.GetDriver().LongName)
    print( 'Size is ',dataset.RasterXSize,'x',dataset.RasterYSize, \
        'x',dataset.RasterCount)
    print( 'Projection is ',dataset.GetProjection())

    geotransform = dataset.GetGeoTransform()
    if not geotransform is None:
        print( 'Origin = (',geotransform[0], ',',geotransform[3],')')
        print( 'Pixel Size = (',geotransform[1], ',',geotransform[5],')')

    print('\n-----\nBand INFO\n-----',)
    band = dataset.GetRasterBand(1)
    print('Band Type=',gdal.GetDataTypeName(band.DataType))
    min = band.GetMinimum()
    max = band.GetMaximum()
    if min is None or max is None:
        (min,max) = band.ComputeRasterMinMax(1)
    print( 'Min=%.3f, Max=%.3f' % (min,max))
    if band.GetOverviewCount() > 0:
        print( 'Band has ', band.GetOverviewCount(), ' overviews.')
    if not band.GetRasterColorTable() is None:
        print( 'Band has a color table with ', \
        band.GetRasterColorTable().GetCount(), ' entries.')

    print('\n-----\nRead raster using scanline\n-----',)
## Cannot use scanline in separate call, it crashes current spyder IDE (2.3.8)
## due to ``Variable Explorer``.
#    scanline = band.ReadRaster( 0, 0, band.XSize, 1, \
#        band.XSize, 1, GDT_Float32 )
#    tuple_of_floats = struct.unpack('f' * b2.XSize, scanline)
    import struct
    tuple_of_floats = struct.unpack('f' * band.XSize, \
        band.ReadRaster( 0, 0, band.XSize, 1, band.XSize, 1, band.DataType))
    # Read  a full image file into array (more efficient?)
    image_array = band.ReadAsArray(xoff=0, yoff=0, win_xsize=None, win_ysize=None, buf_xsize=None, buf_ysize=None, buf_obj=None)

    print('\n-----\nPlot this image\n-----',)
    import matplotlib.pyplot as plt
    outimage = plt.imshow(image_array)
    outimage.set_cmap('viridis')
    plt.colorbar()
