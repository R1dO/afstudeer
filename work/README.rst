==============================
Project wide working directory
==============================

This folder exist in order to have a well defined 'current' working directory.
It serves as a base path when writing scripts and programs, for software
requiring a working directory and to store scripts and programs which are not
yet ready for inclusion under the other root directories of this repository.

Anything stored here should be short-lived. After finishing (part of) the work
the content should be moved to one of the appropriate (sub)folders under the
repository root.

Since it is a working directory it's content is expected to be volatile [#]_.


Folder structure
----------------
Organization of the top-level directory ``//work/``.

This is the lowest level README provided, hence it is possible this table
contains sub-directories.

+--------------------------------+---------------------------------------------+
| Folder                         | Contents                                    |
+================================+=============================================+
| ``data/ -> ../data/``          | Symbolic link to root data folder.          |
|                                |                                             |
|                                | It exists to facilitate access to data files|
|                                | without requiring a script (or the user) to |
|                                | go up one level in the directory tree.      |
+--------------------------------+---------------------------------------------+
| ``toolbox/``                   | Contains symbolic links to specific folders |
|                                | under ``../code/``                          |
+--------------------------------+---------------------------------------------+
| ``input/``                     | Symbolic links to input directories or data.|
|                                | Most likely to sub-directories of:          |
|                                |                                             |
| or any other name indicating   | *  ``data/originals/``                      |
| "original" data.               | *  ``data/derivatives/``                    |
+--------------------------------+---------------------------------------------+
| ``intermediate/``              | Store intermediate files here (for instance |
|                                | block divided ASCII-text files).            |
|                                |                                             |
|                                | Normally those files are deleted after work |
|                                | is finished. If they need to be stored the  |
|                                | best location is ``//data/processed/``.     |
+--------------------------------+---------------------------------------------+
| ``results/``                   | Store (final) results of processing here.   |
|                                |                                             |
|                                | After the current work is finished the      |
|                                | contents of this directory should be moved  |
|                                | to ``//data/processed/``.                   |
+--------------------------------+---------------------------------------------+

.. [#] Feel free to use any other naming scheme if it makes more sense.

Initialize matlab functions
---------------------------
In order to make use of the matlab functions under ``toolbox/matlab/`` they
must be added to the search pad. Assuming the current working directory is
already know to Matlab it suffices to use the function *Add to Path* ->
*Selected Folders and Subfolders* as seen in the next figure.

.. figure:: ./toolbox/add_toolbox_to_matlab_path.png
   :height: 400

   Adding the toolboxes to matlab's searchpath. In this screenshot *afstudeer*
   is a symbolic link inside matlab's searchpath pointing towards ``//work/``.

This will save the path **only** for the current session. To add them
to your path permanently use the save function of the *Set Path* dialog.

Matlab quirks
+++++++++++++
Although matlab has no problem traversing the filesystem using symbolic links
it is not always aware of them.

This leads to the following unexpected situation when using the *Current
Folder* view:

* The view indicates a file is on the search path and running from this view
  (F9) works as expected.

* Opening a file from this view (e.g. pass it to the editor) causes it to be
  opened relative to the current work directy (see ``pwd``), which need not be
  equal to its physical location.

  + If those locations are not equal matlab will complain that the current
    file from the editor is **shadowed** by a file on the search path when run
    from the editor (F5). Eventhough it is still the same file.

In order to work around this make a habit of opening all files w.r.t. the
working directory used when adding them to the search path.
Using the image above as an example this means opening them from this
*specific* view using the '+' boxes to traverse folders.
