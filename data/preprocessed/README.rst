=======================
Preprocessed data files
=======================
This folder contains preprocessed data files.

The purpose of this folder is two-fold:

1. Improve the quality of the datasets (for usage in calculations) by providing
   preprocessed derivatives of them.
2. Improve usability of the datasets by providing time-sliced folders.

Folder contents
---------------
Contents of: ``//data/preprocessed/`` and its subdirectories.

+-----------------------------------+------------------------------------------+
| Folders                           | Description                              |
+===================================+==========================================+
| ``equidistant_timepoints/``       | Provides 'complete' equidistant datasets.|
|                                   |                                          |
|                                   | Achieved by providing links to original  |
|                                   | samples (images) in combination with     |
|                                   | links to a **zero-filled** image for     |
|                                   | samples missing in the original set.     |
+-----------------------------------+------------------------------------------+
| ``spatial_subsets/``              | Subsets of the data limited in spatial   |
|                                   | size (for instance a single pixel).      |
|                                   | Mostly used for some small tests.        |
|                                   |                                          |
|                                   | .. Note:: Only store **small** sets. the |
|                                   |           prefered way is to create them |
|                                   |           on the fly when needed.        |
+-----------------------------------+------------------------------------------+


+-------------------------------+----------------------------------------------+
| Common sub folders [#]_       | Description                                  |
+===============================+==============================================+
| ``*/FY2C/``                   | Data from the FY2C satellite.                |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST/ -> LST.raw/``   | LST (Land Surface Temperature) data using    |
|                               | original file format.                        |
|                               |                                              |
|                               | A link to a specific LST.format folder       |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST.raw/``           | LST data using ENVI ``.raw`` & (optional)    |
|                               | ``.hdr`` files.                              |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST.format/``        | LST data using alternative file formats.     |
|                               |                                              |
|                               | Use appropriate extension.                   |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/*/all/``             | Contains links to samples for the complete   |
|                               | timespan.                                    |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/*/by-year/``         | Provides yearly time-slices.                 |
|                               | Using appropriate links to ``../all/``.      |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/*/by-month/``        | Provides monthly time-slices.                |
|                               | Using appropriate links to ``../all/``.      |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/*/by-day/``          | Provides daily (1-365) time-slices.          |
|                               | Using appropriate links to ``../all/``.      |
+-------------------------------+----------------------------------------------+

+-----------------------------+--------------------------------------------+
| Scripts                     | Description                                |
+=============================+============================================+
| ``generate_content.sh``     | Script to create additional directories    |
|                             | and populate with appropriate content.     |
+-----------------------------+--------------------------------------------+

.. [#] Those folders can be present in all top-level folders. They are only
       created when needed.

