=========
Resources
=========
This folder exist for the sole purpose of storing bulk data.
This includes both original (raw) datasets, and those in various stages of
processing.

Due to the sheer size of the dataset(s) it is not practical (nor advisable) to
include them all in this git-repository. Therefore use is made of an external
cloud-storage provider to facilitate sharing.

.. note:: Apparently Cloud storage can be a volatile business. Hence instead
          of relying on a specific provider i've opted to a direct contact
          approach. So please contact me for a download link.

.. note:: Scripts are provided to automate the task of retrieving and storing
          the datasets.

          See `helpers scripts documentation`_.

Folder structure
----------------
Organization of top-level directory ``//data/``.

=================  =============================================================
Folder             Contents
=================  =============================================================
`originals/`_      Original (downloaded) datasets.

                   Subdirectories and files are write-protected.

                   .. note:: The only personally generated files allowed here
                             are placeholder files that serve as link targets
                             for special cases (e.g. zero or NaN filled for
                             missing points).

`preprocessed/`_   Datasets derived from the ones in ``originals/``. With some
                   basic preprocessing applied (imputation of missing images,
                   zeros replaced by NaN, blunder values removed, etc.).

                   * Provides additional subdivision by time slices.
                   * Not restricted to original file format only (e.g. ``.mat``
                     is allowed here).

`processed/`_      Altered datasets (after some degree of processing). It is
                   allowed to save intermediate and final results here.

`published/`_      Datasets shared with others. Either by publications
                   (papers), by sharing within a project or by sending to
                   other parties.

                   .. note:: Mostly symbolic links to specific directories of
                             ``processed/`` (use write protection on those), with
                             optional additional created objects.

``tmp/``           **Optional** directory for temporal storage.
                   Created by scripts when needed.
=================  =============================================================

.. Note:: If it is desired to store the datasets on a physical different
          location create appropriate symbolic links inside the directories of
          the table above. See the README of each directory for expected
          directory names.

.. Note:: Minimizing data-duplication is achieved by using symbolic links on
          appropriate targets (files and directories). Hence expect a mix of
          symbolic links to original files and generated files inside the
          directories mentioned above.

How do I ...
------------
A non-exhaustive list of common Linux BASH commands for specific actions
operating on the datasets.

* Enable write protection when inside the target directory: [#]_
    .. code-block:: bash

        chmod 555 *

* Disable Write protection (owner only) for an arbitrary absolute path: [#]_
    .. code-block:: bash

        chmod -R 755 /path/to/target/

* Create symbolic links to a file from inside the current directory: [#]_
    .. code-block:: bash

        ln -s /path/to/link/target ./name_of_symbolic_link
  example (using relative paths and the name of the target):
    .. code-block:: bash

        ln -s ../../../originals/FY_Satellite/2008/*200801*LST.* ./

  Here ``200801`` indicates the year+month of interest, and ``LST`` the type
  of data.

* Create multiple size limited (1 GB in this example) ``tar.gz`` archives from a
  specific directory into the current one: [#]_

    .. code-block:: bash

        tar -czpv /path/to/archive/contents | split -d -b 1G - name.tar.gz.

* Extract (multiple) archives from current folder into specific directory [#]_:
    .. code-block:: bash

       cat name.tar.gz.* | tar -xzpv -C /path/to/store/archive/contents

.. [#] The number **5** enables reading and executing (needed for symbolic links).
.. [#] The number **7** enables writing, reading and executing.
.. [#] ``/path`` denotes an absolute path, ``./path`` is relative to the current
       location an ``../path`` is relative to the parent of the current location.
.. [#] ``.tar.gz`` is not required but it is helpful to indicate how it is
       saved and compressed.
.. [#] Due to ``split`` invocation when creating the ``tar.gz.#`` files, it is
       not possible to extract them directly using tar, hence
       ``cat ... | tar ...``

.. Define hyperlinks
.. _`helpers scripts documentation`: ../tools/scripts/README.rst
.. _`originals/`: originals/README.rst
.. _`preprocessed/`: preprocessed/README.rst
.. _`processed/`: processed/README.rst
.. _`published/`: published/README.rst
