======================
Results of processing.
======================
The purpose of this folder is to act as an easy reference to the results of
divers processing steps. This includes derived data-files and figures.


Folder contents
---------------
Contents of: ``//data/processed/`` and its subdirectories.

+-----------------------------------+------------------------------------------+
| Folders                           | Description                              |
+===================================+==========================================+
| ``dataset_information/``          | Analyses the extend of missing values    |
|                                   | (e.g. gaps, histograms, etc).            |
+-----------------------------------+------------------------------------------+
| ``HANTS``                         | Results of HANTS processing.             |
|                                   |                                          |
| ``HANTS/*/test_capability/``      | Grouped per sort of test.                |
+-----------------------------------+------------------------------------------+
| ``SSA/``                          | Results of SSA processing.               |
|                                   |                                          |
| ``SSA/*/test_capability/``        | Grouped per sort of test.                |
|                                   |                                          |
| ``SSA/*/m72s7/``                  | Grouped per processing parameter, where: |
|                                   |                                          |
|                                   | * m = window size                        |
|                                   | * s = Number of SSA components (or eigen-|
|                                   |   values) used for the reconstruction.   |
+-----------------------------------+------------------------------------------+

Subfolders like ``m72s2`` or ``histograms`` which indicate s specific type of
processed performed can be located either on the top level
``SSA/m72s7/FY2C/...`` or on the lowest level ``SSA/FY2C/.../figures/m72s7/``.

+-------------------------------+----------------------------------------------+
| Common sub folders [#]_       | Description                                  |
+===============================+==============================================+
| ``*/FY2C/``                   | Data from the FY2C satellite.                |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST/``               | LST (Land Surface Temperature).              |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST/data/``          | Storage of processing results in diverse     |
|                               | (raw) formats.                               |
| ``*/FY2C/*/all/``             |                                              |
|                               |                                              |
| ``*/FY2C/*/by-year/``         |                                              |
|                               |                                              |
| ``*/FY2C/*/by-month/``        |                                              |
|                               |                                              |
| ``*/FY2C/*/by-day/``          |                                              |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST/figures/``       | **Optional** Figures of processing results.  |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST/movies/``        | **Optional** Demonstration movies            |
+-------------------------------+----------------------------------------------+
| ``*/FY2C/LST/scripts/``       | Scripts used in generating the data and/or   |
|                               | figures.                                     |
|                               |                                              |
|                               | Important for reproducibility                |
+-------------------------------+----------------------------------------------+

+-----------------------------+------------------------------------------------+
| Scripts [#]_                | Description                                    |
+=============================+================================================+
| ``extract_SSA_folder.sh``   | Create the SSA folder and extract its contents |
|                             | from the cloud archives folder under:          |
|                             |                                                |
|                             | ``//data/published/distributed/...``           |
+-----------------------------+------------------------------------------------+
| ``create_SSA_archives.sh``  | Generate a backup of the SSA folder.           |
+-----------------------------+------------------------------------------------+

.. [#] Those folders can be present in all (sub)folders of the table above. They
       are only created when needed.
.. [#] Most likely only scripts to download and create archives. Since the
       contents are copied from ``//work/``, and the scripts reside under
       ``//code/`` (or are included under one of the folders mentioned in the
       first table).
