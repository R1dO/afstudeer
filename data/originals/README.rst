===================
Original data files
===================
This folder contains original data files from specific providers.


Folder contents
---------------
Contents of: ``//data/originals/`` and its subdirectories.

+-------------------------------+--------------------------------------------+
| Folders                       | Description                                |
+===============================+============================================+
| ``FY2C/``                     | Data from the FY2C satellite [#]_.         |
+-------------------------------+--------------------------------------------+
| ``FY2C/LST/``                 | LST (Land Surface Temperature) data        |
|                               |                                            |
| ``FY2C/LST/2008/``            | Grouped per year, hourly sampling, each    |
|                               | image consist of the following file types: |
| ``FY2C/LST/2009/``            |                                            |
|                               | +-----------+----------------------------+ |
| ``FY2C/LST/2010/``            | | ``*.raw`` | Image binary data.         | |
|                               | +-----------+----------------------------+ |
|                               | | ``*.hdr`` | Image header file.         | |
|                               | |           |                            | |
|                               | |           | Contains image properties. | |
|                               | +-----------+----------------------------+ |
|                               |                                            |
|                               | .. Note:: All images from 31-10-2010 until |
|                               |           31-12-2010 are missing (were not |
|                               |           available upon creation).        |
+-------------------------------+--------------------------------------------+
| ``placeholders/``             | Specially generated placeholder files.     |
+-------------------------------+--------------------------------------------+
| ``placeholders/zero_images/`` | Images containing only zeros               |
|                               |                                            |
|                               | Mostly used as LST gap-filling mechanism   |
|                               | in order to create continuous time series. |
+-------------------------------+--------------------------------------------+

+-----------------------------+-------------------------------------------------+
| Scripts                     | Description                                     |
+=============================+=================================================+
| ``extract_FY2C_folder.sh``  | Create the FY2C folder and extract its contents |
|                             | from the cloud archives folder under:           |
|                             |                                                 |
|                             | ``//data/published/distributed/...``            |
+-----------------------------+-------------------------------------------------+
| ``create_FY2C_archives.sh`` | Generate a backup of the FY2C folder.           |
+-----------------------------+-------------------------------------------------+

.. [#] Original as in: provided by my supervisor.
