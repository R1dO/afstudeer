==============
Published data
==============
The purpose of this folder is to store (snapshots of) published data, in order
to provide easy access to them. In this context published data means:

*Shared with others in any form and/or scope*.

The data stored here consist mostly of snapshotting links to data stored under
processed, unless indicated otherwise. It is allowed to pack and compress data
into archives here.

Folder contents
---------------
Contents of: ``//data/published/`` and its subdirectories.

+-----------------------------------+------------------------------------------+
| Folders                           | Description                              |
+===================================+==========================================+
| ``papers/``                       | Data used in papers.                     |
|                                   | Not only raw data, also derived products |
|                                   | like figures and tables.                 |
+-----------------------------------+------------------------------------------+
| ``projects/``                     | Data shared in the scope of different    |
|                                   | projects.                                |
+-----------------------------------+------------------------------------------+
| ``distributed/``                  | Other forms of sharing, for instance     |
|                                   | (but not limited to) compressed archives |
| ``distributed/cloud/copy.com/``   | of the original data and it's results    |
|                                   | shared via a cloud-storage provider.     |
+-----------------------------------+------------------------------------------+

+-----------------------------------+------------------------------------------+
| Scripts [#]_ [#]_                 | Description                              |
+===================================+==========================================+
| ``download_all_data_archives.sh`` | Download all data archives from the      |
|                                   | cloud-storage.                           |
+-----------------------------------+------------------------------------------+

.. Note:: Instead of using the download scripts for getting data from the cloud,
          a copy.com user can also request sharing permissions.

          If needed i can also provide a referral link, which gives extra
          storage capacity (both to you and me).

.. [#] Most likely only scripts to interact with cloud based archive storage.
.. [#] Since uploading to the cloud is only needed by me, no script will be
       provided.
