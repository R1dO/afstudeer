% TS_demo_movie
%
% Script which creates a demo movie based on both the original and the reconstructed ENVI
% binary data-files.
%
% The user is required to select both the original as the reconstructed files.
%
% In order to create the movie a loop over the length of the timeseries is performed. Both
% images are then plotted in the same figure, which is send to the moviecreator.
%
% For the original timeseries it is possible to replace zero values with NaN, although
% this is possibly not required due to the fixed scaling limits.
%
% All input and output files are handled via a GUI-dialog. It is not needed to specify
% them in this script. Multiple files can be selected.
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed
%   (e.g. load into a uint16 data-type, factor 2 reduction w.r.t single).
% * Casting into a function which can be called from other m-files.
% * Robustness, e.g. validation of input (e.g. byte order, image size).
%
% Known bugs:
% * None yet
%
% Disclaimer:
% This script has only been tested under MatlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See the website below for more information regarding ENVI header files:
% http://geol.hu/data/online_help/ENVI_Header_Format.html
%
% See Also ENVI_READ
% Documentation on data representation: FREAD
% ==================================================================================================

%% Initialize environment
% -----------------------
clear variables; close all; clc; % Basic matlab options
% ----------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% This is the ONLY section of this script were users should modify variables.
% The INPUT-structure variables should equal the ones of the ENVI header files!

% Define the image (time) size of the input RAW-files.
input.bands = 1;      % Number of time-points per input image (per INPUT RAW-file).
    % In our case were each timepoint is a single IMAGE this defaults to ONE.

% Specify input data representation. Choose one!
%input.input_class = 'int16';    % ENVI 2 = 16-bit signed integer (int16)
input.input_class = 'float32';  % ENVI 4 = 32-bit floating point (float32)

% Define some user preferences.
upref.data_class = 'single';  % Store values with which accuracy?
    % Note single equals float32. Use "help fread" to see available options.
upref.apply_NaN = 'yes';      % Should we replace zeros with NaN's?

% Define the start path when searching for the location of datasets.
gui.dataset.basePath = 'data/preprocessed/equidistant_timepoints/FY2C/LST.raw';
gui.dataset.processedPath = 'data/processed/SSA/unknown_parameters/FY2C/LST.raw';
gui.movie.storagePath = 'data/processed/SSA/unknown_parameters/FY2C/LST.raw/figures';
% ----------------------------------------------------------------------------------------

%% Init: GUI user preferences
% Ask user some questions about the dataset they are trying to load.

% Define the dialog box
options.image_ui.title = 'Input images properties';
options.image_ui.prompt = {...
    'How many rows has each image (y-axis) ?',...
    'How many colums has each image (x-axis) ?'};
options.image_ui.defaults = {'408','708'};
options.image_ui.answers = inputdlg(options.image_ui.prompt, ...
    options.image_ui.title, 1, options.image_ui.defaults);
% Cast answers into input.
input.rows = str2double(options.image_ui.answers{1});     % Image rows (y-axis)
input.cols = str2double(options.image_ui.answers{2});     % Image columns (x-axis)

%% Init: Specify the binary data-set(s)
% -------------------------------------
% Specify file and path to the original data-set (Use a GUI).
[upref.fileOrig, upref.dirOrig] = uigetfile( ...
    {'*.raw', 'ENVI binary images (*.raw)'; '*.*', 'All files (*.*)'}, ...
    'Select the original Gappy ENVI binary images.',...
    gui.dataset.basePath,'MultiSelect','on');

% Specify file and path to the reconstructed data-set (Use a GUI).
[upref.fileRec, upref.dirRec] = uigetfile( ...
    {'*.raw', 'ENVI binary images (*.raw)'; '*.*', 'All files (*.*)'}, ...
    'Select the reconstructed ENVI binary images.',...
    gui.dataset.processedPath, 'MultiSelect','on');

% Make sure that the images are sorted and hold the same amount of images.
upref.fileOrig = sortrows(upref.fileOrig);
upref.fileRec = sortrows(upref.fileRec);
if isequal(length(upref.fileOrig), length(upref.fileRec));
    % Do nothing
else
    error('r1dO:inputError',...
            'OOPS \n Both the original as reconstructed timeserie should have the same length');
end



%% Main: Create video
% Define a filename (Propose it to the user).
outMovie.file = 'testmovie.avi';
% Using a GUI let the user decide where to put the movie.
[outMovie.file,outMovie.dir] = uiputfile('*.avi',...
    'Where to save the movie',...
    fullfile(gui.movie.storagePath, outMovie.file));
    % Overrules proposed name. "upref.dir" points to last visited directory.

% Prepare and open movie file
outMovie.object = VideoWriter([outMovie.dir outMovie.file]);
%    outMovie.object = VideoWriter([outMovie.dir outMovie.file],'Uncompressed AVI');
%    outMovie.object = VideoWriter([outMovie.dir outMovie.file],'Archival');
outMovie.object.FrameRate = 1.33;   % Display each image for 3/4th of a second.
outMovie.object.Quality = 95;
open(outMovie.object);              % Open the file for writing

% Create animation by plotting the area.
% Obtain colorbar limits, for consistency over all images.
%outMovie.limits = [ min(min(min(data{1}))) ,  max(max(max(data{1})))];
% Define colorbar limits, since statement above is influenced by outliers.
%    outMovie.limits = [225 , 325];
outMovie.limits = [210 , 320];
outmovie.colormap = [1 1 1; colormap; 0 0 0];
% Define plot-titles
plotfiles = strrep(upref.fileOrig,'FY2C_FDI_ALL_NOM_', ''); % Removes first part
plotfiles = strrep(plotfiles,'LST.raw', ' UTC');             % Replace last part

% Define figure.
fig = figure(1); hold off
colormap(outmovie.colormap)
%clf     % Remove the standard white space axes
% Loop over all images.
for ind = 1:length(upref.fileOrig);
    % Start by reading the 2 images.
    outMovie.imageOrig = envi_read( fullfile( upref.dirOrig, upref.fileOrig{ind}),...
        input.rows, input.cols, input.bands, input.input_class);
    outMovie.imageRec = envi_read( fullfile( upref.dirRec, upref.fileRec{ind}),...
        input.rows, input.cols, input.bands, input.input_class);
    % Replace zeros with NaN's
    switch upref.apply_NaN
        case 'yes'
            outMovie.imageOrig(outMovie.imageOrig==0) = NaN;
        case 'no'
        % Do Nothing.
        otherwise
            error( 'R1dO:switchOptions',...
                'OOPS \n Cannot switch to requested NaN-setting: %s', upref.apply_NaN);
    end

    % Fill the figure with apropriate content
%     % Title part @ top , spanning the figure
%     axes('Position', [0.05 0.95 0.9 0.5], 'Visible', 'on');
    % Original image
%    axes('Position', [0.05 0.5 0.8 0.4], 'Visible', 'off');
    subplot(2,1,1, 'replace');
     imagesc(outMovie.imageOrig);
     axis image
     title(plotfiles(ind));
    % Reconstructed image
%    axes('Position', [0.05 0.05 0.8 0.4], 'Visible', 'off');
    subplot(2,1,2,'replace');
     imagesc(outMovie.imageRec);
     axis image
     title(plotfiles(ind));
     % The colorbar
    axes('Position', [0.6 0.10 0.3 0.8], 'Visible', 'off');
     caxis(outMovie.limits)
     colorbar

     % Write each frame to the file.
     currFrame = getframe(fig);
     writeVideo(outMovie.object,currFrame);
%pause(0.75)
end

% Close the file
close(outMovie.object);
fprintf('\n\tMovie has been written.\n');
