% SSA_compare_channels
%
% Script to compare 2 channels and visualize their differences.
%
% This script only touches the original data-sets, it does not alter them.
% it assumes that the channels are stored in ASCII data-files.
%
% This script will produce the following outputs:
% * Visual plot of the channels and their difference.
% * Scatter plot between the observed and reconstructed values.
% * General statistics of the channels difference.
%
% Any user preferences can be set in the Cell "User: Define options".
% The following options are implemented:
% * None Yet.
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script.
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed (by loading into a single or even integer data-type).
% * Allowing selection of multiple files.
% * Casting into a function which can be called from other m-files.
%
% Disclaimer:
% This script has only been tested under matlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% Additional note:
% This script uses functions from the statistical toolbox, and hence will
% not work in a vanilla environment.
% See also: NANMEAN NANSTD
%
% ==================================================================================================

%% Initialize environment
% -----------------------
clear variables; close all; clc; % Basic matlab options
% --------------------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% This is the ONLY section of this script were users should modify variables.
% It allows them to modify the output.

% Define axes-limit offset w.r.t. the data maximimum/minimum values.
plotpref.offset = 5;

% Define the start path when searching for the location of datasets.
matfileLocation = 'data/processed/SSA/test_capability/groundstation/LST.txt/';
% --------------------------------------------------------------------------------------------------

%% Init: Load channels
% --------------------
% Uncomment the next line to clear the variables of this cell.
%clear original reconstructed

% Specify file and path to the gappy channel.
[original.file,original.dir] = uigetfile({'*.*' ,  'All Files (*.*)'},...
    'Select the original gappy channel' , matfileLocation);

% Specify file and path to the reconstructed channel. Assume in same
% directory as the gappy channel, but allow other location.
[reconstructed.file,reconstructed.dir] = uigetfile({'*.*' ,  'All Files (*.*)'},...
    'Select the reconstructed channel ( _rec)' , original.dir);

% Load the above specified datasets .
original.data = load(fullfile(original.dir , original.file));
reconstructed.data = load(fullfile(reconstructed.dir , reconstructed.file));
[~, reconstructed.title, ~] = fileparts(reconstructed.file);
% --------------------------------------------------------------------------------------------------

%% Init: Set plot properties
% --------------------------
% Find the absolute integer min and max values of both datasets. And round
% down/up to multiple of "plotpref.offset".
plotpref.axis_limits = [ floor( (min( [original.data ; reconstructed.data]) - plotpref.offset)/5)*5 ,...
    ceil( (max( [original.data ; reconstructed.data]) + plotpref.offset)/5)*5];
%prefs.xlims = [0, length(original.data)];


%% Main: Calculate difference and statistics
% ------------------------------------------
% Uncomment the next line to clear the variables of this cell.
%clear difference

% Compute the difference vector (original-reconstructed)
difference.data = original.data-reconstructed.data;
% Add a statistics (cell) field to the difference structure
difference.statistics = {'Abs deviation', max(abs(difference.data));...
    'Maximum', max(difference.data); 'Minimum', min(difference.data);...
    'Mean', nanmean(difference.data); 'Std', nanstd(difference.data)};
% --------------------------------------------------------------------------------------------------

%% Output: Plotting.
% ------------------
% Plot the original, reconstructed and their difference in a single figure.
figure(1)
% Divide plot area in 3 horizontal parts and use upper 2 parts for the time series.
subplot(3,1,1:2); hold on;
 bar(original.data, 'g', 'EdgeColor', 'g');     % Original bars under reconstructed
 plot(reconstructed.data, 'k-');
% plot(original.data,'b.-');                     % Original on top of reconstructed
    title({'Original vs Reconstructed'; ['Reconstruction: ', reconstructed.title]}, 'Interpreter', 'none')
    ylabel('Temperature (K)')
    legend('Original','Reconstructed','Orientation','horizontal')
 set(gca, 'YLim', plotpref.axis_limits);     % Set Ylimits
% Use lowest horizontal part of plot area for the difference plot.
subplot(3,1,3)
 bar(difference.data, 'r', 'EdgeColor', 'g')
% plot(difference.data, 'r')
    ylabel('Difference (K)')
    xlabel('Time (hours)')
    legend('Original - Reconstructed')


% Create a scatter-plot between the values of the original data-set and the
% reconstructed one. Only one color hence use plot() not scatter().
figure(2)
 plot(original.data,reconstructed.data, 'k.')
    title({'Original values vs reconstructed values';...
        ['Reconstruction: ', reconstructed.title]}, 'Interpreter', 'none')
    xlabel('Original (K)')
    ylabel('Reconstructed (K)')
    grid on
% --------------------------------------------------------------------------------------------------

%% Output: Tabular
% ----------------
%Header
 fprintf('\n\tStatistics on the difference between the original and reconstructed timeserie.\n')
 fprintf('\tFile: %s \n\n',reconstructed.file);

% Values
% Unfortunately I could not find a way to correctly fprintf the cell in the
% current configuration without looping. Hence the temporal variable.
tmp_fprintf_cell = reshape(difference.statistics',1,numel(difference.statistics));
fprintf('%15s:\t % 1.5e\n',tmp_fprintf_cell{:})

clear tmp_fprintf_cell
% --------------------------------------------------------------------------------------------------
% ==================================================================================================

