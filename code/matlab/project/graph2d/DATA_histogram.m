% DATA_histogram.m
%
% Script which reads a matlab -v7.3 mat file and outputs a logarithmic histogram
% displaying the distribution of LST values. And spatial plots of the
% positive/negative spatial outliers.
%
% We only touch the original files, we do not alter them.
%
% Any user preferences is set in the Cell "User: Define options".
% The following additional user preferences are implemented:
% * Define the full path name to the m-file (as starting point for the GUI).
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script. Multiple files canNOT be selected.
%
% Limitations:
% * This script only works on R2011b (and later) due to usage of:
%   - new function matfile().
%
% Known bugs:
% * None Yet
%
% Required (non-standard) m-files:
%   * r1_memory_linux()
%
% Disclaimer:
% This script has only been tested under MatlabR2011b in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See Also MATFILE, R1_MEMORY_LINUX

% ==============================================================================
% Version:  1.0.0       ($Rev$)
% Matlab:   7.13.0.564 (R2011b)
% Copyright 2012:   H den Ouden                     | $Author$
% Mail:             H.denOuden@student.tudelft.nl   | r1denouden@gmail.com
% $Id$
% ==============================================================================

%% Initialize environment
% -----------------------
clear variables; clc; % Clear Workspace variables and command window.
close all;      % Close windows.
% ------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% The ONLY section of this script were users should modify variables.
%
% Define the start path when searching for the location of datasets.
matfileLocation = 'data/preprocessed/equidistant_timepoints/FY2C/LST.mat/';

% Define default variable name containing the data.
% ##### ALWAYS CHECK THIS NAME #####
matfileVariable = 'data';
% ##################################

% Define minimum, maximum and bin-size LST values for the histogram count
lstMin = 100;       % [K]
lstMax = 400;       % [K]
lstBin = 10;        % [K] Bin size for the histogram count.
% Note: Try to keep (lstMax-lstMin)/lstBin below 500 elements (on a 2Gb machine)

% Realistic values for min-max temperatures
%tempMin = 273-90;   % -90deg Celsius, taken from: http://wmo.asu.edu/world-lowest-temperature
%tempMax = 273+60;   % +60deg Celsius, taken from: http://wmo.asu.edu/world-highest-temperature
tempMin = 210;
tempMax = 320;

% Define max number of elements to be stored in memory
maxElems = r1_memory_linux('single')/4;     % Use only a quarter of the RAM.
% ------------------------------------------------------------------------------

%% Init: Selecting mat-file using a GUI + query dimensions
% --------------------------------------------------------
% Specify file and path to the data-set (Single file selection).
% Overwrites variables defined under "User: Define options".
[matfileName, matfileLocation] = uigetfile('', 'Please select the -v7.3 mat file', ...
    fullfile(matfileLocation, ''), 'MultiSelect','off');

% Construct an object to the datafile without actually loading it.
dataObj = matfile( fullfile(matfileLocation, matfileName));
% ------------------------------------------------------------------------------

%% Init: Initialize variables
% ---------------------------
% Define binvector containing bin-limits.
histBin = lstMin:lstBin:lstMax;
histBin = [-inf, histBin, +inf];            % Trick to include all out-of bound values in the first bin and last bin.
% Preallocate the count vector.
histCount = zeros(size(histBin));

% Query dimensions of data cube (using mattfile() size() method)
[tsRows, tsCols, tsLength] = size(dataObj,matfileVariable);

% Test if full dataset can be stored inside user defined portion of system RAM.
tsPageElems = tsRows*tsCols;                % Number of elements for a single image.
needLoop = tsPageElems*tsLength > maxElems; % True if dataset is too big
% ------------------------------------------------------------------------------

%% Main: Histogram count (NON-loop version)
% -----------------------------------------
% First load the complete dataset, then perform counting.
if ~needLoop
    % Use dynamic fieldnames to obtain data cube, not "depreciated" getfield().
    cube = dataObj.(matfileVariable);       % Load complete data-cube using the data-variable.
    disp('Cube loaded');                    % Feedback
    histCube = histc( cube, histBin, 3);    % Histogram with a page for each bin.
    histCount = permute( sum(sum(histCube)), [1,3,2]);
        % Sums over all rows and columns. Permute the result to get a row-vector.
    % Create spatial plots of outlier frequency.
    countTooLow = sum( cube<tempMin, 3);
    countTooHigh = sum( cube>tempMax, 3);
    clear cube                              % Clear possibly large local variable
    disp('Histogram count finished');       % Feedback
end % Only runs for small data sets
% ------------------------------------------------------------------------------

%% Main: Histogram count (loop version)
% -------------------------------------
%{
### IMPORTANT NOTE ###
Matlab works column wise, then row wise, then page wise, etc.
Hence when loading from a hard-drive iterate over the HIGHEST dimension for best
performance (prevents unnecessary random-access times).
Do NOT use "end" in indexing statements when a matfile() variable is used, since
that requires a full load of the matrix.
######################
Tradeoff between computing time and memory-requirement.
1st   Define the maximum size of a sub-cube that fits inside RAM (maxPages).
2nd   Define an index vector containing from-till (inclusive-exclusive) page
      indexes. Each interval =< maxPages.
3rd   Initialize an all zero histogram cube (will be summed inside the loop).
4th   Perform the histogram counting via an iterator loop. Output a histogram
      countCube. Withs spatial counts for each histogram level.
5th   Create histogram vector by correctly summing per page, followed by
      reshaping to the correct vector.
%}
if needLoop
    h = waitbar(0,'Performing histogram count. Please wait...');    % Display a waiting bar.
    % Initialize counting variables
    histCube = zeros( tsRows, tsCols, length(histBin) );            % Histogram cube (with a page for each bin).
    countTooLow = zeros( tsRows, tsCols, 1 );                       % Spatial distribution negative outliers
    countTooHigh = zeros( tsRows, tsCols, 1 );                      % Spatial distribution positive outliers
    % Initialize itterator control variables
    maxPages = floor( maxElems/tsPageElems );                       % Max number of pages to load per iteration.
    dataIndex = floor( linspace(1, tsLength + 1, ceil(tsLength/maxPages) + 1));
        % Equal spaced iterator indexes (inclusive-exclusive).
    nLoads = length(dataIndex) - 1;                                 % Number of iterations.
    for i = 1:nLoads;
        iStart = dataIndex(i);                                          % Start page (inclusive)
        iStop = dataIndex(i+1) - 1;                                     % Stop page (inclusive)
        iCube = dataObj.(matfileVariable)(:, :, iStart:iStop );         % Load correct data-cube from disk
        % Update counting variables
        histCube = histCube + histc(iCube,histBin,3);                   % Use histogram over the 3rd dimension.
        countTooLow = countTooLow + sum( iCube<tempMin, 3);
        countTooHigh = countTooHigh + sum( iCube>tempMax, 3);
        waitbar(i/nLoads);                                              % Update waitingbar
    end % Load + counting loop over all from-indices.
    histCount(:) = permute( sum(sum(histCube)), [1,3,2]);
        % Sums over all rows and columns. Permute the result to get a row-vector.
    disp('Histogram count finished');                               % Feedback
    close(h);                                                       % Close waiting bar
    clear dataIndex h i iCube iStart iStop % Clear local-only variable
end % Only runs for large data sets
% ------------------------------------------------------------------------------

%% Output: Plotting (WIP)
% -----------------
% X-axis can not cope with +/- inf, hence perform a trick
histBinOrig = histBin;  % Temporal copy.
histBin(1) = histBin(2)-lstBin ;        % Replace -inf by the first smaller bin limit
histBin(end) = histBin(end-1)+lstBin;   % Replace +inf by the first larger bin limit

% Note: histCount contain the count of all values from the current column
% up-till the next column (corresponding columns in histBin). bar() treats the
% corresponding value as the midpoint of a bin.
histBinPlot = histBin + 5;               % Needed for bar-plots without the histc option.

% Define a helper variable for the line function (borders of acceptable
% temperatures.
realTemperatureBoundary= repmat( [tempMin, tempMax], 2, 1);

% Plot the histogram
figure('Name','Blunder values in January 2008','color', [1,1,1]);   % Request a new figure with appropriate name and background color.
% Editing the colormap (0-frequency pixels must be white on subplots 3 and 4).
% This means that we should specify the bar-color explicit.
 fig.colormap = colormap;   % Get current colormap
 fig.colormap(1,:) = 1;     % First entry must be all white
 colormap(fig.colormap);    % Update the figure colormap.
% Plot the normal histogram in the upper left box.
 axObj(1) = subplot(2,2,1);
    bar( histBinPlot, histCount, 1, 'b'); % Bar plot with bins touching each other.
        title('January 2008')
        xlabel('Temperature range [K]')
        ylabel('Frequency')
    line( realTemperatureBoundary, repmat( get( gca, 'Ylim')', 1, 2), 'Color','r'); % Plot boundary lines
% Plot the logarithmic scaled histogram in the upper right box.
 axObj(2) = subplot(2,2,2);
    bar( histBinPlot, histCount, 1, 'b');   % Bar plot with bins touching each other.
        title('January 2008 (Logarithmic)')
        xlabel('Temperature range [K]')
        ylabel('Frequency')
    set(gca,'YScale','log');                                 % Y-axis behaviour
    line( realTemperatureBoundary, repmat( get( gca, 'Ylim')', 1, 2), 'Color','r'); % Plot boundary lines
% Control x-axis behaviour for the two upper plots.
 set(axObj(1:2), 'XLim',[histBin(1), histBin(end)], 'YGrid','on','YMinorGrid','off','XMinorTick','on');
% Plot the negative outliers spatial distribution .
 axObj(3) = subplot(2,2,3);
    imagesc(countTooLow);
     axis image
     colorbar('SouthOutside')
     title('Negative outlier frequency, spatial distribution')
     set(gca,'TickDir', 'out')
% Plot the positive outliers spatial distribution .
 axObj(4) = subplot(2,2,4);
    imagesc(countTooHigh);
     axis image
     colorbar('SouthOutside')
     title('Positive outlier frequency, spatial distribution')
     set(gca,'TickDir', 'out')

% ------------------------------------------------------------------------------

%% Clean up
% Remove unnecessary "Global" variables

% Let user know the script is finished
fprintf('\n\tScript: %s has finished.\n', mfilename);
% --------------------------------------------------------------------------------------------------

%% Diverse comments
% -----------------
% Optimization and extensions might be possible on the following subjects;
% * Allow overwriting of plot text (title, labels, legends), if possible?
% * Memory usage / speed.
% * Robustness / validation of input.
% * Implement the plotting of bin-content vs spatial location.
% * Use integers (int64?) for counting values not doubles (histcube-array).
%
% ------------------------------------------------------------------------------
% Use 80 columns for the header and full-line comments.
% ----------------------------------------------------------------------------------------------------------------------
% Use 120 columns for code line (permits longer variable names, and better indentation).
