% DATA_daily_variation_summed.m
%
% Script to get (and visualize) data on specific hours of the day. With the
% ability to sum over a longer time period (multiple days).
%
% ???Required (non-standard) m-files???:
%   * r1_memory_linux()
%
% Disclaimer:
% This script has only been tested under MatlabR2011b in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See Also ?

% ==============================================================================
% Version:  0.0.0       ($Rev$)
% Matlab:   7.13.0.564 (R2011b)
% Copyright 2012:   H den Ouden                     | $Author$
% Mail:             H.denOuden@student.tudelft.nl   | r1denouden@gmail.com
% $Id$
% ==============================================================================

%% Initialize environment
% -----------------------
clear variables; clc; % Clear Workspace variables and command window.
close all;      % Close windows.
% ------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% The ONLY section of this script were users should modify variables.

% Define the start path when searching for the location of datasets.
matfileLocation = 'data/preprocessed/equidistant_timepoints/FY2C/LST.mat/';

% Define default variable name containing the data.
% ##### ALWAYS CHECK THIS NAME #####
matfileVariable = 'data';
% ##################################

% Define the length of a day
dayLength = 24;                         % Number of samples per day.

% Define max number of elements to be stored in memory
maxElems = r1_memory_linux('single')/4; % Use only a quarter of the RAM.
% ------------------------------------------------------------------------------

%% Init: Selecting mat-file using a GUI + query dimensions
% --------------------------------------------------------
% Specify file and path to the data-set (Single file selection).
% Overwrites variables defined under "User: Define options".
[matfileName, matfileLocation] = uigetfile('', 'Please select the -v7.3 mat file', ...
    fullfile(matfileLocation, ''), 'MultiSelect','off');

% Construct an object to the datafile without actually loading it.
dataObj = matfile( fullfile(matfileLocation, matfileName));
% ------------------------------------------------------------------------------

%% Init: Initialize variables
% ---------------------------
% Query dimensions of data cube (using mattfile() size() method)
[tsRows, tsCols, tsTime] = size(dataObj,matfileVariable);

% Test if full dataset can be stored inside user defined portion of system RAM.
%tsPageElems = tsRows*tsCols;                % Number of elements for a single image.
%needLoop = tsPageElems*tsTime > maxElems;   % True if dataset is too big
% ------------------------------------------------------------------------------

%% Main: Query daily LST-max location (loop version)
% --------------------------------------------------
% Initialize counting variables
hourDailyMaxLST = zeros(tsRows,tsCols, ceil(tsTime/dayLength));    % Output array.
hourDailyMinLST = zeros(tsRows,tsCols, ceil(tsTime/dayLength));    % Output array.

% Initialize itterator control variables
dataIndex = 1:dayLength:tsTime+1;                           % Equal spaced iterator indexes (inclusive-exclusive).
nIterations = length(dataIndex) - 1;                        % Number of iterations.
% Sadly we need a trick for those cases where there are NaN's for complete days.
% Therefore we define an extra first NaN only page. if namax() index returns 1, we
% know the day is gap only. And hence we can correct for it.
iCube = zeros(tsRows,tsCols,dayLength+1);                   % Define 25 hour day
iCube(:,:,1) = nan;                                         % First page is NaN only.

% Perform the max value lookup per day
h = waitbar(0,'Performing maximum value lookup. Please wait...');   % Display a waiting bar.
for itt = 1:nIterations;
%i = 1;  %temporal construct to check innerloop
    iStart = dataIndex(itt);                                      % Start page (inclusive)
    iStop = dataIndex(itt+1) - 1;                                 % Stop page (inclusive)
    iCube(:, :, 2:end) = dataObj.(matfileVariable)(:, :, iStart:iStop );    % Load data-cube from disk (NaN-aware)
    [~, hourDailyMaxLST(:,:,itt)] = nanmax(iCube,[],3);           % Find location of max LST value.
    [~, hourDailyMinLST(:,:,itt)] = nanmin(iCube,[],3);           % Find location of min LST value.
    % Mean must be calculated here to prevent errors due to carry-over of
    % time (24h->01h). So compare current value with previous average. If
    waitbar(itt/nIterations);                                     % Update waitingbar
end % Load + lookup loop over all from-indices.
disp('maximum LST daily location lookup finished');         % Feedback
close(h);                                                   % Close waiting bar

% Correct for extra (NaN) page.
hourDailyMaxLST = hourDailyMaxLST -1;
hourDailyMinLST = hourDailyMinLST -1;
% Correct for full days of NaNs
hourDailyMaxLST(hourDailyMaxLST==0) = NaN;
hourDailyMinLST(hourDailyMinLST==0) = NaN;
% Original images are defined from 00:00 untill 23:00, hence substract 1.
hourDailyMaxLST = hourDailyMaxLST -1;
hourDailyMinLST = hourDailyMinLST -1;
% Derive the mean value
% Convert to radians and use mean angle to avoid errors due to time
% rollover (23:00 -> 00:00)
meanHourDailyMaxLST = (hourDailyMaxLST/24*360)*2*pi/360;   % Time of Day (ToD) in radians
meanHourDailyMinLST = (hourDailyMinLST/24)*2*pi;
% Use imaginary representation
meanHourDailyMaxLST = exp(i*meanHourDailyMaxLST);
meanHourDailyMinLST = exp(i*meanHourDailyMinLST);
% Take the mean
meanHourDailyMaxLST = nanmean(meanHourDailyMaxLST,3);
meanHourDailyMinLST = nanmean(meanHourDailyMinLST,3);
% Convert back to hours (via radians)
meanHourDailyMaxLST = angle(meanHourDailyMaxLST)*360/(2*pi)*(24/360);
meanHourDailyMinLST = angle(meanHourDailyMinLST)*24/(2*pi);
% Correct for negative hours
meanHourDailyMaxLST = mod(meanHourDailyMaxLST,24);
meanHourDailyMinLST = mod(meanHourDailyMinLST,24);

% Clear local-only variables
clear dataIndex h i iCube iStart iStop nIterations
% ------------------------------------------------------------------------------

%% Output: Plotting
% -----------------
% Plot mean (time) location of daily LST max.
figure('Name','Average UTC time of LST max during a day','color', [1,1,1]); % Request a new figure with appropriate name and background color.
colormap('jet')
imagesc_with_colorbar(meanHourDailyMaxLST,'Climits', [0 24]);
    title('Average UTC time of maximum LST during a day per pixel')
    set(gca,'TickDir', 'out')
    filename = 'gaps/average_time_of_max_LST_2008-11.svg';

% Plot mean (time) location of daily LST min.
figure('Name','Average UTC time of LST min during a day','color', [1,1,1]); % Request a new figure with appropriate name and background color.
colormap('hsv')
imagesc_with_colorbar(meanHourDailyMinLST,'Climits', [0 24]);
    title('Average UTC time of minimum LST during a day per pixel')
    set(gca,'TickDir', 'out')
    filename = 'gaps/average_time_of_min_LST_2008-11.svg';

% Plot (time) location of daily LST max for the 10th (arbitrarily chosen) day .
figure('Name','Time of max LST 10-november-2008','color', [1,1,1]); % Request a new figure with appropriate name and background color.
colormap('jet')
imagesc_with_colorbar(hourDailyMaxLST(:,:,10),'Climits', [0 24]);
    title('Time of max LST 10-november-2008')
    set(gca,'TickDir', 'out')
    filename = 'gaps/average_time_of_max_LST_2008-11-10.svg';

% Plot (time) location of daily LST min for the 10th (arbitrarily chosen) day .
figure('Name','Time of min LST 10-november-2008','color', [1,1,1]); % Request a new figure with appropriate name and background color.
colormap('hsv')
imagesc_with_colorbar(hourDailyMinLST(:,:,10),'Climits', [0 24]);
    title('Time of min LST 10-november-2008')
    set(gca,'TickDir', 'out')
    filename = 'gaps/average_time_of_min_LST_2008-11-10.svg';    
    
 % Plot X-profile for row 200 of the plots above.
figure('Name','X−profile of the daily max/min plots for row 200','color', [1,1,1]); % Request a new figure with appropriate name and background color.
plot([meanHourDailyMaxLST(200,:)', meanHourDailyMinLST(200,:)', hourDailyMaxLST(200,:,10)'])
 title('X−profile of the daily max/min plots for row 200')
 legend('meanDailyMax','meanDailyMin','max10november','Location','Best')
 axis([0 tsTime 0 24])

% ------------------------------------------------------------------------------

%% Clean up
% Remove unnecessary "Global" variables

% Let user know the script is finished
fprintf('\n\tScript: %s has finished.\n', mfilename);
% --------------------------------------------------------------------------------------------------

%% Diverse comments
% -----------------
% Optimization and extensions might be possible on the following subjects;
% * Allow overwriting of plot text (title, labels, legends), if possible?
% * Memory usage / speed.
% * Robustness / validation of input.
% * Use integers (int64?) for counting values not doubles?
%
% ------------------------------------------------------------------------------
% Use 80 columns for the header and full-line comments.
% ----------------------------------------------------------------------------------------------------------------------
% Use 120 columns for code line (permits longer variable names, and better indentation).
