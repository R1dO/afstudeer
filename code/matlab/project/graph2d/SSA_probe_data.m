% SSA_probe_data.m
%
% Script which loads a ascii dataset of interrest.
% It retrieves information about the number of NaN's encountered per
% channel(e.g. TimeSerie).
%
% The script only touches the original dataset, it does not alter it.
%
% The user does not need to specify the read vector in this script. A
% Dialog is implemented during runtime.
%
% Optimalization and extentions might be possible on the following subjects;
% * Memory usage / speed (by loading into a single or even integer datatype).
% * Allowing selection of multiple files.
% * Caste into a function which can be called from other m-files.
%
% Disclaimer:
% This script has only been tested under matlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% ==================================================================================================

%% Basic matlab options.
% ----------------------
clear variables; close all; clc; % Basic matlab options
% --------------------------------------------------------------------------------------------------

%% Specify input dataset
% Uncomment the next line to clear the variables of this cell.
%clc; clear bestand pad

%original = uiimport('-file'); % Not happy with its behaviour.

% Define the start path when searching for the location of datasets.
asciiFileLocation = 'data/preprocessed/equidistant_timepoints/FY2C/LST.txt/';

% Specify file and path to the dataset.
[bestand,pad] = uigetfile({'*.*',  'All Files (*.*)'},...
    'Select the dataset of interest', asciiFileLocation);
% --------------------------------------------------------------------------------------------------

%% Load the dataset and get some basic information.
% Uncomment the next line to clear the variables of this cell.
%clc; clear Original NaN_* loadtime

% Load the above specified dataset .
tic             % Start timer
Original = load(fullfile(pad,bestand));
loadtime =toc;  % Stop timer and store result

% Find all NaN values in the dataset.
% Note due too Matlab design decissions it is a 1 byte per value matrix not a 1
% bit. Still 8 times smaller than "double".
NaN_mask = isnan(Original);

% Store the memory size
Original_whos = whos('Original');

% For each channel check the amount of NaN's encountered.
NaN_per_channel = sum(NaN_mask);

% Find the average number of NaN's per channel
NaN_avg = mean(NaN_per_channel);

% Find the channel(s) with the minimum amount of NaN's.
NaN_min = min(NaN_per_channel);
NaN_min_index = find(NaN_per_channel==NaN_min);
NaN_min_occurences = length(NaN_min_index);

% Find the channel(s) with the maximum amount of NaN's.
NaN_max = max(NaN_per_channel);
NaN_max_index = find(NaN_per_channel==NaN_max);
NaN_max_occurences = length(NaN_max_index);
% --------------------------------------------------------------------------------------------------

%% Output: Print information to screen
% ------------------------------------
%Header
fprintf('\nFile: %s \n\n',bestand);
fprintf('File loaded in %g seconds\n\n', loadtime);
fprintf(['[N]\t [L]\t Memsize(kB)\t Mean(NaNs)\t Min(NaNs)\t #Min(NaNs)\t'...
    'Max(NaNs)\t #max(NaNs) \n\n']);

% Values
fprintf('%u\t %u\t %g\t\t %g\t\t %u\t\t %u\t\t %u\t\t %u\n',...
    size(Original,1), size(Original,2), Original_whos.bytes/1024, NaN_avg, NaN_min,...
      NaN_min_occurences , NaN_max , NaN_max_occurences);
% --------------------------------------------------------------------------------------------------

%% Create some area plots from the loaded data
% % Assuming the input data was using teh matlab -v7.3 style .mat files.
%
% % Calculate Mean average (nan-corrected) temperature.
% meanimage = nanmean(Original.data,3);      % 3rd dimension is time.
% % Plot Mean average (nan-corrected) temperature.
% figure('Name','Mean LST over January 2008','color', [1,1,1]); % New figure with appropriate name and background color.
%  imagesc(meanimage);
%   axis image
%   colorbar('SouthOutside')
%   title('Mean LST over January 2008')

% ==================================================================================================

