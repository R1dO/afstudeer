% Batch_Envi_to_mat73_plus_stats.m
%
% Script which reads several (monthly) directories of ENVI binary files and
% stores each directory as a separate mat-file (v7.3).
% The data is NaN-corrected (ENVI zero values are cast into NaN's).
%
% The mat file has the name of the selected directory with "_NaN" as suffix. It
% contains the following variables:
% * data    = NaN-corrected data aray (image-rows x image-columns x timepionts).
% * info    = Structural array with additional information.
%  - A short description of the array
%  - A list with original (ENVI) filenames.
%  - Original image descriptors (rows,columns,timepoints,precision)
%  - A single copy of the first encountered ENVI header file.
%  - A flag to indicate that NaN-correction has been applied.
% * derivatives   = Structural array with derivative products.
%  - nanTimepointsPerPixel      = Matrix of missing time points per pixel.
%  - nanPixelsPerTimepoint      = Vector of missing pixels per time point.
%  - histogram                  = Vector with (1 degree) value count of "data".
%  - maxSignalPerPixel          = Matrix of longest continuous timeserie per pixel.
%  - maxSignalPerPixelStart     = Matrix of longest continuous timeserie starttime per pixel.
%  - maxGapPerPixel             = Matrix of longest continuous gap per pixel.
%  - maxGapPerPixelStart        = Matrix of longest continuous gap starttime per pixel.
%
% All output files are stored under "/data/datasets/work" (user preference).
%
% We only touch the original (ENVI) files, we do not alter them.
%
% Any user preferences is set in the Cell "User: Define options".
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script. Multiple directories CANNOT be selected.
%
% Limitations:
% * None known yet
%
% Known bugs:
% * None known yet
%
% Required (non-standard) m-files:
% * None known yet
%
% Disclaimer:
% This script has only been tested under MatlabR2011b in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See Also  MULTIBANDREAD

% ==============================================================================
% Version:  1.0.0       ($Rev$)
% Matlab:   7.13.0.564 (R2011b)
% Copyright 2012:   H den Ouden                     | $Author$
% Mail:             H.denOuden@student.tudelft.nl   | r1denouden@gmail.com
% $Id$
% ==============================================================================

%% Init: Matlab environment
% ------------------------------
clear variables; close all; clc; % Basic matlab options
uiwait(warndlg({'This function is not up to par with current repository'...
    'structure.', 'It is a candidate for deletion (after a new script'... 
    'has been provided'}, 'Obsoleted Function','modal'));
% ------------------------------------------------------------------------------

%% Init: User preferences
% -----------------------
% The ONLY section of this script were users should modify variables.
% Needed for new storage scheme
use_year='2008';

% Define the starting location for storage of each .mat file.
storageLocation = 'data/preprocessed/equidistant_timepoints/FY2C/LST.mat';
% Location of base directory containing (monthly) subfolders.
enviBaseLocation = 'data/preprocessed/equidistant_timepoints/FY2C/LST.raw';

% Define input image properties.
imageBands = 1;             % Number of time-points per input image (per INPUT RAW-file).
imageRows = 408;
imageColumns = 708;
imagePrecision = 'single';  % ENVI 4 = 32-bit floating point (float32 == single class)

% Define minimum, maximum and bin-size LST values for the histogram count
% This is pure data driven, to see the extent of data anomalies make it
% ridiculously small/large.
% The histogram count will lump together all the values below the minimum, and
% above the maximum value before the 1st / after the last entry of the vector.
lstHistogramMin = 100;       % [K]
lstHistogramMax = 400;       % [K]

% If you want a GUI for selecting the base directory of the (monthly) subfolders
% set the following varibale to: true
useGUI = true;
% --------------------------------------------------------------------------------------------------

%% Init: GUI user preferences
% ---------------------------
% Only use this cell if useGUI = true
if useGUI
    % Specify path to the data-set.
    guiStruct.InputTitle = 'Select the parent folder containing the monthly directories';
    enviBaseLocation = uigetdir(enviBaseLocation , guiStruct.InputTitle);
    %
    % Specify path to store .mat files.
    guiStruct.OutputTitle = 'Select the folder where the .mat files should be stored';
    storageLocation = uigetdir(storageLocation , guiStruct.OutputTitle);
    %
    % Define input image properties
    guiStruct.ImagesTitle = 'Properties of the original input images';
    guiStruct.ImagesPrompt = {'Number of rows per image (y-axis):',...
                        'Number of columns per image (x-axis):',...
                        'Number of bands per image (time-points):',...
                        'Pixel precision (e.g. data-type):',...
                        'LST histogram lower limit [K]',...
                        'LST histogram upper limit [K]'};
    guiStruct.ImagesDefaults = {num2str(imageRows),...
                        num2str(imageColumns),...
                        num2str(imageBands),...
                        imagePrecision,...
                        num2str(lstHistogramMin),...
                        num2str(lstHistogramMax)};
    guiStruct.ImagesAnswers = inputdlg(guiStruct.ImagesPrompt, guiStruct.ImagesTitle, 1, guiStruct.ImagesDefaults);
    imageRows = str2double(guiStruct.ImagesAnswers{1});         % Double
    imageColumns = str2double(guiStruct.ImagesAnswers{2});      % Double
    imageBands = str2double(guiStruct.ImagesAnswers{3});        % Double
    imagePrecision = guiStruct.ImagesAnswers{4};                % String
    lstHistogramMin = str2double(guiStruct.ImagesAnswers{5});   % Double
    lstHistogramMax = str2double(guiStruct.ImagesAnswers{6});   % Double
end

%% Init: Initialize variabeles based on user preferences.
% Vector with bin-edges for the histogram count.
lstHistogramEdgesArray = [0, lstHistogramMin:1:lstHistogramMax, +inf]; % Kelvin does not allow negative values.
% --------------------------------------------------------------------------------------------------

%% Init: Construct a list of input directories and a list of output files
% -----------------------------------------------------------------------
% Get a directory-only listing. Using the "name" and "isdir" fields of the
% "dir()" command.
% This might break onder WINDOWS since it has no concept of current '.' and
% parent '..' directories (at least to my knowledge).
inputDirectoryArray = dir(enviBaseLocation);
% Cast the directory names in a cell vector, discard non-directories (including broken links).
inputDirectoryArray = { inputDirectoryArray( [ inputDirectoryArray(:).isdir ]).name}';
% Remove linux hidden files.
inputDirectoryArray = inputDirectoryArray(~strncmp(inputDirectoryArray, '.', 1));

% Base the names of the output files on the directory names.
%outputFileArray = strcat(inputDirectoryArray, repmat('_NaN.mat', length(inputDirectoryArray), 1));
% --------------------------------------------------------------------------------------------------

%% Main: Perform described tasks
% ------------------------------
% This section iterates over all inputDirectories performing the following
% operations:
% * Select all .raw image files and the 1st .hdr file.
% * Create the output .mat (v73) file
% * Create (and fill) the info variable inside the .mat file.
% * Read all .raw image files iteratively, performing the following operations:
%  - Replace zero valued pixels with NaN
%  - Store the adapted image in the corresponding data array of the .mat file.
%  - Update the missing timepoints matrix
%  - Update the missing pixels vector
%  - Update the histogram vector
%  - Update the max signal (and max signal start) matrix
%  - Update the max gap (and max gap start) matrix
% * Store the matrices/vectors designated by updated above in the .mat file.

% Iterate over the inputDirectories
for i=1:length(inputDirectoryArray);
    % close all % Safety during testing

    % Define the current image directory (absolute path).
    iFullDirectory = fullfile(enviBaseLocation, inputDirectoryArray{i}, use_year);

    fprintf(1, 'Start processing directory: %s \n', iFullDirectory);

    % Retrieve all .raw image file names (using the name field from dir() ).
    iRawFileArray = dir([iFullDirectory, '/*.raw']);
    iRawFileArray = {iRawFileArray.name}';

    % Define the header file based on the first image entry.
    iHeaderFile = strrep(iRawFileArray{1}, '.raw', '.hdr');

    % Define the contents of the "info" variable
    iInfoStruct.description = ['ENVI raw-images of: ', inputDirectoryArray{i}];
    iInfoStruct.imageUsesNan = 'true';
    iInfoStruct.nanMeaning = 'gaps';
    iInfoStruct.originalFileNames = iRawFileArray;
    iInfoStruct.imageBands = imageBands;
    iInfoStruct.imageRows = imageRows;
    iInfoStruct.imageColumns = imageColumns;
    iInfoStruct.imagePrecision = imagePrecision;
    iInfoStruct.imageHeaderContents = importdata( fullfile(iFullDirectory, iHeaderFile), '\t');
        % '\t' is required to allow parsing of header files containing "coordinate system string" variable.

    % Create the v73 mat-file using the a matfile() object construction.
    % This allows for partial and direct writing to the output file.
    iFilename = ['missing_is_NaN_', use_year, inputDirectoryArray{i}, '.mat'];
    iMatObject = matfile( fullfile(storageLocation, inputDirectoryArray{i}, use_year,iFilename), 'Writable', true);

    % Define data array size by storing a single value (with the desired precision) at the last element of the array.
    % Notes:
    % * Pre-allocating the full cube is not recommend (e.g. object.data=zeros(rows,cols,timepoints)), it will first
    % create the array in memory before dumping it to disk.
    % * Defining an empty cube (e.g. object.data=[]) will lead to problems when trying to add images later on.
    iMatObject.data(imageRows, imageColumns, length(iRawFileArray)) = zeros(1,1,imagePrecision);

    % Pre-allocate all itterator variables.
    jGapMaskPrevious = false(imageRows, imageColumns);
    jGapMaskCurrent = false(imageRows, imageColumns);
    jGapMaskDelta = zeros(imageRows, imageColumns);
    jSignalMaskPrevious = false(imageRows, imageColumns);
    jSignalMaskCurrent = false(imageRows, imageColumns);
    jSignalMaskDelta = zeros(imageRows, imageColumns);
    jGapTimepointsPerPixel = zeros(imageRows, imageColumns);
    jGapPixelsPerTimepoint = zeros(1, length(iRawFileArray));
    jHistogram = zeros(size(lstHistogramEdgesArray));
    jGapCurrent = zeros(imageRows, imageColumns);
    jGapStart = nan(imageRows, imageColumns);
    jSignalCurrent = zeros(imageRows, imageColumns);
    jSignalStart = nan(imageRows, imageColumns);
    jMaxGapPerPixel = zeros(imageRows, imageColumns);
    jMaxGapPerPixelStart = nan(imageRows, imageColumns);
    jMaxSignalPerPixel = zeros(imageRows, imageColumns);
    jMaxSignalPerPixelStart = nan(imageRows, imageColumns);
    % Loop over all images to process and store them.
    for j = 1:length(iRawFileArray)
        % Define full pathname to the current image
        jImagePathFull = fullfile(iFullDirectory, iRawFileArray{j});

        % Read image (using single precision for both input as output).
        jImage = multibandread(jImagePathFull, [imageRows, imageColumns, imageBands], strcat('*',imagePrecision), ...
            0, 'bsq', 'ieee-le');   % Required extra parameters: offset=0, interleave='bsq', byteorder='ieee-le')

        % Define masking matrices.
        jGapMaskPrevious = jGapMaskCurrent;
        jGapMaskCurrent = (jImage == 0);
        jGapMaskDelta = jGapMaskCurrent - jGapMaskPrevious;
            % -1=Gap stop, +1=Gap start, 0 = no changes.
        jSignalMaskPrevious = jSignalMaskCurrent;
        jSignalMaskCurrent = ~jGapMaskCurrent;
        jSignalMaskDelta = jSignalMaskCurrent - jSignalMaskPrevious;
            % -1=Signal stop, +1=Signal start, 0 = no changes.

        % Assign NaN to gaps and store the image in the mat-file data array
        % This must be done before the histc() call to prevent gaps from being counted.
        jImage(jGapMaskCurrent) = NaN;
        iMatObject.data(:,:,j) = jImage;

        % Update missing timepoints per pixel matrix
        jGapTimepointsPerPixel = jGapTimepointsPerPixel + jGapMaskCurrent;

        % Update missing pixels per timepoint vector
        jGapPixelsPerTimepoint(j) = sum(sum(jGapMaskCurrent));

        % Update histogram row-vector (dK = 1 degree)
        jHistogram = jHistogram + histc(jImage(:)', lstHistogramEdgesArray);

        % Update current gap/signal (and corresponding start-point) matrices.
        jGapCurrent = jGapCurrent + jGapMaskCurrent;
        jGapStart(jGapMaskDelta==+1) = j;
        jSignalCurrent = jSignalCurrent + jSignalMaskCurrent;
        jSignalStart(jSignalMaskDelta==+1) = j;

        % Update max gap/signal and corresponding start-point matrices.
        jMaxGapPerPixel = max(jMaxGapPerPixel, jGapCurrent);
        jMaxGapPerPixelStart(jGapCurrent==jMaxGapPerPixel) = jGapStart(jGapCurrent==jMaxGapPerPixel);
        jMaxSignalPerPixel = max(jMaxSignalPerPixel, jSignalCurrent);
        jMaxSignalPerPixelStart(jSignalCurrent==jMaxSignalPerPixel) = jSignalStart(jSignalCurrent==jMaxSignalPerPixel);

        % Reset gapCurrent if a signal starts
        jGapCurrent(jGapMaskDelta==-1) = 0;
        jSignalCurrent(jSignalMaskDelta==-1) = 0;

        % Update display counter (per 10 iterations)
        if mod(j,10);     % Returns false (or 0) when j is multiple of 10.
             % Do nothing
        else
            fprintf(1,'%s \t processed: %1.1f %% \n', iFilename,  j/length(iRawFileArray)*100);
        end
    end % Loop over all image files inside iFullDirectory.

    % Store calulated values in the derivative struct,
    iDerivativesStruct.nanTimepointsPerPixel = jGapTimepointsPerPixel;
    iDerivativesStruct.nanPixelsPerTimepoint = jGapPixelsPerTimepoint;
    iDerivativesStruct.histogram = jHistogram;
    iDerivativesStruct.histogramEdges = lstHistogramEdgesArray;
    iDerivativesStruct.maxGapPerPixel = jMaxGapPerPixel;
    iDerivativesStruct.maxGapPerPixelStart = jMaxGapPerPixelStart;
    iDerivativesStruct.maxSignalPerPixel = jMaxSignalPerPixel;
    iDerivativesStruct.maxSignalPerPixelStart = jMaxSignalPerPixelStart;
    % Clear all innerloop variables
    clear j*

    % Write remaining variables to the matfile.
    iMatObject.info = iInfoStruct;
    iMatObject.derivatives = iDerivativesStruct;

    % Put in a check variable
    %iMatObject.imagesParsed = i; % REMOVE THIS

    % Close the matobject (might not be needed, just for safety)
    clear iMatObject

    %disp(iHeaderFile);
    fprintf(1, '\n');
end % Loop over all input directories.

% Clear selected outer-loop variable(s)
clear iRawFileArray


%% Diverse comments
% -----------------
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed.
% * Robustness / validation of input.
% * Use integers (int64?) for counting/iterator values not doubles?
%
% ------------------------------------------------------------------------------
% Use 80 columns for the header and full-line comments.
% ----------------------------------------------------------------------------------------------------------------------
% Use 120 columns for code line (permits longer variable names, and better indentation).
