% SSA_create_subset_data
%
% Script to create a subset from the original ascii data-set.
%
% This script only touches the original data-sets, it does not alter them.
% it assumes that the channels are stored in ASCII data-files.
%
% Any user preferences can be set in the Cell "User: Define options".
% The following options are implemented:
% * 1 channel vector with the minimum gaps.
% * 1 channel vector with the maximum gaps.
%
% NOT IMPLEMENTED
% * A multi channel matrix with random channels.
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script.
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed (by loading into a single or even integer data-type).
% * Allowing selection of multiple files.
% * Casting into a function which can be called from other m-files.
%
% Disclaimer:
% This script has only been tested under matlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% Note: If there are multiple channels with minimum/maximum gaps this
% script will always return the first channel of them.
% See also: MIN MAX
%
% ==================================================================================================

%% Initialize environment
% -----------------------
clear variables; close all; clc; % Basic matlab options
uiwait(warndlg({'This function is not up to par with current research'...
    'status.', 'It is a candidate for review'}, 'Obsoleted Function',...
    'modal'));
% --------------------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% This is the ONLY section of this script were users should modify variables.
% It allows them to specify the type of subset wanted.

% Choose which vector to create by uncommenting the appropriate line below.
output.type = 'Min';    % Channel with the minimum amount of NaN's.
%output.type = 'Max';    % Channel with the maximum amount of NaN's.
%output.type = 'Multi';   % A multi channel dataset.
    % IMPORTANT: Select only one from the list above.

% In case of multi channel output define the number of channels here.
%output.channels = 25;

% Store image properties (to map ASCII column to image column)
original.imagerows = 408;
original.imagecols = 708;

% Define the start path when searching for the location of datasets.
gui.dataset.basePath = 'data/preprocessed/equidistant_timepoints/FY2C/LST.txt/';
gui.dataset.processedPath = 'data/preprocessed/spatial_subsets/FY2C/LST.txt';
% --------------------------------------------------------------------------------------------------

%% Init: Load an input data-set
% -----------------------------
% Specify file and path to the data-set.
[original.file,original.dir] = uigetfile({'*.*',  'All Files (*.*)'},...
    'Select the input multi-channel data-set.',gui.dataset.basePath);

% Load the above specified data-set .
original.data = load(fullfile(original.dir , original.file));
% --------------------------------------------------------------------------------------------------

%% Main: Extract channel of interest
% ----------------------------------
% Calculate the amount of NaN's per channel.
original.NaN_per_channel = sum(isnan(original.data));

% Find the channel(s) which suits the users demand.
switch output.type
    case 'Min'
        [output.NaNs, output.column] = min(original.NaN_per_channel);
    case 'Max'
        [output.NaNs, output.column] = max(original.NaN_per_channel);
    case 'Multi'
        disp('This setting is not implemented, unsure if ever will.')
        return
        output.random = rng;
        output.column = randperm(output.channels);
    otherwise
        error('r1dO:switchOptions',...
            'OOPS \n Cannot switch to requested output.type: %s',output.type);
end
% NOTE: min()/max() always returns the first encountered column on multiple hits.

% Define the output data-set
output.data = original.data(:,output.column);
% --------------------------------------------------------------------------------------------------

%% Output: Write data-file
% ------------------------
original.imageRows = 408;
original.imageCols = 708;
% Use 1 based image references
output.imageCol = mod(output.column, original.imageCols);
if isequal( output.imageCol, 0)
    output.imageRow = floor(output.column/original.imageCols);
    output.imageCol = original.imageCols;
else
    output.imageRow = floor(output.column/original.imageCols) +1;
end

% Propose the following name to the user:
output.file = ['SingleChannel_', output.type,...
                '_Row_', num2str(output.imageRow),...
                '_Col_', num2str(output.imageCol),...
                '.data'];
%
% Using the inputs GUI let user decide where to put the result.
[output.file,output.dir] = uiputfile('*.data','Where to save the output data-set',...
    fullfile( pwd, gui.dataset.processedPath, output.file));

% Store data-set in the requested file, each value is written with 3 decimal
% places (equal to the input file).
dlmwrite( fullfile( output.dir,output.file), output.data, 'delimiter', '\t','precision', '%.3f');

% Quick test to see if the file was written correctly (taking into account
% numerical errors)
if max(output.data - load( fullfile( output.dir,output.file))) <= eps(min(output.data))
    disp({'The subset is successfully written to:'; fullfile(output.dir,output.file)});
else
    error('r1dO:writefile',['It looks like something went wrong during the write/read cycle\n',...
        'Please review this script'])
    % Note will also happen when output.data consist only of NaN's.
end
