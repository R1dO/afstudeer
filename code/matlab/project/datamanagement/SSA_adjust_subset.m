% SSA_adjust_subset
%
% Script which simulates cooling of ground due to cloud cover.
%
% This script only touches the original data-sets, it does not alter them.
% it assumes that the channels are stored in ASCII datafiles.
%
% Any user preferences can be set in the Cell "User: Define options".
% The following options are implemented:
% * The user can specify the cooling behaviour.
% * Introduce arificial gaps in the signal !!!ToBeImpl!!!
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script.
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed (by loading into a single or even integer data-type).
% * Allowing selection of multiple files.
% * Casting into a function which can be called from other m-files.
% * It might be possible to speed up the for loop. Either for larger (more
%   channel) datasets, or larger cooling windows.
%
% Disclaimer:
% This script has only been tested under matlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% ==================================================================================================

%% Initialize environment
% -----------------------
clear variables; close all; clc; % Basic matlab options
disp('Function not working, probably outdated as well')
return
% --------------------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% This is the ONLY section of this script were users should modify variables.
% It allows them to specify the type of subset wanted.

% Define the cooling window. E.g. a vector stating how many kelvin should
% be substracted after a gap. For instance [5 3 2] would mean: Substract 5
% degrees for the first observation after a gap, substract 3 degrees from
% the second observation after the gap and substract 2 degrees from the 3rd
% observation after the gap.
% If a gap is encountered within the window the cooling will start at the
% first number.
%cooling_window = [4 3 2 1 1];
cooling_window =[];             % We dont want to use artificial cooling.

% Section where artificial gaps can be introduced
gaps.number = 20;           % Number of artificial gaps
gaps.length = 20;           % Maximum size of a gap
gaps.sizeISrandom ='no';    % Should we allow for different gap sizes?
gaps.distribution

% --------------------------------------------------------------------------------------------------

%% Init: Load input dataset
% -------------------------
% Uncomment the next line to clear the variables of this cell.
%clc; clear original

% Specify file and path to the gappy channel.
[original.file,original.dir] = uigetfile({'*.*' ,  'All Files (*.*)'},...
    'Select the original gappy channel' , 'Data/');

% Load the above specified dataset .
original.data = load(fullfile(original.dir , original.file));
% --------------------------------------------------------------------------------------------------

%% Main: Find signal periods
% --------------------------
% Uncomment the next line to clear the variables of this cell.
%clc; clear signal

% Find the singals in the dataset, using a binary mask.
signal.mask = isfinite(original.data);
% Find 'start' and 'stop' row-identifiers of the temperature measurement
% periods between the gaps.
signal.row_start = find( diff( [0 ; signal.mask]) > 0);
signal.row_stop = find( diff( [signal.mask ; 0]) < 0);
signal.row_length = signal.row_stop - signal.row_start + 1;
% --------------------------------------------------------------------------------------------------

%% Main: Apply cooling window
% ----------------------------
% Uncomment the next line to clear the variables of this cell.
%clc; clear output

% If else construct to determine if cooling is wanted.
if isempty(cooling_window)
    % Do nothing
    fprintf('/nThe cooling option is not used./n')
else
    % Copy channel from the input
    output.data = original.data;

    % Unfortunatly since multiple time points should be adjusted a loop
    % becomes necessary. Abeit the number of repeats is kept to an absolute
    % minimum (as far as my knowledge goes).
    % The loop goes over the cooling window, and adjust the appropriate
    % time points columnwise.
    for idCW = 1:length(cooling_window)
        % Determine rows to adapt (*Sigh* in-loop variable creation *Sigh*)
        idROWS = signal.row_start(signal.row_length >= idCW)' + idCW -1;
        output.data(idROWS) = output.data(idROWS) - cooling_window(idCW);
    end
    clear id*   % Clean temporal variables
end
% --------------------------------------------------------------------------------------------------

%% Main: Introduce artificial GAPS

%% Output: Write datafile
% -----------------------
% Propose the following name to the user (use original name without
% extention, and rebuild new name).
output.file = [regexprep(original.file,'\.\w*','') '_adapted.txt'];

% Using the gui, let user decide where to put the result.
[output.file, output.dir] = uiputfile('*.txt','Where to save the adapted dataset',...
    [original.dir output.file]);
    % This overrules the proposed name. "original.dir" points user to last
    % visited directory.

% Store dataset in the requested file, each value is written with 3 decimal
% places (equal to the input file).
dlmwrite( fullfile( output.dir,output.file), output.data, 'delimiter', '\t','precision', '%.3f');

% Quick test to see if the file was written correctly (taking into account
% numerical errors)
if max(output.data - load( fullfile( output.dir,output.file))) <= eps(min(output.data))
    disp({'The subset is succesfully written to:'; fullfile(output.dir,output.file)});
else
    error('r1dO:writefile',['It looks like something went wrong during the write/read cycle\n',...
        'Please review this script'])
end
