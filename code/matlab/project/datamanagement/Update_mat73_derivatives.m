% Update_mat73_derivatives.m
%
% Script which reads the raw data of one or several huge mat73 files
% containing LST data and adds derivative information to the struct derivatives.
% All calculated data is based on the time period defined within the matfile.
%
% The following changes will be performed (if applicable):
% * If exist, empty the redundant variable "imagesParsed".
% * Add an image with the mean average LST values to the "derivatives" struct
% * Add an image with the total number of gaps per pixel
% * Add an image with the total number of signal-fragments per pixel (Note this
%   will deviate not more than +/- 1 from number of gaps, but depends if the
%   time-series start/stops with a signal/gap.
%
% It is assumed that the .mat73 files contain the following variables:
% * data        = NaN-corrected data aray (rows x columns x timepionts).
% * info        = Structural array with additional information.
% * derivatives = Structural array with derivative products.
% Those names (and other user preferences) are can be changed set in the Cell:
%   "User: Define options".
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script. Multiple directories CANNOT be selected.
%
% Limitations:
% * mat-file variables can only be emptied, not deleted.
%
% Known bugs:
% * None known yet
%
% Required (non-standard) m-files:
% * None known yet
%
% Disclaimer:
% This script has only been tested under MatlabR2011b in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See Also  MATFILE R1_DUALLOG

% ==============================================================================
% Version:  0.0.1       ($Rev$)
% Matlab:   7.13.0.564 (R2011b)
% Copyright 2012:   H den Ouden                     | $Author$
% Mail:             H.denOuden@student.tudelft.nl   | r1denouden@gmail.com
% $Id$
% ==============================================================================

%% Init: Matlab environment
% ------------------------------
clear variables; close all; clc; % Basic matlab options
r1_duallog('START')
% ------------------------------------------------------------------------------

%% Init: User preferences
% -----------------------
% The ONLY section of this script were users should modify variables.

% Default storage location.
storageLocation = 'data/preprocessed/equidistant_timepoints/FY2C/LST.mat';

% Assume the following naming scheme inside the .mat73 files:
varData = 'data';               % The (huge) data array
varInfo = 'info';               % Structural array with additional information.
varDerivatives = 'derivatives'; % Structural array with derivative products.
varUnwanted = 'imagesParsed';   % Empty this one if it exist.

% Balance between memory usage and number of itterations
% Each image is ~1.1MiB, the number of pages is always a multiple of 24.
% 24*1.1 fits easily in memory, hence:
jStepsize = 24;
r1_duallog('\n>>> Parsed user preferences.\n')
% --------------------------------------------------------------------------------------------------

%% Init: GUI user preferences
% ---------------------------
% Let user select one or multiple matfiles.
% Overwrites variables defined under "User: Define options".
[matfileNames, storageLocation] = uigetfile('', 'Please select the -v7.3 mat file(s)', ...
    fullfile(storageLocation, ''), 'MultiSelect','on');

% Make sure a single selected .mat73 file is also returned as a cell
if ischar(matfileNames);
    matfileNames = {matfileNames};
end

%% Main: Perform described tasks
% ------------------------------
% This section iterates over all requested .mat73 files performing the
% following operations:
% * Empty the redundant variable "imagesParsed" if it exist.
% * Add/replace an image with the mean average LST values in the "derivatives" struct.
% * Add an image with the total number of gaps per pixel.
% * Add an image with the total number of signal fragments per pixel.

% Iterate over the .mat73 files
for i=1:length(matfileNames);
    % Feedback
    r1_duallog('\tStart processing matfile: %s\n', matfileNames{i});

    % Create a (writable) pointer to the v73 mat-file using the matfile() object construction.
    % Allow for partial and direct writing to the output file.
    iMatObject = matfile( fullfile(storageLocation, matfileNames{i}), 'Writable', true);

    % Section for redundant variables
    % -------------------------------
    % Check existance of "imagesParsed" variable that is not empty. Correct when found.
    if isempty( who(iMatObject, varUnwanted)) || isempty( iMatObject.(varUnwanted));
        r1_duallog('\t* No redundant variable with erroneous value exist.\n');
    else
        iMatObject.(varUnwanted) = [];
        r1_duallog('\t* Variable ''%s'' has been emptied.\n', char( who( iMatObject, varUnwanted)));
    end
    % -----------------------------------
    % End section for redundant variables


    % Loop section over data array
    % ----------------------------
    % Calulates:
    % Pixel mean image
    % No of gaps per pixel image
    % No of signal fragments per pixel image

    % Current data array dimensions.
    [iRows, iCols, iPages] = size(iMatObject, varData);

    % Initialize counting arrays
    jLstSumPerPixel = zeros(iRows, iCols);
    jCountNans = zeros(iRows, iCols);       % Needed for correctly calculate mean
    jCountGaps = zeros(iRows, iCols);       % Start gapcount image with zero gaps.
    jCountSignal = zeros(iRows, iCols);       % Start signal fragments image with zero signal fragments.
    jGapStartInit = zeros(iRows, iCols);    % Starting page so diff(X,1,3) returns 1 on gap-start.
    jSignalStartInit = zeros(iRows, iCols); % Starting page so diff(X,1,3) returns 1 on signal-start.

    % Loop over all pages to process the complete array.
    for j=1:jStepsize:iPages
        jData = iMatObject.(varData)(:, :, j:j+jStepsize-1 );   % Get Array
        jGapMaskCurrent = isnan(jData);                         % Keep track of NaN
        jSignalMaskCurrent = ~jGapMaskCurrent;                  % Keep track of signal

        % Pixel mean image
        jData(jGapMaskCurrent) = 0;                             % Replace NaN with zero.
        jLstSumPerPixel = jLstSumPerPixel + sum(jData,3);       % Sum all non-NaN LST values.
        jCountNans = jCountNans + sum(jGapMaskCurrent,3);       % Sum all gaps.

        % No of gaps per pixel image
        jGapDataArray = cat(3, jGapStartInit, jGapMaskCurrent); % Add the gap start page before the current gapMask.
        jGapDiffArray = diff(jGapDataArray, 1, 3);              % Difference in time (1=GapStart, -1=SignalStart)
        jGapDiffArray(jGapDiffArray < 1) = 0;                   % We only want the starting elements.
        jCountGaps = jCountGaps + sum(jGapDiffArray, 3);        % Update the gap count image.
        jGapStartInit = jGapMaskCurrent(:,:,end);               % Update gap starting page.

        % No of signal fragments per pixel image
        jSignalDataArray = cat(3, jSignalStartInit, jSignalMaskCurrent); % Add the signal start page before the current signalMask.
        jSignalDiffArray = diff(jSignalDataArray, 1, 3);                % Difference in time (-1=GapStart, 1=SignalStart)
        jSignalDiffArray(jSignalDiffArray < 1) = 0;                    % We only want the starting elements.
        jCountSignal = jCountSignal + sum(jSignalDiffArray, 3);         % Update the gap count image.
        jSignalStartInit = jSignalMaskCurrent(:,:,end);                 % Update gap starting page.

        % Update display counter.
        r1_duallog('\t %s : %1.1f %%\n',matfileNames{i}, (j+23)/iPages*100);
    end

    % Calculate requested mean values
    iLstMeanPerPixel = jLstSumPerPixel ./ (iPages - jCountNans);

    % Store the new images in the derivative struct.
    % NOTE: will fail if 'varDerivatives' does not exist in matobject.
    iMatObject.(varDerivatives) = setfield( iMatObject.(varDerivatives) , 'lstMeanPerPixel', iLstMeanPerPixel);
    iMatObject.(varDerivatives) = setfield( iMatObject.(varDerivatives) , 'amountGapsPerPixel', jCountGaps);
    iMatObject.(varDerivatives) = setfield( iMatObject.(varDerivatives) , 'amountSignalFragmentsPerPixel', jCountSignal);

    % --------------------------------
    % End loop section
end
r1_duallog('\n Execution finished\n')
r1_duallog('STOP')

%% Diverse comments
% % -----------------
% % Optimization and extensions might be possible on the following subjects;
% % * Memory usage / speed.
% % * Robustness / validation of input.
% % * Use integers (int64?) for counting/iterator values not doubles?
% %
% % ------------------------------------------------------------------------------
% % Use 80 columns for the header and full-line comments.
% % ----------------------------------------------------------------------------------------------------------------------
% % Use 120 columns for code line (permits longer variable names, and better indentation).
