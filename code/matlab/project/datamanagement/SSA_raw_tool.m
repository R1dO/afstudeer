% SSA_raw_tool
%
% Script which reads ENVI binary data-files.
% With the ability to perform certain tasks.
%
% We only touch the original files, we do not alter them.
%
% Any user preferences is set in the Cell "User: Define options".
% The following user preferences are implemented:
% * Map zero valued entries to NaN, or just keep them zero.
% * Store the data in ssa-mtm ASCII style (3 digit accuracy)
%   or as a Matlab .mat file (Using the v7.3 file specification, does not
%   really support partitioning).
% * Partitioning the data-set in multiple windows (e.g. cluster of pixels).
%   The partitioning has the following scheme (example uses 4 horizontal and 3
%   vertical blocks). When storing to ASCII the filename will contain a
%   section that indicates the block partitioning setup: '[columns x rows]'.
%   --                   --
%   | [1]  [2]  [3]  [4]  |
%   | [5]  [6]  [7]  [8]  |
%   | [9]  [10] [11] [12] |
%   --                   --
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script. Multiple files can be selected.
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed
%   (e.g. load into a uint16 data-type, factor 2 reduction w.r.t single).
% * Casting into a function which can be called from other m-files.
% * Robustness, e.g. validation of input (e.g. byte order, image size).
%
% Known bugs:
% * If only 1 image is selected an warning message appears complaining
%   about the number of input vectors. This is related to block-division.
%   Please ignore this warning, the block division works.
%
% Disclaimer:
% This script has only been tested under MatlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See the website below for more information regarding ENVI header files:
% http://geol.hu/data/online_help/ENVI_Header_Format.html
%
% See Also ENVI_READ
% Documentation on data representation: FREAD
% ==================================================================================================

%% Initialize environment
% -----------------------
clear variables; close all; clc; % Basic matlab options
% --------------------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% This is the ONLY section of this script were users should modify variables.
% The INPUT-structure variables should equal the ones of the ENVI header files!

% Define the image (time) size of the input RAW-files.
input.bands = 1;      % Number of time-points per input image (per INPUT RAW-file).
    % In our case were each timepoint is a single IMAGE this defaults to ONE.

% Specify input data representation. Choose one!
%input.input_class = 'int16';    % ENVI 2 = 16-bit signed integer (int16)
input.input_class = 'float32';  % ENVI 4 = 32-bit floating point (float32)

% Define some user preferences.
upref.data_class = 'single';  % Store intermediate values with which accuracy?
    % Note single equals float32. Use "help fread" to see available options.
upref.apply_NaN = 'yes';      % Should we replace zeros with NaN's?
upref.xblocks = 1;            % Partition image in # of vertical blocks?
upref.yblocks = 1;            % Partition image in # of horizontal blocks?
upref.createMovie = 'no';    % Create an avi movie from the time series?

% Specify how the data is stored on disk.
% Uncomment the required storage. Only one is allowed, if multiple lines
% are uncommented only the last one is used.
upref.storage = 'ssa-mtm';    % Store the data in a single ASCII file with the format xxx.yyy
upref.storage = 'mat';        % Store the whole workspace in a single Matlab ".mat" file.
%upref.storage = 'none';       % Do not store any data on disk.

% Define the start path when searching for the location of datasets.
gui.dataset.basePath = 'data/preprocessed/equidistant_timepoints/FY2C/';
gui.dataset.processedPath = 'data/processed/dataset_information/FY2C/LST';
% --------------------------------------------------------------------------------------------------

%% Init: GUI user preferences
% Ask user some questions about the dataset they are trying to load.

% Define the dialog box
options.image_ui.title = 'Properties of the original input images';
options.image_ui.prompt = {...
    'How many rows has each image (y-axis) ?',...
    'How many colums has each image (x-axis) ?'}; %,...
    %'How many days are you trying to load ?'};
options.image_ui.defaults = {'408','708'}; %,'31'};
options.image_ui.answers = inputdlg(options.image_ui.prompt, ...
    options.image_ui.title, 1, options.image_ui.defaults);
% Cast answers into input.
input.rows = str2double(options.image_ui.answers{1});     % Image rows (y-axis)
input.cols = str2double(options.image_ui.answers{2});     % Image columns (x-axis)
%input.days = str2double(options.image_ui.answers{3});     % Days a month.

%% Init: Specify the binary data-set(s)
% -------------------------------------
% Specify file and path to the data-set (Use a GUI).
[upref.file, upref.dir] = uigetfile( ...
    {'*.raw', 'ENVI binary images (*.raw)'; '*.*', 'All files (*.*)'}, ...
    'Please select the ENVI binary images.',...
    fullfile( pwd, gui.dataset.basePath, 'LST.raw',''), 'MultiSelect','on');

% Circumvent problems when only 1 image is selected.
switch true
    case ischar(upref.file)     % True for a single image
        upref.file = {upref.file};  % Convert to cell
    case iscellstr(upref.file)  % True for multiple images
        % Do nothing. upref.file already in correct format.
    otherwise                   % Error construct
        error('r1dO:switchOptions', 'OOPS \n Unknown declaration of file name');
end

% --------------------------------------------------------------------------------------------------

%% Main: Read the data-file(s)
% ----------------------------
% Pre-allocate variables which grow during the iterations (performance and faster out of memory).
data = zeros( input.rows, input.cols, length(upref.file), upref.data_class);
if isequal(input.bands,2);
    data2=data;
end % Case for multiband input

% Display a waiting bar.
h = waitbar(0,['Reading ', num2str(length(upref.file)), ' images. Please wait...']);
% Reading multiple files can only be done via a loop (as far as I know).
% The function ENVI_READ takes care of correctly reading the input files, and
% returning the correct pages in the array.
for ind = 1:length(upref.file)
    tmp = envi_read( fullfile( upref.dir, upref.file{ind}),...
        input.rows, input.cols, input.bands, input.input_class);
    data(:,:,ind) = tmp(:,:,1);
    if isequal(input.bands,2);
        data2(:,:,ind) = tmp(:,:,2);
    end % Case for multiband input
    % Update waitingbar
    waitbar(ind/length(upref.file));
end
% Close waitingbar
close(h)
% --------------------------------------------------------------------------------------------------

%% Main: Error checking
% ---------------------
% See if any file handle is still open
fopen('all')
% --------------------------------------------------------------------------------------------------

%% Main: Apply user settings
% --------------------------
% Replace zeros with NaN's
switch upref.apply_NaN
    case 'yes'
        data(data==0) = NaN;
    case 'no'
        % Do Nothing.
    otherwise
        error( 'R1dO:switchOptions', 'OOPS \n Cannot switch to requested NaN-setting: %s',...
            upref.apply_NaN);
end

% Block division
% Define the maximum number of rows/columns inside a block.
upref.rows_per_yblock = ceil(input.rows/upref.yblocks);
upref.cols_per_xblock = ceil(input.cols/upref.xblocks);
% Define the arrays which tell mat2cell how to divide the matrix. Allow
% for last row/column-block to be smaller due to the rounding above.
upref.rowdist = [ repmat(upref.rows_per_yblock, 1 , upref.yblocks - 1) ,...
    input.rows - upref.rows_per_yblock * (upref.yblocks -1)];
upref.coldist = [ repmat(upref.cols_per_xblock, 1 , upref.xblocks - 1) ,...
    input.cols - upref.cols_per_xblock * (upref.xblocks -1)];
% Use mat2cell to do the partitioning.
data = mat2cell( data, upref.rowdist, upref.coldist, length(upref.file));
% --------------------------------------------------------------------------------------------------

%% Output: Store dataset(s) on disk
% ---------------------------------
% Since there are three options this becomes a giant switch-case construct.
switch upref.storage
    case 'none'     % No storage required.
        % Nothing to see here ... please move on.
    case 'mat'      % Store the complete workspace.
        % Propose the following name to the user:
        output.file = {['workspace_of_' datestr(date,29) ]};
        % Using a GUI let the user decide where to put the result.
        [output.file,output.dir] = uiputfile('*.mat',...
            'Where to save the -v7.3 mat file',...
            fullfile( pwd, gui.dataset.basePath, 'LST.mat',output.file{1}));
            % This overrules the proposed name.
        % Store the file.
        % Most own code depend on the matfile() functionality when using
        % huge datasets, indexing into cells is not supported by that
        % function.
        if max(upref.xblocks,upref.yblocks) <= 1
                data=cell2mat(data);
        end
        save( fullfile(output.dir, output.file), 'data', 'input', 'upref','-v7.3');
    case 'ssa-mtm'  % Store as ASCII file in ssa-compatible format.
        % Propose the following base-name to the user:
        output.basename = ['TimeSerie_', num2str(length(upref.file)),...
            '_points'];
        % Using a GUI let the user decide where to put the result.
        [output.file,output.dir] = uiputfile('*.data',...
            'Where to save the output data-set(s), filename will act as basename.',...
            fullfile( pwd, gui.dataset.basePath, 'LST.txt',output.basename));
        % Extract basename
        [~,output.basename,~] = fileparts(output.file);

        % Permute the cell to get output-files in the order specified in the
        % header (again matlab runs COLUMNWISE, even for linear indexing).
        data=data';
        % Since there could be multiple blocks this must be a loop.
        for ind = 1:numel(data)
            % Block dependent filename
            if max(upref.xblocks,upref.yblocks) <= 1
                output.file = [output.basename,'.data'];
            else
                output.file = [output.basename, '_block_',...
                    num2str(ind,'%02d'), '_of_', num2str(numel(data)),...
                    '_[', num2str(upref.xblocks), 'x',...
                    num2str(upref.yblocks), '].data'];
            end
            % Cast the block into the correct matrix ordering
            % Start by permuting the block to a time*column*row array.
            data{ind} = permute(data{ind}, [3,2,1]);
            % Add each original row to the end of the previous one.
                % Error checking has already been performed during the reading
                % phase, hence it is ok to rely on the matrix dimensions inside
                % each cell.
            data{ind} = reshape(data{ind}, size(data{ind},1),...
                size(data{ind},2)*size(data{ind},3) );

            % Finally write to disk, each value has 2 decimal places.
            % "dlmwrite" is not used on purpose. Since it loops on formatting
            % hence it runs much slower than the code below (approx 25 times).
            % Open a file handle to a writable new file:
            fid = fopen( fullfile( output.dir, output.file ), 'w');
            % Write the whole matrix in one go (note the data transpose,
            % which is necessary since Matlab reads column wise).
            fprintf(fid, [repmat('%3.3f\t', 1, size(data{ind},2)-1) '%3.3f\n'],data{ind}' );
            % close the file-handel
            fclose(fid);
        end
    otherwise
        error('r1dO:switchOptions',...
            'OOPS \n Cannot switch to requested storage type: %s',upref.storage);
end

%% Senna
% % ------
% % Just because it is possible ;-)
% % Ok we first need to replace "_" in the file names to "\_", otherwise
% % Matlab's TeX interpreter goes wild.
% plotfiles = strrep(upref.file,'_','\_');
% % Obtain colorbar limits, for consistency over all images.
% limits = [ min(min(min(data{1}))) ,  max(max(max(data{1})))];
%
% figure(1), hold off
%
% for ind = 1:length(upref.file);
%     imagesc(data{1}(:,:,ind),limits);
%     axis image
%     title(['File: ',plotfiles(ind)])
%     colorbar
%     pause(0.75)
% end
% clear plotfiles

%% Create video
% lets see if this works:
if strcmp(upref.createMovie,'yes');   % Create a movie
    uiwait(warndlg({'This function needs more work, especially on'...
        'setting the background to white','preferably move it to a'...
        'standalone function'}, 'Create_Video() Needs work!','modal'));
    % First define a filename (Propose it to the user).
    outMovie.file = 'testmovie.avi';
    % Using a GUI let the user decide where to put the movie.
    [outMovie.file,outMovie.dir] = uiputfile('*.avi',...
        'Where to save the movie',...
        fullfile( pwd, gui.dataset.processedPath,'movies',outMovie.file));
        % Overrules proposed name. "upref.dir" points to last visited directory.

    % Prepare and open movie file
    outMovie.object = VideoWriter([outMovie.dir outMovie.file]);
%    outMovie.object = VideoWriter([outMovie.dir outMovie.file],'Uncompressed AVI');
%    outMovie.object = VideoWriter([outMovie.dir outMovie.file],'Archival');
    outMovie.object.FrameRate = 1.33;   % Display each image for 3/4th of a second.
    outMovie.object.Quality = 95;
    open(outMovie.object);              % Open the file for writing

    % Create animation by plotting the area.
    % Obtain colorbar limits, for consistency over all images.
    %outMovie.limits = [ min(min(min(data{1}))) ,  max(max(max(data{1})))];
    % Define colorbar limits, since statement above is influenced by outliers.
%    outMovie.limits = [225 , 325];
    outMovie.limits = [240 , 300];
    % Define plot-titles
    plotfiles = strrep(upref.file,'FY2C_FDI_ALL_NOM_', ''); % Removes first part
    plotfiles = strrep(plotfiles,'.raw', '');               % Removes extention part

    % Define figure.
    fig = figure(1); hold off
    % Loop over all images.
    for ind = 1:length(upref.file);
        imagesc(data{1}(:,:,ind),outMovie.limits);
         axis image
         title(plotfiles(ind));
         colorbar
        % Write each frame to the file.
        currFrame = getframe(fig);
        writeVideo(outMovie.object,currFrame);
    end

    % Close the file
    close(outMovie.object);
else
    % Do nothing
end

%% Clean up
% Remove unnecessary variables
clear ind fid
% Let user know the script is finished
fprintf('\n\tScript is finished.\n');
% --------------------------------------------------------------------------------------------------

%% Extra comments
% Section with extra info/todos/remarks
%
% Optimization and extensions which are probably not relevant:
% * Store each image (time point) in a separate structure or field.
%       * This causes a lot of overhead, if a single image is required just
%         select the appropriate band from data (e.g. data(:,:,image_number)).
% * Use memory mapping for larger sets.
%       * Only beneficial in following cases (and only for binary files):
%           * Large files needing random access.
%           * Small files when each is accessed a large number of times.
% * Load multiple data-files including files from multiple-sub-directories.
%       * This is better handled via alternative methods system wise. For
%         instance use symbolic links in a giant "all" directory pointing to
%         the individual data-sets.
% --------------------------------------------------------------------------------------------------
