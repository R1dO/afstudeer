% FY2C_check_dataset
%
% Check if a folder contain the expected amount of FY2C-satellite images .
% With the ability to repair missing images (by inserting zero-valued images).
% Those reparation images will be inserted in the folder by using hard-links
% to an appropriate placeholder file.
% Supported images are:
% * Hourly LST data
% * 8 day albedo data
%
% If missing images are inserted a SUBSTITUTES file will be put in the folder.
%
% We only touch the original files, we do not alter them.
%
% Any (non GUI) user preferences is set in the Cell "User: Define options".
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script. Multiple files can be selected.
% This script expects them under data/ of the current working directory!
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed , by vectorization (if necessary and possible)
% * Casting into a function which can be called from other m-files.
% * Robustness, e.g. validation of input.
% * Adapting GUI style for user definable options (partly implemented).
%
% Known bugs:
% * Some months can report more images than there should be available (month
% overflow).
%
% Limitations:
% * Only numerical month references, are allowed as input.
% * This script will only check the LST images, not the LSE.
% * This script only works on directories.
%
% Disclaimer:
% This script has only been tested under MatlabR2014b in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See the website below for more information regarding ENVI header files:
% http://geol.hu/data/online_help/ENVI_Header_Format.html
%
% See Also ENVI_READ
% Documentation on data representation: FREAD
% ==============================================================================

% ==============================================================================
% Version:  1.1.0
% Matlab:   8.4.0.150421 (R2014b)
% Copyright 2012-2015:   H den Ouden
% Mail:             H.denOuden@student.tudelft.nl   | r1denouden@gmail.com
% ==============================================================================

%% Init: Matlab environment
% ------------------------------
clear variables; close all; clc; % Basic matlab options+
uiwait(warndlg({'This function is not up to par with current repository'...
    'structure.', 'It is a candidate for deletion (after a new script'... 
    'has been provided'}, 'Obsoleted Function','modal'));
% ------------------------------------------------------------------------------

%% Init: User preferences
% -----------------------
% The ONLY section of this script were users should modify variables.

% Define the naming convention (without the date-time part) of the files.
% Uncomment the requested lines
% LST dataset
datelist.nameStart2008_2009 = 'FY2C_FDI_ALL_NOM_';
datelist.nameStart2010 = 'FY2E_FDI_ALL_NOM_';
datelist.nameEnd = 'LST.raw';
% % Albedo dataset
%datelist.nameStart = 'GLASS03ALBEDO.MAB01V1.A';
%datelist.nameEnd = '.dat';

% Define the start path when searching for the location of the dataset.
gui.dataset.basePath = 'data/preprocessed/equidistant_timepoints/FY2C/';
% --------------------------------------------------------------------------------------------------

%% Init: Initialize variables
% ---------------------------
% Needed for correct functionning of this script

% Default behaviour when we are missing some images
input.repair = 'No';

%% Init: GUI user preferences
% ---------------------------
% Ask user some questions about the dataset they are trying to check.

% Define the dialog box for retrieving information about the year
gui.year.title = 'Define the time period you want to check';
gui.year.prompt = {'Which Year?', 'Which (numerical) Month? 0 is all','Sampling in hours (8 days = 192)'};
gui.year.defaults = {'2008', '0','1'};
gui.year.answers = inputdlg(gui.year.prompt, gui.year.title, [1 45], gui.year.defaults);
% Cast dialog answers into input.
input.year = str2double(gui.year.answers{1});                                   % The year
input.month = str2double(gui.year.answers{2});                                  % The month
input.sampling = str2double(gui.year.answers{3});                               % Sampling between images

% Specify path to the data-set (Use a GUI).
gui.dataset.title = ['Select folder which contain month: ', num2str(input.month),'.'];
%gui.dataset.startPath = fullfile( gui.dataset.basePath, 'LST', num2str(input.year));
gui.dataset.startPath = fullfile( pwd, gui.dataset.basePath, 'LST.raw');
input.dataDir = uigetdir(gui.dataset.startPath , gui.dataset.title);

%% Init: Preference checking
% --------------------------
% Check if a year is requested
if isequal(input.month,0)
    fprintf(['\n\t','Checking a complete year.','\n'])
    input.month = 1:12;                                                         % 12 months in a year.
elseif ~isnan(input.month) && input.month <= 12
    fprintf(['\n\t','Checking a single month.','\n'])
else
    error('R1dO:UserError', ['\nOnly numbers between 0 and 12 are allowed.',...
        '\nCannot process: %s'],gui.year.answers{2});
end
% ------------------------------------------------------------------------------

%% Main: Obtain existing raw image file-names
% -------------------------------------------
% Get file listing and request only LST raw-image filenames
tmp = dir([input.dataDir,'/*', datelist.nameEnd]);                  % Only those files ending with 'LST.raw' or '.dat'.
input.names = {tmp.name}';                                          % Cast names into a cell vector

%% Main: Generate file list
% -------------------------
% Generate file list with hourly timepoints in the interval of interrest.
datelist.baseDate = datenum([input.year input.month(1) 01]);                    % Base for naming.
datelist.monthDays = eomday(input.year , input.month);                          % Days per month
%datelist.names = cell( sum(datelist.monthDays)*24/input.sampling, 1);          % Predefine cell list
datelist.names = cell( ceil(sum(datelist.monthDays)*24/input.sampling), 1);     % Predefine cell list, ceil() as ugly hack for integers.

% First part of filename depends partially on year
if isequal(input.year, 2010);
    datelist.nameStart = datelist.nameStart2010;
else
    datelist.nameStart = datelist.nameStart2008_2009;
end % No need for error checking here.

% Display a waiting bar, during the loop.
h = waitbar(0,['Generating ', num2str(length(datelist.names )), ' Filenames. Please wait...']);

% Necessary loop, fills the datelist with all dates and hours.
for ind = 1:length(datelist.names )
    if isequal( mod(input.sampling,24), 0);
        nStartday = datenum(input.year, input.month(1), 1) - datenum(input.year,01,01);
        if isequal( mod(nStartday,8), 0);
        tmp = sprintf('%03.0f',  nStartday + (ind-1) * 8 +1);
        datelist.names{ind} = strcat(datelist.nameStart, num2str(input.year), tmp, datelist.nameEnd);                    % Cast names into a cell vector
        else
        tmp = sprintf('%03.0f',  nStartday - mod(nStartday, 8) + ind * 8 +1);
        datelist.names{ind} = strcat(datelist.nameStart, num2str(input.year), tmp, datelist.nameEnd);   % Cast names into a cell vector
        end % It could happen that a 8th day start on the first day of a month.
    else
        tmp = datestr( addtodate(datelist.baseDate,(ind-1)*input.sampling,'hour'), 'yyyymmddHHMM'); % Temporal variable
        datelist.names{ind} = strcat(datelist.nameStart, tmp, datelist.nameEnd);                    % Cast names into a cell vector
    end     % Dirty check for multi-daily increments or hourly.
    % Update waitingbar
    waitbar(ind/length(datelist.names),h);
end
close(h)                                                                        % Close waitingbar

%% Main: Compare lists
% --------------------
% If there are missing images their names show up in the following list.
missing = datelist.names(~ismember(datelist.names , input.names));

%% Main: Ask user what to on missing files
% ----------------------------------------
% Check if we actually have missing values, and decide wat to do.
if isempty(missing)                                                             % No missing images
    msgbox('No missing images found.','Validation result')
elseif ~isempty(missing)                                                        % Missing images
    % Display a list of missing values.
    gui.missing.title = ['Missing ', num2str(length(missing)), ' images'];
    gui.missing.message = [{'The following images are missing:' ; ''} ; missing];
    gui.missing.icon = 'warn';
    listdlg('ListString', gui.missing.message(3:end) , 'Name',...
        gui.missing.title , 'PromptString', gui.missing.message(1),...
        'ListSize', [320,768]);
    % Ask user if we should repair the file
    gui.repair.title = 'Repair missing images';
    gui.repair.question = ['Should we substitute those with ',...
                            num2str(length(missing)), ' zero valued images?'];
    input.repair = questdlg(gui.repair.question, gui.repair.title);
else
    error('r1dO:switchOptions',...
        'OOPS \n Something went wrong on checking for missing values.');
end

%% Output: Generate zero-valued images
% ------------------------------------
% Check if we want to generate those values
switch input.repair
    case 'Yes'                                                                  % We want output
        % Define the image properties
        gui.image.title = 'Properties of the output images';
        gui.image.prompt = {...
            'How many rows has each image (y-axis) ?',...
            'How many colums has each image (x-axis) ?',...
            'What is the data representation?'};
        gui.image.defaults = {'408','708','float32'};
        gui.image.answers = inputdlg(...
            gui.image.prompt, gui.image.title, 1, gui.image.defaults);
        % Cast answers into input.
        input.rows = str2double(gui.image.answers{1});                   % Image rows (y-axis)
        input.cols = str2double(gui.image.answers{2});                   % Image columns (x-axis)
        input.class = gui.image.answers{3};                              % Data representation
        % Write the zero-valued images (use a loop)
        % Display a waiting bar.
        h = waitbar(0,['Writing ', num2str(length(missing)),...
            ' ENVI raw files. Please wait...']);
        for ind = 1:length(missing);
            % Define filename and path
            loc = fullfile( input.dataDir, missing{ind} );
            % ENVI_WRITE takes care of correctly writing to disk.
            envi_write( zeros(input.rows,input.cols,'single') , loc , 'both');
            % Update waitingbar
            waitbar(ind/length(missing));
        end
        % Close waitingbar
        close(h)

        % Write the substitutes file in the same directory, informing the user which files
        % are generated.
        loc = fullfile( input.dataDir, 'SUBSTITUTES' );
        fid = fopen(loc , 'wt'); % Writable file-handle which accepts tekst.
        fprintf(fid,...
            ['The %d files mentioned here were generated.\n',...
            'This was needed to provide a continious sampling interval.\n',...
            'They are all zero-valued.\n\n'], length(missing) );
        fprintf(fid, '%s\n', missing{:});
        fclose(fid);

    otherwise                                                                   % We do not want output
        fprintf(['\n\t','Missing images are not repaired.','\n'])
end

%% Main: Error checking
% ---------------------
% % See if any file handle is still open
fopen('all')
% % --------------------------------------------------------------------------------------------------

%% Clean up
% Remove unnecessary variables
clear ind h tmp
% --------------------------------------------------------------------------------------------------

%% Extra comments
% Section with extra info/todos/remarks
%
% Optimization and extensions which are probably not relevant:
% * Store each image (time point) in a separate structure or field.
%       * This causes a lot of overhead, if a single image is required just
%         select the appropriate band from data (e.g. data(:,:,image_number)).
% * Use memory mapping for larger sets.
%       * Only beneficial in following cases (and only for binary files):
%           * Large files needing random access.
%           * Small files when each is accessed a large number of times.
% * Load multiple data-files including files from multiple-sub-directories.
%       * This is better handled via alternative methods system wise. For
%         instance use symbolic links in a giant "all" directory pointing to
%         the individual data-sets.
% --------------------------------------------------------------------------------------------------
