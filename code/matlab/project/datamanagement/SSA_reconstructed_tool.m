% SSA_reconstructed_tool
%
% Script which reads the SSA reconstructed ASCII files and cast them into a 3D
% array, where each page is an image for a given time point.
%
% We only touch the original files, we do not alter them.
%
% Any user preferences is set in the Cell "User: Define options".
% The following user preferences are implemented:
% * Store the data in a Matlab .mat file (Using the v7.3 file specification).
% * Store in ENVI binary file.
% * If several files are selected it is assumed that they correspond to
%   different blocks of the actual image.
%   The partitioning code expect the following ID numbering scheme (example uses
%   4 vertical and 3 horizontal blocks).
%   --                   --
%   | [1]  [2]  [3]  [4]  |
%   | [5]  [6]  [7]  [8]  |
%   | [9]  [10] [11] [12] |
%   --                   --
%
% Input and output files are handled via a GUI-dialog. It is not needed to
% specify them in this script. Multiple files can be selected (they need not be sorted).
%
% Optimization and extensions might be possible on the following subjects;
% * Memory usage / speed
%   (e.g. load into a uint16 data-type, factor 2 reduction w.r.t single).
% * Casting into a function which can be called from other m-files.
% * Robustness, e.g. validation of input (e.g. matrix dimensions).
%
% Known bugs:
% * None Yet.
%
% Disclaimer:
% This script has only been tested under MatlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
%
% See the website below for more information regarding ENVI header files:
% http://geol.hu/data/online_help/ENVI_Header_Format.html
%
% For documentation on binary read:
% See Also FREAD
% ==================================================================================================

%% Initialize environment
% -----------------------
clear variables; close all; clc; % Basic Matlab options
% --------------------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% The ONLY section of this script where users should modify variables.

% Define the image size of the output array (e.g. size of each page/band).
output.rows = 408;          % Number of image rows (y-axis)
output.cols = 708;          % Number of image columns (x-axis)
output.bands = 744;          % Number of time-points in the OUTPUT 3D array (e.g. number of time-points).
output.class = 'single';    % Store values with which accuracy?

% Define user preferences.
% Specify how the blocks partition scheme was set-up.
upref.xblocks = 3;            % Partition image in # of vertical blocks?
upref.yblocks = 4;            % Partition image in # of horizontal blocks?
% IMPORTANT The values should be equal to the ones created by: "SSA_raw_tool.m"

% Specify how the 3D-array should be stored on disk.
% Uncomment the required storage. Only one is allowed, if multiple lines
% are uncommented only the last one is used.
upref.storage = 'ssa-mtm';    % Store the data in a single ASCII file with the format xxx.yyy
%upref.storage = 'mat';        % Store the whole workspace in a single Matlab ".mat" file.
%upref.storage = 'ENVI';       % Store all bands in a single ENVI binary file (BSQ).
%upref.storage = 'ENVImulti';  % Store as multiple single band images (like the original dataset).
%upref.storage = 'none';       % Do not store any data on disk.

% Define the start path when searching for the location of datasets.
gui.dataset.basePath = 'data/preprocessed/spatial_subsets/FY2C/LST.txt';
gui.dataset.originalsPath = 'data/preprocessed/equidistant_timepoints/FY2C/LST.raw';
gui.dataset.processedPath = 'data/processed/SSA/test_capability/FY2C/';
% --------------------------------------------------------------------------------------------------

%% Init: Specify the ASCII reconstructed data-set(s)
% --------------------------------------------------
% Specify file and path to the data-set (Use a GUI).
[upref.file, upref.dir] = uigetfile( ...
    {'*.data; *.rec', 'ssa-ASCII style data (*.data, *.rec)';...
    '*.rec', 'ssa-ASCII-reconstructed (*.rec)';...
    '*.*', 'All files (*.*)'}, ...
    'Please select the reconstructed blocks.', gui.dataset.basePath, 'MultiSelect','on');

% In order to circumvent problems when only 1 block is selected the
% following "switch -- case" construct is necessary.
switch true
    case ischar(upref.file)     % True for a single block
        upref.file = {upref.file};  % Convert to cell
    case iscellstr(upref.file)  % True for multiple blocks
        % Do nothing. upref.file already in correct format.
    otherwise                   % Error construct
        error('r1dO:switchOptions', 'OOPS \n Unknown declaration of file name');
end

% Error construct to see if number of selected channels equals the expected
% ones.
if length(upref.file) == upref.xblocks*upref.yblocks;
    % Do nothing, this should be correct.
else
    error( 'R1dO:InputOutput:BlockCount',['\nThe partition scheme from upref.xblocks'...
        ' and upref.yblocks (%d) differ from the number of block-files read (%d).\n'],...
        upref.xblocks*upref.yblocks, length(upref.file));
end

% --------------------------------------------------------------------------------------------------

%% Init: Guess how the blocks are partitioned.
% --------------------------------------------
% Block division (from "SSA_raw_tool.m")
% Define the maximum number of rows/columns inside a block.
upref.rows_per_yblock = ceil(output.rows/upref.yblocks);
upref.cols_per_xblock = ceil(output.cols/upref.xblocks);
% Define the arrays which dictated how mat2cell divided the matrix. Allow
% for last row/column-block to be smaller due to the rounding above.
upref.rowdist = [ repmat(upref.rows_per_yblock, 1 , upref.yblocks - 1) ,...
    output.rows - upref.rows_per_yblock * (upref.yblocks -1)];
upref.coldist = [ repmat(upref.cols_per_xblock, 1 , upref.xblocks - 1) ,...
    output.cols - upref.cols_per_xblock * (upref.xblocks -1)];
% Cast those values in a matrix with same dimensions as the cell-array used for
% storing the datafile(s). (note the permute, ugly hack)
upref.rowdist = repmat(upref.rowdist',1,upref.xblocks)';
upref.coldist = repmat(upref.coldist,upref.yblocks,1)';

%% Main: Read the data-file(s)
% ----------------------------
% Note that each ssa-mtm data file has following format:
% (lines of time-points) * (columns*rows of an image).

% Preallocate the cell array with the number of blocks and their ordering.
% While we are at it. Also preallocate the numeric arrays inside each cell,
% using ssa-mtm style.
% Start by defining an anonymous function which tells "arrayfun" the size of the
% numeric array to store in each cell.
preAll = @(Crows,Ccols) zeros(output.bands,Crows*Ccols,'single');
% Create the actual cell (but permuted) array.
data = arrayfun(preAll, upref.rowdist, upref.coldist,'uni',false);
% Make sure the filenames are in ascending order (prevents wrong output, when the input
% files are created in random order (and timebased selection).
upref.file = sortrows(upref.file(:));

% Reading multiple files can only be done via a loop (as far as I know).
% Display a waiting bar.
h = waitbar(0,['Reading ', num2str(length(upref.file)), ' ssa-mtm ASCII files. Please wait...']);
for ind = 1:length(upref.file)
    % Try to directly read using the load command.
    data{ind} = single( load( fullfile( upref.dir, upref.file{ind} )));
    % Convert each ssa-style matrix to the corresponding 3D-array.
    % CEIL() is used in combi with "ind" to get correct size from
    % "upref.coldist" and "upref.rowdist".
    % Note: cols and rows are swapped once more.
    data{ind} = reshape( data{ind}', upref.coldist(ind),...
        upref.rowdist(ind), output.bands);
    % Finalize by permuting the block to a row*col*time array.
    data{ind} = permute(data{ind}, [2,1,3]);

    % Update waitingbar
    waitbar(ind/length(upref.file));
end
% Close waitingbar
close(h)

% Smash all blocks together into the desired 3D array. Again note the transpose.
data = cell2mat(data');

fprintf('\ndata-files loaded and glued together.\n');
% --------------------------------------------------------------------------------------------------

%% Output: Store data-set(s) on disk
% ----------------------------------
% Since there are five options this becomes a giant switch-case construct.
switch upref.storage
    case 'none'         % No storage required.
        % Nothing to see here ... please move on.
    case 'mat'          % Store the complete workspace.
        % Propose the following name to the user:
        output.file = {['workspace_of_' datestr(date,29) ]};
        % Using a GUI let the user decide where to put the result.
        [output.file,output.dir] = uiputfile('*.mat',...
            'Where to save the -v7.3 mat file', [gui.dataset.processedPath output.file{1}]);
            % This overrules the proposed name. "upref.dir" points user to last
            % visited directory.
        % Store the file.
        save( char([output.dir output.file]), 'data', 'gui', 'upref','-v7.3');
    case 'ENVI'         % Store as a single ENVI binary (BSQ) image.
        % Propose the following name to the user:
        output.file = ['Reconstructed_Timeserie_' num2str(output.bands) '_TimePoints.raw'];
        % Using a GUI let the user decide where to put the result.
        [output.file,output.dir] = uiputfile('*.raw',...
            'Where to save the output data-set(s)', [gui.dataset.processedPath, output.file]);
            % This overrules the proposed name. "upref.dir" points user to last
            % visited directory.
        % The function ENVI_WRITE takes care of correctly permuting the data.
        % And write the correct array to disk (inluding the header file).
        stats = envi_write(data, fullfile( output.dir, output.file ));
        % display STATS as a checking mechanism.
        disp(stats)
    case 'ENVImulti'    % Store as multiple single band images.
        % Ask user for the original filenames, they serve as a basename for the
        % reconstructed filenames. UIGETFILE is used since it checks for
        % existence. UI
        hlp = helpdlg({'In the dialog box please select the original GAPPY images.',...
            'These will serve as basenames for the the reconstructed images.',...
            'The original files will NOT be overwritten.'},...
            'Help for: Select original ENVI binary images');
        output.file = uigetfile(...
            {'*.raw', 'ENVI binary images (*.raw)'; '*.*', 'All files (*.*)'}, ...
            'Select original ENVI binary images (for naming scheme only).',...
            gui.dataset.originalsPath, 'MultiSelect','on');
        % Check if number of filenames equal number of bands.
        if isequal(size(data,3),length(output.file));
            fprintf('\n\t%d images will be written to disk',length(output.file));
        else
            error('R1dO:ioNumberOfFiles',...
                '\n\tOOPS\n\t Number of outputfiles (%d) do not match image bands (%d)',...
                length(output.file), size(data,3));
        end
        % Add "_rec" to the basenames.
        output.file = regexprep(output.file,'\.\w*','_rec.raw');
        output.file_hdr = regexprep(output.file,'\.\w*','.hdr');
        % Let user decide in which directory the reconstructed images are stored.
        output.dir = uigetdir(gui.dataset.processedPath,'Select the directory to store the images');
        % Close help dialog.
        close(hlp)

        % Writing multiple files can only be done via a loop (as far as I know).
        % Display a waiting bar.
        h = waitbar(0,['Writing ', num2str(length(output.file)),...
            ' ENVI raw files. Please wait...']);
        for ind = 1:length(output.file);
            % ENVI_WRITE takes care of correctly writing to disk (including a header file).
            envi_write(  data(:,:,ind) , fullfile( output.dir, output.file{ind} ) , 'both' );
        % Update waitingbar
        waitbar(ind/length(output.file));
        end
        % Close waitingbar
        close(h)
    case 'ssa-mtm'      % Store as ASCII file in ssa-compatible format.
        % Propose the following base-name to the user:
        output.file = ['Reconstructed_TimeSerie_with_' num2str(output.bands)...
            '_timepoints.data'];
        % Using a GUI let user decide where to put the result.
        [output.file,output.dir] = uiputfile('*.data',...
            'Where to save the output data-set(s)',...
            [gui.dataset.processedPath, output.file]);
            % This overrules the proposed name. "upref.dir" points user to last
            % visited directory.
        % Cast the block into the correct matrix ordering.
        % Start by permuting the block to a time*column*row array.
        data = permute(data, [3,2,1]);
        % Add each original row to the end of the previous one.
        data = reshape(data, output.bands, output.rows*output.cols);
        % Write file to disk using 2 decimal places.
        % "dlmwrite" is not used on purpose. Since it loops on formatting hence
        % it runs much slower than the code below (approx 25 times).
        % Open a file handle to a writable new file:
        fid = fopen( fullfile( output.dir, output.file ), 'w');
        % Write the whole matrix in one go (note the data transpose, it is
        % necessary since Matlab reads column wise while fprint writes row-wise).
        fprintf(fid, [repmat('%3.3f\t', 1, size(data,2)-1) '%3.3f\n'],data' );
        % close the file-handle
        fclose(fid);

    otherwise
        error('r1dO:switchOptions',...
            'OOPS \n Cannot switch to requested storage type: %s',upref.storage);
end

%% Senna
% ------
% % Just because it is possible ;-)
% figure(1), hold off
% for ind = 1:output.bands
%     imagesc(data(:,:,ind));
%      title(['Image of timepoint ',num2str(ind)])
%      colorbar
%     pause(0.75)
% end
% clear plotfiles

%% SHOW  blocks
% ------
% Just because it is possible ;-)
uiwait(warndlg({'The next image is a simple demonstration.',...
    'It does NOT use the actual division'}, 'Needs work',...
    'modal'));

figure(2)
    imagesc(data(:,:,1));
     title(['Image of timepoint ',num2str(1)])
     colorbar
    pause(0.75)
hold on
plot([177 177], [0.5 408.5])
plot([354 354], [0.5 408.5])
plot([531 531], [0.5 408.5])

plot([0.5 708.5], [102 102])
plot([0.5 708.5], [204 204])
plot([0.5 708.5], [306 306])
clear plotfiles

%% Clean up
% Remove unnecessary variables
clear ind
% --------------------------------------------------------------------------------------------------

%% Extra comments
% Section with extra info/todos/remarks
%
% Optimization and extensions which are probably not relevant:
% * Store each image (time point) in a separate structure or field.
%       * This causes a lot of overhead, if a single image is required just
%         select the appropriate band from data (e.g. data(:,:,image_number)).
% * Use memory mapping for larger sets.
%       * Only beneficial in following cases (and only for binary files):
%           * Large files where random access is needed.
%           * Small files which are accessed a large number of times.
% * Load multiple data-files including files from multiple-sub-directories.
%       * This is better handled via alternative methods system wise. For
%         instance use symbolic links in a giant all directory pointing to
%         the individual data-sets.
% --------------------------------------------------------------------------------------------------
