% Write data-file
% ------------------------
uiwait(warndlg({'This function is rather slow (due to '' dlmwrite() ''),',...
    'It is a candidate for deletion (after a new script'...
    'has been provided'}, 'Obsoleted Function','modal'));
% ----------------------------------------------------------
% Propose the following name to the user:
output.file = '2008_01_minvalues_time_index';

% Define the start path when searching for the location of datasets.
gui.dataset.processedPath = 'data/preprocessed/spatial_subsets/FY2C/LST.txt';

% 
% Using the inputs GUI let user decide where to put the result.
[output.file,output.dir] = uiputfile('*.data','Where to save the output data-set',...
    fullfile( pwd, gui.dataset.processedPath, output.file));
    % This overrules the proposed name. "original.dir" points user to last visited directory.

% Store data-set in the requested file, each value is written with 3 decimal
% places (equal to the input file).
dlmwrite( fullfile( output.dir,output.file), data , 'delimiter', '\t','precision', '%.3f');

% Quick test to see if the file was written correctly (taking into account
% numerical errors)
% if max(output.data - load( fullfile( output.dir,output.file))) <= eps(min(output.data))
%     disp({'The subset is successfully written to:'; fullfile(output.dir,output.file)});
% else
%     error('r1dO:writefile',['It looks like something went wrong during the write/read cycle\n',...
%         'Please review this script'])
% end
