% statusReportPlots
%
% This script contains the commands to recreate the plots from the status report.
%
% It is designed to run in one go, but if desired per-cell evaluation is
% possible (need to evaluate the "User:" and all "Init:" cells first).
%
% It follows the following scheme:
% * Load required data (derivatves) and put them in a variable.
% * For each figure (e.g. cell)
%   * Create and decorate the plot
%   * Optional: Export the figure to the currrent working directory.
%   * Output limit-values in tabular format.
%
% See also: export_fig
% ==================================================================================================

%% Start script (clean)
% ---------------------
clear variables; close all; clc;  % Basic matlab options
% --------------------------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% The ONLY section of this script were users should modify variables.

% Should we create outputs of the plots (true | false)?
createSVG = false;
createPDF = false; % Obsolete, using SVG now.

% Define default locations.
location.matfile = 'data/preprocessed/equidistant_timepoints/FY2C/LST.mat';
location.outputFigures = 'data/processed/dataset_information/FY2C/LST/figures';
% --------------------------------------------------------------------------------------------------

%% Init: Load datasets
% --------------------
% November 2008
load( fullfile( location.matfile, 'by-month/11/2008/missing_is_NaN_200811.mat'), 'derivatives');
derivativesNov = derivatives;
% The year 2008
load( fullfile( location.matfile, 'by-year/2008/missing_is_NaN_2008all.mat'), 'derivatives');
derivativesAll = derivatives;
clear derivatives
% --------------------------------------------------------------------------------------------------

%% Init: Default plot/table properties
% ------------------------------------
% These properties will affect all plots.

% White canvas
set(groot, 'defaultFigureColor', 'white');
% Default renderer
set(groot, 'defaultFigureRenderer','painters');
% Use a minimum textsize of 11
set(groot, 'defaultTextFontSize',11);
% A4 is standard in here.
set(groot, 'defaultFigurePaperType','A4');

% Default colormap
%set(0, 'defaultFigureColormap', jet);

% Print table header (on a new line).
fprintf(1, '\n\tFigure: \t\t\t| min value \t| max value \n');
% --------------------------------------------------------------------------------------------------

%% Plot: mean LST values
% ----------------------
% November 2008
fig = 1;
h{fig} = figure('name','November 2008: mean LST');
    imagesc_with_colorbar(derivativesNov.lstMeanPerPixel, ...
        'BarLimits', [230 310]);
    title('mean LST values for November 2008');
filename{fig} = 'general/lstMean2008-11';

% Year 2008
fig = fig +1;
h{fig} = figure('name','Year 2008: mean LST');
    imagesc_with_colorbar(derivativesAll.lstMeanPerPixel, ...
        'BarLimits', [230 310]);
    title('mean LST values for the year 2008');
filename{fig} = 'general/lstMean2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: mean LST \t| %3.2f [K] \t| %3.2f [K]\n', ...
    min(min(derivativesNov.lstMeanPerPixel)), max(max(derivativesNov.lstMeanPerPixel)) );
fprintf(1, '\tThe Year 2008: mean LST \t| %3.2f [K] \t| %3.2f [K]\n', ...
    min(min(derivativesAll.lstMeanPerPixel)), max(max(derivativesAll.lstMeanPerPixel)) );
% --------------------------------------------------------------------------------------------------

%% Plot: Histograms
% -----------------
% bar plot edges does not like inf:
derivativesNov.histogramEdges(end) = 550;
derivativesAll.histogramEdges(end) = 550;
% Bar plot centeres the bin on the Edges-value, hence "+ 0.5" will be added to
% the histogramEdges.

% November 2008
fig = fig +1;
h{fig} = figure('name','November 2008: LST histogram');
    bar(derivativesNov.histogramEdges + 0.5 , derivativesNov.histogram, 1,'b')
        set(gca, 'Xlim', [100 450]);
    title('LST histogram for November 2008');
    xlabel('LST [K]')
    ylabel('Frequency')
filename{fig} = 'histograms/lstHistogram2008-11';

fig = fig +1;
h{fig} = figure('name','November 2008: log LST histogram');
    bar(derivativesNov.histogramEdges + 0.5 , derivativesNov.histogram, 1,'b')
        set(gca, 'YScale', 'log', 'Xlim', [100 450]);
    title('LST histogram for November 2008');
    xlabel('LST [K]')
    ylabel('Frequency')
filename{fig} = 'histograms/lstLogHistogram2008-11';

% Year 2008
fig = fig +1;
h{fig} = figure('name','Year 2008: LST histogram');
    bar(derivativesAll.histogramEdges + 0.5 , derivativesAll.histogram, 1,'b')
        set(gca, 'Xlim', [100 450]);
    title('LST histogram for the year 2008');
    xlabel('LST [K]')
    ylabel('Frequency')
filename{fig} = 'histograms/lstHistogram2008-all';

fig = fig +1;
h{fig} = figure('name','Year 2008: log LST histogram');
    bar(derivativesAll.histogramEdges + 0.5 , derivativesAll.histogram, 1,'b')
        set(gca, 'YScale', 'log', 'Xlim', [100 450]);
    title('LST histogram for the year 2008');
    xlabel('LST [K]')
    ylabel('Frequency')
filename{fig} = 'histograms/lstLogHistogram2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: hist LST \t| No limits calculated \n');
fprintf(1, '\tThe Year 2008: hist LST \t| No limits calculated \n');
% --------------------------------------------------------------------------------------------------

%% Plot: missing pixels per timepoint
% -----------------------------------

% November 2008
fig = fig +1;
h{fig} = figure('name','November 2008: missing pixels per timepoint');
    bar(derivativesNov.nanPixelsPerTimepoint/(408*708)*100, 1);
        axis([0 720 0 101])
        set(gca, 'XTick', (0:48:720));
    title('Percentage of missing pixels per timepoint for November 2008');
filename{fig} = 'gaps/missingPixelsPercentage2008-11';

% Year 2008
fig = fig +1;
h{fig} = figure('name','Year 2008: Missing pixels per timepoint');
    plot(derivativesAll.nanPixelsPerTimepoint/(408*708)*100, 'k.')
        axis([0 8784 0 101])
        set(gca, 'XTick', linspace(0, 8784, 12+1) );
    title('Percentage of missing pixels per timepoint for the year 2008');
filename{fig} = 'gaps/missingPixelsPercentage2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: %%PixelMiss \t| %3.2f %% \t| %3.2f %%\n', ...
    [min(derivativesNov.nanPixelsPerTimepoint), max(derivativesNov.nanPixelsPerTimepoint) ]/(408*708)*100 );
fprintf(1, '\tThe Year 2008: %%PixelMiss \t| %3.2f %% \t| %3.2f %%\n', ...
    [min(derivativesAll.nanPixelsPerTimepoint), max(derivativesAll.nanPixelsPerTimepoint) ]/(408*708)*100 );
% Without the missing images
nanPixelsPerTimepointAlt = derivativesAll.nanPixelsPerTimepoint;
nanPixelsPerTimepointAlt(nanPixelsPerTimepointAlt==(408*708)) = NaN;
fprintf(1, '\tThe Year 2008: %%PixelMissAlt \t| %3.2f %% \t| %3.2f %%\n', ...
    [min(derivativesAll.nanPixelsPerTimepoint), max(nanPixelsPerTimepointAlt) ]/(408*708)*100 );
% --------------------------------------------------------------------------------------------------

%% Plot: missing timepoints per pixel
% -----------------------------------
% November 2008
derivativesNov.percentMissingTimepointsPerPixel = ...
    derivativesNov.nanTimepointsPerPixel/720*100
fig = fig +1;
h{fig} = figure('name','November 2008: missing timepoints per pixel');
    imagesc_with_colorbar(derivativesNov.percentMissingTimepointsPerPixel, ...
        'BarLimits', [0 100]);
    title('Percentage of missing timepoints for November 2008');
filename{fig} = 'gaps/missingTimepointsPercentage2008-11';

% Year 2008
derivativesAll.percentMissingTimepointsPerPixel = ...
    derivativesAll.nanTimepointsPerPixel/8784*100
fig = fig +1;
h{fig} = figure('name','Year 2008: missing timepoints per pixel');
    imagesc_with_colorbar(derivativesAll.percentMissingTimepointsPerPixel, ...
        'BarLimits', [0 100]);
    title('Percentage of missing timepoints for the year 2008');
filename{fig} = 'gaps/missingTimepointsPercentage2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: %%TimepointMiss \t| %3.2f %% \t| %3.2f %%\n', ...
    min(min(derivativesNov.nanTimepointsPerPixel))/720*100, max(max(derivativesNov.nanTimepointsPerPixel))/720*100 );
fprintf(1, '\tThe Year 2008: %%TimepointMiss \t| %3.2f %% \t| %3.2f %%\n', ...
    min(min(derivativesAll.nanTimepointsPerPixel))/8784*100, max(max(derivativesAll.nanTimepointsPerPixel))/8784*100 );
% --------------------------------------------------------------------------------------------------

%% Plot: maximum gap per pixel
% -----------------------------------
% November 2008
fig = fig +1;
h{fig} = figure('name','November 2008: max gap per pixel');
    imagesc_with_colorbar(derivativesNov.maxGapPerPixel, ...
        'BarLimits', [0 500]);
    title('Maximum continuous gap length for November 2008');
filename{fig} = 'gaps/maxGapLength2008-11';

% Year 2008
fig = fig +1;
h{fig} = figure('name','Year 2008: max gap per pixel');
    imagesc_with_colorbar(derivativesAll.maxGapPerPixel, ...
        'BarLimits', [0 500]);
    title('Maximum continuous gap length for the year 2008');
filename{fig} = 'gaps/maxGapLength2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: maxTimeGap \t| %3.2f %% \t| %3.2f %%\n', ...
    min(min(derivativesNov.maxGapPerPixel))/720*100, max(max(derivativesNov.maxGapPerPixel))/720*100 );
fprintf(1, '\tThe Year 2008: maxTimeGap \t| %3.2f %% \t| %3.2f %%\n', ...
    min(min(derivativesAll.maxGapPerPixel))/8784*100, max(max(derivativesAll.maxGapPerPixel))/8784*100 );
% --------------------------------------------------------------------------------------------------

%% Plot: maximum signal per pixel
% -----------------------------------
% November 2008
fig = fig +1;
h{fig} = figure('name','November 2008: max signal per pixel');
    imagesc_with_colorbar(derivativesNov.maxSignalPerPixel, ...
        'BarLimits', [0 720]);
    title('Maximum continuous signal length for November 2008');
filename{fig} = 'gaps/maxSignalLength2008-11';

% Year 2008
fig = fig +1;
h{fig} = figure('name','Year 2008: max signal per pixel');
    imagesc_with_colorbar(derivativesAll.maxSignalPerPixel, ...
        'BarLimits', [0 1000]);
    title('Maximum continuous signal length for the year 2008');
filename{fig} = 'gaps/maxSignalLength2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: maxTimeSignal \t| %3.2f %% \t| %3.2f %%\n', ...
    min(min(derivativesNov.maxSignalPerPixel))/720*100, max(max(derivativesNov.maxSignalPerPixel))/720*100 );
fprintf(1, '\tThe Year 2008: maxTimeSignal \t| %3.2f %% \t| %3.2f %%\n', ...
    min(min(derivativesAll.maxSignalPerPixel))/8784*100, max(max(derivativesAll.maxSignalPerPixel))/8784*100 );
% --------------------------------------------------------------------------------------------------

%% Plot: number of gaps per pixel
% -----------------------------------
% November 2008
fig = fig +1;
h{fig} = figure('name','November 2008: # gap per pixel');
    imagesc_with_colorbar(derivativesNov.amountGapsPerPixel, ...
         'BarLimits', [0 ceil( 720/2 )]);
    title('Number of gaps for November 2008');
filename{fig} = 'gaps/amountGaps2008-11';

% Year 2008
fig = fig +1;
h{fig} = figure('name','Year 2008: # gap per pixel');
    imagesc_with_colorbar(derivativesAll.amountGapsPerPixel, ...
         'BarLimits', ...
         [0 round(max(derivativesAll.amountGapsPerPixel(:))+50, -2)]);  % Max encountered rounded up to 100.
%        'BarLimits', [0 ceil( 8784/2 )]);
    title('Number of gaps for the year 2008');
filename{fig} = 'gaps/amountGaps2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: #Gaps \t\t| %3.0f [-] \t| %3.0f [-]\n', ...
    min(min(derivativesNov.amountGapsPerPixel)), max(max(derivativesNov.amountGapsPerPixel)) );
fprintf(1, '\tThe Year 2008: #Gaps \t\t| %3.0f [-] \t| %3.0f [-]\n', ...
    min(min(derivativesAll.amountGapsPerPixel)), max(max(derivativesAll.amountGapsPerPixel)) );
% --------------------------------------------------------------------------------------------------

%% Plot: average gap per pixel
% ----------------------------
% November 2008
derivativesNov.gaplengthPerPixel = derivativesNov.nanTimepointsPerPixel ...
    ./ (derivativesNov.amountGapsPerPixel + realmin); % Corrected for No-gap
fig = fig +1;
h{fig} = figure('name','November 2008: average gap per pixel');
    imagesc_with_colorbar(derivativesNov.gaplengthPerPixel, ...
        'BarLimits', [0 30]);
    title('Average gap length for November 2008');
filename{fig} = 'gaps/averageGapLength2008-11';

% Year 2008
derivativesAll.gaplengthPerPixel = derivativesAll.nanTimepointsPerPixel ...
    ./ derivativesAll.amountGapsPerPixel;
fig = fig +1;
h{fig} = figure('name','Year 2008: average gap per pixel');
    imagesc_with_colorbar(derivativesAll.gaplengthPerPixel, ...
        'BarLimits', [0 30]);
    title('Average gap length for the year 2008');
filename{fig} = 'gaps/averageGapLength2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: averageTimeGap \t| %3.2f h \t| %3.2f h\n', ...
    min(min( derivativesNov.nanTimepointsPerPixel ./ (derivativesNov.amountGapsPerPixel + realmin) )), ...
    max(max( derivativesNov.nanTimepointsPerPixel ./ (derivativesNov.amountGapsPerPixel + realmin) )));
fprintf(1, '\tThe Year 2008: averageTimeGap \t| %3.2f h \t| %3.2f h\n', ...
    min(min( derivativesAll.nanTimepointsPerPixel ./ derivativesAll.amountGapsPerPixel )), ...
    max(max( derivativesAll.nanTimepointsPerPixel ./ derivativesAll.amountGapsPerPixel )));
% --------------------------------------------------------------------------------------------------

%% Plot: average signal per pixel
% ----------------------------
% November 2008
derivativesNov.signalMap = (720 - derivativesNov.nanTimepointsPerPixel) ...
    ./ derivativesNov.amountSignalFragmentsPerPixel;
fig = fig +1;
h{fig} = figure('name','November 2008: average signal per pixel');
    imagesc_with_colorbar(derivativesNov.signalMap,'BarLimits', [0 720]);
    title('Average signal length for November 2008');
filename{fig} = 'gaps/averageSignalLength2008-11';

% Year 2008
derivativesAll.signalMap = (8784 - derivativesAll.nanTimepointsPerPixel) ...
    ./ derivativesAll.amountSignalFragmentsPerPixel ; 
fig = fig +1;
h{fig} = figure('name','Year 2008: average signal per pixel');
    imagesc_with_colorbar(derivativesAll.signalMap,'BarLimits', [0 20]);
    title('Average signal length for the year 2008');
filename{fig} = 'gaps/averageSignalLength2008-all';

% Table on screen:
fprintf(1, '\tNovember 2008: averageTimeSignal | %3.2f h \t| %3.2f h\n', ...
    min(min( (720 - derivativesNov.nanTimepointsPerPixel) ./ derivativesNov.amountSignalFragmentsPerPixel )), ...
    max(max( (720 - derivativesNov.nanTimepointsPerPixel) ./ derivativesNov.amountSignalFragmentsPerPixel )));
fprintf(1, '\tThe Year 2008: averageTimeSignal | %3.2f h \t| %3.2f h\n', ...
    min(min( (8784 - derivativesAll.nanTimepointsPerPixel) ./ derivativesAll.amountSignalFragmentsPerPixel )), ...
    max(max( (8784 - derivativesAll.nanTimepointsPerPixel) ./ derivativesAll.amountSignalFragmentsPerPixel )));
% --------------------------------------------------------------------------------------------------

%% Plot: average signal per pixel nov2008 log color
% -------------------------------------------------
% November 2008
derivativesNov.logSignalMap = log10(derivativesNov.signalMap);
fig = fig +1;
h{fig} = figure('name','November 2008: average signal per pixel (log color)');
	imagesc_with_colorbar(derivativesNov.logSignalMap, 'BarLimits', [0 3]);
    title(['average signal length for November 2008 ',...
        '(logarithmic color scale)']);
    colorTicks = get(h{fig}.Children(1), 'XTickLabel');
    colorTicks = 10.^str2num(colorTicks);
    set(h{fig}.Children(1),'XTickLabel',colorTicks)
filename{fig} = 'gaps/averageSignalLengthLog2008-11';
% --------------------------------------------------------------------------------------------------

%% Export to files
if createPDF
    for i = 1:length(h)
        print(h{i}, fullfile(location.outputFigures,filename{i}), '-dpdf');
    end
end

if createSVG
    for i = 1:length(h)
        print(h{i}, fullfile(location.outputFigures,filename{i}), '-dsvg');
    end
end

%% Cleanup
reset(groot);

%% END script