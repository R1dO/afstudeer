% HANTS_single_channel.m
%
% Script to test the HANTS capabilities on a specific dataset, with artificially
% introduced gaps.
%
% The dataset consist of single-point images with 16bit integer encoding for
% each time point (ENVI-style). The dataset is selectable using a GUI.
%
% This script performs a HANTS-algorithm analysis on this dataset. Since for now 
% we are interested in its function and performance a complete reconstruction
% (with the max number of components is performed).
%
% The results are then plotted in a single window.
%
% Note:
% * We only touch the original files, we do not alter them.
% * The current matlab-HANTS algorithm does not work wit NaN values.
%
% Disclaimer:
% This script has only been tested under MatlabR2011a in a 64bit Linux
% environment. It is not guaranteed to work on different environments.
% ==============================================================================

%% Basic matlab script options.
% -----------------------------
clear variables; close all; clc; % Basic matlab options
% ------------------------------------------------------------------------------

%% User: Define options
% ---------------------
% The ONLY section in this script were users are allowed to modify variables.
% The INPUT-structure variables should equal the ones of the ENVI header files!

% Define the image size of the input RAW-files.
uinput.rows = 1;        % # of image rows (y-axis)
uinput.cols = 1;        % # of image columns (x-axis)
uinput.bands = 1;       % # of time-points per input image (per INPUT RAW-file). 
    % Normally each time point is a single IMAGE, hence it defaults to ONE.
 
% Specify input data representation. Choose one!
uinput.class = 'int16';    % ENVI 2 = 16-bit signed integer (int16)
    
% Specify parameters for HANTS
% (using the upper limits for the baselength and number of frequencies)
uhants.ni = [];     % No need to specify number of time-samples, it is obtained from the input data.
%uhants.nb = 168;    % Length of the base period (in samples). Largest expected signal.
uhants.nb = 744;    % Length of the base period (in samples). Largest expected signal.
%uhants.nf = 62;    % # of frequencies used for the fitting signal. 
uhants.nf = 338;    % # of frequencies used for the fitting signal. 
                    % If a daily signal has 24 samples this must be >= nb/24 to capture that signal.
uhants.HiLo = 0;    % Reject either none (=0) , high (='Hi') or low (= 'Lo') outliers.
uhants.low = 230;   % Lowest accepted value (K) in time series. Smaller values are rejected.
uhants.high = 320;  % Largest accepted value (K) in time series. Higher values are rejected.
uhants.fet = 5;     % Fit error tolerance (K)
uhants.dod = 1;     % Degree of over-determinacy. Additional samples required for curve fitting.
uhants.delta = 0.1; % Small positive number (e.g. 0.1) to suppress high amplitudes
% --------------------------------------------------------------------------------------------------

%% Init: Specify the binary data-set(s)
% -------------------------------------
data_location = ['./data/preprocessed/spatial_subsets/FY2C/LST.raw/' ...
    'by-month/unknown/r284_c301_25day_original/'];
% Specify file and path to the data-set (Use a GUI). 
[uinput.file, uinput.dir] = uigetfile( ...
    {'LST*', 'ENVI binary images (*.raw)'; '*.*', 'All files (*.*)'}, ...
    'Please select the ENVI binary images.',...
    data_location,...
    'MultiSelect','on');

% Circumvent problems when only 1 image is selected.
switch true
    case ischar(uinput.file)     % True for a single image
        uinput.file = {uinput.file};  % Convert to cell
    case iscellstr(uinput.file)  % True for multiple images
        % Do nothing. upref.file already in correct format.
    otherwise                   % Error construct
        error('r1dO:switchOptions', 'OOPS \n Unknown declaration of file name');
end        
% --------------------------------------------------------------------------------------------------

%% Init: HANTS parameters based on the dataset.
% ---------------------------------------------
uhants.ni = length(uinput.file);    % # of samples.
uhants.ts = 1:uhants.ni;            % Time samples indicators.
% --------------------------------------------------------------------------------------------------

%% Main: Read the data-file(s)
% ----------------------------
% Pre-allocate data (otherwise it grows during the iterations).
data = zeros( uinput.rows, uinput.cols, length(uinput.file), uinput.class);

% Display a waiting bar.
h = waitbar(0,['Reading ', num2str(length(uinput.file)), ' images. Please wait...']);
% Reading multiple files can only be done via a loop (as far as I know).
% The function ENVI_READ takes care of correctly reading the input files, and
% returning the correct pages in the array.
for ind = 1:length(uinput.file)
    data(:,:,ind) = envi_read( fullfile( uinput.dir, uinput.file{ind}),...
        uinput.rows, uinput.cols, uinput.bands, uinput.class);
    % Update waiting bar
    waitbar(ind/length(uinput.file));
end
% Close waitingbar
close(h)

% ENVI_READ gives an array with dimensions 1*1*time points. Cast into a
% column-vector. 
data = permute(data,[3 2 1]);
data = single(data);    % Matlab HANTS algorithm hates integers.

% Create a logical gap mask.
uinput.gaps = data==0;
% --------------------------------------------------------------------------------------------------

%% Main: Apply HANTS procedure
% ----------------------------
% Call HANTS with correct parameters.
[rec.amp,rec.phi,rec.data]= HANTS(...
    uhants.ni, uhants.nb, uhants.nf, data, uhants.ts, uhants.HiLo, uhants.low,...
    uhants.high, uhants.fet, uhants.dod, uhants.delta);
% --------------------------------------------------------------------------------------------------

%% Output: Plotting.
% ------------------
% Before plotting we will replace the zeros in the original dataset with NaN's.
data(uinput.gaps) = NaN;

% Plot the original, reconstructed and their difference in a single figure.
figure('Filename','temperature_differences.svg')
% Divide plot area in 3 horizontal parts and use upper 2 parts for the time series.
subplot(3,1,1:2); hold on; 
 bar(data, 'g', 'EdgeColor', 'g');     % Original bars under reconstructed
 plot(rec.data, 'k-');
    title({'HANTS fit single pixel.';...
        ['Base period = ', num2str(uhants.nb),...
        ', # of frequencies = ', num2str(uhants.nf), '.']})
    ylabel('Temperature (K)')
    legend('Original','Reconstructed','Orientation','horizontal')
 set(gca, 'YLim', [floor(min(rec.data))-2  ceil(max(rec.data))+2],...
     'Xlim', [0 uhants.ni]);     % Set axis limits
% Use lowest horizontal part of plot area for the difference plot.
subplot(3,1,3)
 bar(data-rec.data, 'r', 'EdgeColor', 'g')
    ylabel('Difference (K)')
    xlabel('Time (hours)')
    legend('Original - Reconstructed')
 set(gca, 'YLim', [floor(min(data-rec.data))-2  ceil(max(data-rec.data))+2],...
     'Xlim', [0 uhants.ni]);     % Set axis limits
    

% Create a scatter-plot between the values of the original data-set and the
% reconstructed one. Only one color hence use plot() not scatter().
figure('Filename','temperature_correlation.svg')
 plot(data,rec.data, 'k.')
    title({'HANTS fit single pixel.';...
        ['Base period = ', num2str(uhants.nb),...
        ', # of frequencies = ', num2str(uhants.nf), '.']})
    xlabel('Original (K)')
    ylabel('Reconstructed (K)')
    grid on
 set(gca,...
     'YLim', [floor(min(rec.data(~uinput.gaps)))-2  ceil(max(rec.data(~uinput.gaps)))+2],...
     'Xlim', [floor(min(data))-2  ceil(max(data))+2]);
    
% --------------------------------------------------------------------------------------------------

%% Output: Write text files
% -------------------------
% This section will create ASCII data files from HANTS reconstruction in the same
% directory as the input data files.
result_location = ['./data/processed/HANTS/test_capability/FY2C/'...
    'LST.txt/by-month/unknown/r284_c301_25day_reconstructed/'];

% Lets be nice, and first ask the user if its OK to write the text files.
gui(1).title = 'Write HANTS output to disk';
gui(1).question = sprintf(...
    ['Should we write the output of the HANTS algorithm to disk?\n',...
    'This will create three ASCII files (using 3 decimal accuracy):\n',...
    '\n * The reconstructed time serie:\t"HANTS.rec"',...
    '\n * The reconstructed amplitudes:\t"HANTS.amp"',...
    '\n * The reconstructed phases:\t"HANTS.phase"']);
gui(1).output = questdlg(gui(1).question, gui(1).title);

% Obay the user decission.
switch gui(1).output
    case 'Yes'
        storageLocation = uigetdir( result_location , ...
            'Select the folder to store HANTS output');
        % Write the text files in the same directory as the input files.
        % 1) The reconstructed time series (using 3 decimal places).
        dlmwrite( fullfile( storageLocation,'HANTS.rec'), rec.data, 'delimiter', '\t','precision', '%.3f');
        % The reconstructed time series (using 3 decimal places).
        dlmwrite( fullfile( storageLocation,'HANTS.amp'), rec.amp, 'delimiter', '\t','precision', '%.3f');
        % The reconstructed time series (using 3 decimal places).
        dlmwrite( fullfile( storageLocation,'HANTS.phase'), rec.phi, 'delimiter', '\t','precision', '%.3f');
    otherwise
        fprintf(['\n\t','HANTS output not stored on disk','\n'])
end

%% Clean Up and Inform user the script has finished
clear h ind
fprintf('\n\tThe script %s has finished.\n\n', mfilename);
% ==============================================================================
