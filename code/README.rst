Scripts and toolboxes
=====================
This folder will hold all scripts and programms which are deemed reasonably
mature. In addition any (generated) code documentation will be stored here as
well.

.. contents::

.. note:: Code documentation is not to be confused with written reports which
          are stored under the top-level directory ``//reports/``

Folder structure
----------------
Organization of the top-level directory ``//code/``.

=============  =================================================================
Folder         Contents
=============  =================================================================
`doc/`_        TODO: Documentation related to source code.

               * Generated documentation is not provided.
               * Configuration files for generation *will be* provided.
-------------  -----------------------------------------------------------------
``external``   External (downloaded) programs and code-snippets.
-------------  -----------------------------------------------------------------
``python``     Store python code here (if used). [1]_
-------------  -----------------------------------------------------------------
``matlab``     Store matlab code here (if used). [1]_
-------------  -----------------------------------------------------------------
``bash``       Store shell script code here (if used). [1]_
=============  =================================================================

.. [1] Submodules are allowed here as a means to import own external software
       repositories.

=================================  ==============================================
Scripts                            Description
=================================  ==============================================
``extract_external.sh``            Create the external software folder and
                                   extract its contents from the cloud archives
                                   folder under:

                                   ``//data/published/distributed/...``

``create_doc_archive.sh``          *Optional* script to generate a backup of the
                                   documentation folder if it exists. To provide
                                   the documentation when a user has no access to
                                   the doc-generation tools.
``create_external_archive.sh``     Generate a backup of the external software
                                   folder.
=================================  ==============================================

.. Define hyperlinks
.. _`doc/`: doc/README.rst
