Code documentation
===================
Documentation for written code visible outside the usual code editor (for
example in a webbrowser).
If an editor provides automatic documentation generation functionality it is
best to use the appropriate subfolder for this program.

There is no need for automatically generated code to be stored inside the
repository. Hence expect mostly configuration files here with an occasional
written document [#]_.
For convenience the contents **might** be provided as a separate download.

.. [#] If it is hard to document from within the editor of choice and if the
       resulting files are small, it is allowed to store the code inside the
       repository.

       This is the case with matlab publish function and the desire
       to use a seperate folder to contain the help files (e.g. not in the same
       folder tree as the code).

Folder contents
---------------
Contents of: ``//data/doc/`` and its subdirectories.

+-----------------------------------+------------------------------------------+
| Folders                           | Description                              |
+===================================+==========================================+
| ``html/``                         | Generated documentation suitable for     |
|                                   | webbrowsers.                             |
|                                   |                                          |
| ``html/matlab/``                  |                                          |
| ``html/python/``                  | Subdivided by language                   |
+-----------------------------------+------------------------------------------+
| ``tex``                           | Generated documentation suitable for     |
|                                   | inclusion in other text files or for     |
|                                   | pdf-generation.                          |
| ``tex/matlab/``                   |                                          |
+-----------------------------------+------------------------------------------+
| ``*.rst``                         | Written documentation source that serves |
|                                   | as input for automatic generation.       |
+-----------------------------------+------------------------------------------+
| ``*.conf``                        | Configuration files for the document-    |
|                                   | generation tools.                        |
+-----------------------------------+------------------------------------------+
