Thesis repository of hdenouden
==============================

Use this repository to gain access to the latest state of my thesis.
This includes:

* A location to store data sets (mostly empty but, with instruction how to get
  the sample data sets).
* Reports (including thesis)
* Code (Matlab scripts)
* Documentation (for written code)

.. hint:: See the CHANGELOG_ for a course overview of tagged releases.

.. contents::

Folder structure
----------------
Organization of the project root directory ``//`` [#]_.

=============  =================================================================
Folder         Contents
=============  =================================================================
`code/`_       Location to store (stand-alone) scripts, toolboxes and
               their documentation.
-------------  -----------------------------------------------------------------
`data/`_       Location to store raw and processed data.

               * Original data is not part of this git repository.
               * See folder's README for instructions how to get data.
-------------  -----------------------------------------------------------------
`reports`_     Location containing the source files for generating reports and
               other 'presentable' documents (excluding documentation related to
               source code).

               Uses an external repository via ``gitmodule``
-------------  -----------------------------------------------------------------
`tools/`_       Tools to make life a bit easier (including example files).
-------------  -----------------------------------------------------------------
`work/`_       Use this as a **current working** directory.

               If a software program desires a working directory it is best to
               point to this folder.
-------------  -----------------------------------------------------------------
``.archive/``  Hidden archive directory for obsoleted files having nu purpose
               residing in any of the other sections. They are not needed
               anymore but we don't want to delete them right away.

               It's directory structure is a copy of ``//`` (and it's
               subdirectories) to ease identification of the last known
               'working' location.
=============  =================================================================

Any hyperlinked folder contains a README file to describe its contents (when
applicable), this cascades down to the lowest level.

.. Note:: All scripts assume a basic Linux environment. When additional packages
          are required they will be listed explicitly.

.. Note:: Names not starting with either ``//`` (project root ) or ``../``
          (parent directory) are relative to the current directory of the
          respective README files.

.. [#] It is chosen to use ``//`` as a way to designate the repository root in
       the documentation, since ``/`` is confusing w.r.t. absolute paths (those
       starting from the file-system root).

Submodule usage
---------------
Part of the folders may contain submodules (e.g. imports of own functions that
can be considered general purpose). Since the usage of this is not always trivial
some handy commands are listed below.

* Start using a module (e.g. import from an existing repository).
    .. code-block:: bash

       git submodule add [URL] <name>

  + This will clone the repository from [URL] into a directory <name>,
    check-out it's  master branch, populate a ``.gitmodule`` file and
    add the current ID of the submodule to the index.


  + This is a **one-time** only operation for me, and is not necessary for
    others.
  + This can only be done from the repository root
  + <name> must either be an existing (sub) repository (e.g. <name>.git/
    exists), or not exist at all.
  + Finish process by issuing ``git add``, ``git commit``, ``git push``.
* Initialize the submodule after a fresh clone of the repository
    .. code-block:: bash

       git submodule init
       git submodule update

    + Those commands will initialize, fetch and checkout the appropriate
      commits based on the information stored in ``.gitmodules``.
    + An alternative to do it at clone time is ``git clone --recursive``.
* Getting information on the submodule (while working on the parent).

  + Add the option ``--submodule`` to the command (this should work for most
    basic commands.
* Pull in newest changes from the submodule origin.

  + From inside the submodule directory use ``git fetch`` and ``git merge``.
  + Or use ``git submodule update --remote`` from the parent.

Latest archive update
---------------------
Use the following table to see if it is necessary to download and extract the
cloud stored archives.

========================  =============================  =========================
Name                      Local date                     Cloud date
========================  =============================  =========================
``thesis resources``      |_resources_thesis_SSA.local|  |_resources_thesis_SSA.cloud|
``data/processed/SSA/``   |SSA_processed_local|          |SSA_processed_cloud|
``data/originals/FY2C/``  |FY2C_originals_local|         |FY2C_originals_cloud|
========================  =============================  =========================

.. note:: Sorry ...

          This table only works on a local computer in combination
          with the `Restview <https://mg.pov.lt/restview/>`_ program.

Contact
-------
H. den Ouden (René)
email: R1denouden@gmail.com | h.denouden@student.tudelft.nl

.. Define hyperlinks
.. _CHANGELOG: CHANGELOG.rst
.. _`code/`: code/README.rst
.. _`data/`: data/README.rst
.. _`tools/`: tools/README.rst
.. _`doc/`: doc/README.rst
.. _`work/`: work/README.rst
.. _`reports`: reports/README.rst

.. Define includes
.. include:: data/originals/FY2C/FY2C_originals_time_stamp
.. include:: data/processed/SSA/SSA_processed_time_stamp
.. include:: reports/thesis/_resources/.timestamp-_resources_thesis_SSA_archive.cloud
.. include:: reports/thesis/_resources/.timestamp-_resources_thesis_SSA_archive.local
.. include:: tools/scripts/last-update_SSA_processed
.. include:: tools/scripts/last-update_FY2C_originals
