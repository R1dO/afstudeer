##########
Change Log
##########
.. contents:: :depth: 2

*******
General
*******
Normal releases named after their respective (annotated) repository tag.

[Unreleased][unreleased]
========================
Added
-----

Changed
-------
* It is no longer assumed that data archives are stored under a specific
  cloud provider.

Depreciated
-----------

Removed
-------
* Download script for data archives.

Fixed
-----

0.4.6_ - 29-01-2016
===================
Bump version to 0.4.6, this allows version to (semi) follow the thesis repo.

Added
-----
* IDE (pycharm) - noise (for easy setup on different locations).
* A python test to test if it is possible to read envi files

Changed
-------
* Store images suitable for reports in the submodule and provide links to them.
* Rename submodule folder //doc/ back to  //reports/
* More refactoring of existing scripts to use functions of "mytoolbox"
* Reintegrate reports module (use submodules **per** report type).
* Move archives management scripts to appropriate submodules, and link to them.
* Update ignore rules

Fixed
-----
* Correctly calculate min/max within a day, and diverse other improvements to
  matlab script

0.3.0_ - 2015-07-29
===================
Moved reports into its own repository and import it using ``git submodule``.

Changed
-------
* Moved ``//doc/`` to seperate repositoy, and include that repository.
* Less verbose CHANGELOG table of contents.
* Update matlab scripts to use new function from updated my-matlab-tools
  submodule.
* Allow inclusion of IDE config files (subject to reconsideration).

Removed
-------
* Unnecessay items under .archive/

Fixed
-----
* Missing description for code documentation folder
* Missing header-style text file for HANTS-test data.
* Some maintenance scripts pointed to an obsoleted directory.

0.2.0_ - 2015-06-23
===================
Thesis project reboot.

All previous material has been put in the appropriate places within this
thesis repository. This should solve the last horde preventing me to have fun
doing science.

Added
-----
* This changelog file.
* .gitignore rules.
* Documentation (global and content organisation) via README's.
* Project specific spell=check dictionaries
* Example IDE config file(s).
* Scripts to download and extract external stored date, for 2 reasons:

  * Their (binary) files are to big to store in this repository.
  * It is unsure their contents are allowed to be shared via this repository.
    If it is not allowed their download permissions will be revoked.
* Scripts to create data archives (storage must be done manually).
* Script(s) to generate content (directory structure and hard=links)
* Preliminary example results of SSA processing.
* Archived data (from previous work).

Changed
-------
* Improved directory structure (including README files).
* Default names of generated files (images).
* All reports (including thesis) moved to the ``report`` folder structure.
* All (old) Matlab scripts are moved to the appropriate location under the
  ``code`` folder structure

Depreciated
-----------
* Diverse Matlab functions/scripts.

  Those functions/scripts now contain a warning message indicating they are a
  candidate for removal (after an alternative is provided).

Fixed
-----
* Missing images in reports
* Assumed default locations inside scripts.

Removed
-------
* Obsoleted directories

initial-svn-import_ - 2014-12-19
================================
Added
-----
* Matlab code.
* Old reports (verslag).

*******
Reports
*******
.. note:: Obsoleted, due to usage of external repository for reports.

Describe tags for specific report versions.
Those tags exist to allow quick regeneration of previously 'distributed'
reports.
Due to this nature, only changes regarding the reports will be mentioned
even if such a tag happens between two normal versions. The changes regarding
the reports might show both in this and the previous chapter.


statusreport-v1_ - 2015-05-29
=============================
To start discussing where to go on from now.

.. General hyperlinks
.. _initial-svn-import: https://bitbucket.org/R1dO/afstudeer/commits/tag/initial-svn-import
.. _0.2.0: https://bitbucket.org/R1dO/afstudeer/branches/compare/0.2.0%0Dinitial-svn-import
.. _0.3.0: https://bitbucket.org/R1dO/afstudeer/branches/compare/0.3.0%0D0.2.0
.. _0.4.6: https://bitbucket.org/R1dO/afstudeer/branches/compare/0.4.6%0D0.3.0

.. Reports hyperlinks
.. _statusreport-v1: https://bitbucket.org/R1dO/afstudeer/commits/tag/statusreport-v1

