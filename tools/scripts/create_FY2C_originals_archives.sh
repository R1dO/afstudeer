#!/bin/bash
# Create archive(s) of `//data/originals/FY2C/`.
#
# Be careful when changing script- ENVIRONMENTAL variables.

# Import maintenance functions
source "$(dirname $(readlink -f $0))/maintenance_functions.sh"

####################################
###        User variables        ###
### Overwrites the imported ones ###
####################################

# Allow archive overwriting
# (Activate only when required due to changed source contents).
OVERWRITE_ARCHIVES=false  # ``true`` or ``false`` (default)

# Paths w.r.t repository root
source_folder="data/originals/FY2C/"
archive_base_name="FY2C_originals"

##############
#### MAIN ####
##############
# FY2C
CreateFolderArchive $source_folder $archive_base_name

# Additional ground based data
CreateFolderArchive data/originals/unknown groundstationValidation

exit $?
