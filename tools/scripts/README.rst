==============
Helper scripts
==============
This folder serves as a container for helper scripts.

Folder contents
---------------
Contents of: ``//tools/scripts/``.

.. note::
   All archives are assumed to be stored under ``~/datasets/archives``.

``create_code_external_archives.sh``
   Create archive of the folder: ``//data/code/external/``.

``create_FY2C_originals_archives.sh``
   Create archive of the folder: ``//data/originals/FY2C/``.

``create_reports_thesis_resources_archive``
   Create archive of the folder: ``//data/reports/thesis/_resources``.

``create_SSA_processed_archives.sh``
   Create archive of the folder: ``//data/processed/SSA/``.

``extract_code_external.sh``
   Extract external code archive into the appropriate folder.

``extract_FY2C_originals.sh``
   Extract original FY2C archive into the appropriate folder.

``create_reports_thesis_resources_archive``
   Extract thesis resources archive into the folder:
   ``//data/reports/thesis/_resources``.

``extract_SSA_processed.sh``
   Extract processed SSA archive into the appropriate folder.

``last-update_*``
   Timestamped (with restructured text replace directive) files of last known
   upload.

``maintenance_functions.sh``
   Shell script defining maintenance functions. Sourced by most other scripts.

``populate_preprocessed.sh``
   Generate folders and symbolic links under ``//data/preprocessed`` not
   included in this repository (or in downloadable archives).
