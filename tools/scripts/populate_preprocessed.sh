#!/bin/bash
# Create subfolders of `//data/preprocessed/` and add symbolic links to specific
# targets.
#
# This populates `//data/preprocessed/` with data not provided by this
# repository or dowload archive. Mainly because they are easily generated.
#
# This script creates a sample tree hence much more directories will exist than
# actually needed. Since an empty directory is pretty cheap (storage wise) this
# is not considered problematic.
#
# Not all directories will be populated with links to original data since this
# is not needed at the moment.

# Import maintenance functions
source "$(dirname $(readlink -f $0))/maintenance_functions.sh"

###################################
###       User variables        ###
### Can overwrite imported ones ###
###################################

# Paths w.r.t. repository root
target_folder="data/preprocessed"
source_folder="data/originals"

# Time period
START_YEAR="2008"
END_YEAR="2010"

# Top level folders. Describing the type of preprocessing (to be) applied
top_folders=(
    "equidistant_timepoints"
    "use_NaN_for_missing"
    )
# Data provider
provider="FY2C"
# Different data formats used for storage.
FORMATS=(
    "LST.raw"
    "LST.mat"
    "LST.txt"
    )
###################
#### Functions ####
###################

# ------------------------------------------------------------------- #
# CreateCommonSubfolders ()                                           #
# Creates a common directory structure inside the given folder.       #
# Honouring the desired dataformats used for storage.                 #
# Parameters:                                                         #
#  $1 = Top level folder (describing the kind of content).            #
# Returns:                                                            #
#  0 = success                                                        #
#  $E_NOPERM  = It is not allowed to write in the desired location.   #
# ------------------------------------------------------------------- #
CreateCommonSubfolders ()
{
    # Path manipulation strings
    # Common subfolders for time periods
    years={$START_YEAR..$END_YEAR}
    months={0{1..9},{10..12}}
    periods={"all","by-"{"day","month/"${months}/{all,${years}},"year/"${years}}} # Uses brace expansion !!
    for format in ${FORMATS[@]}; do
        # Create a path string suitable for brace expansion.
        pathstring=${1}/${format}/${periods}
        mkdir -p $(eval echo $pathstring)
    done

}

##############
#### MAIN ####
##############
# Create folder structure
for top_folder in ${top_folders[@]}; do
    CreateCommonSubfolders ${REPO_PATH}/${target_folder}/${top_folder}/${provider}
done

# Create hard links under ``equidistant_timepoints/FY2C/LST.raw/all``
storagepath=${REPO_PATH}/${target_folder}/${top_folders[0]}/${provider}/${FORMATS[0]}/all/
cd $storagepath
for year in $(eval echo {$START_YEAR..$END_YEAR}); do
    ln -P ../../../../../originals/FY2C/LST/${year}/* ./
done
# Hard-links .. e.g. don't be alarmed by output of ``du`` on those directories.
# they might look huge, but since they reference the same file (by inode) actual
# disk usage has not been increased (~5 MB vs ~170 MB for symbolic links).
# Check the second column of ``ls -l`` on a file to observe that the inode count
# went up indicating the same file is referenced from (#) locations.

# Create hard links under ``equidistant_timepoints/FY2C/LST.raw/by-year/2008/``
storagepath=${REPO_PATH}/${target_folder}/${top_folders[0]}/${provider}/${FORMATS[0]}/by-year/2008/
cd $storagepath
ln -P ../../all/*_2008* ./

echo "TODO"
echo "* generate .mat files using matlab (currently manual)."
