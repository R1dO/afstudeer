#!/bin/bash
# Create archives of `//code/external/`.
#
# Be careful when changing script- ENVIRONMENTAL variables.

# Import maintenance functions (and global variables).
source "$(dirname $(readlink -f $0))/maintenance_functions.sh"

####################################
###        User variables        ###
####################################

# Allow archive overwriting
# (Activate only when required due to changed source contents).
OVERWRITE_ARCHIVES=false  # ``true`` or ``false`` (default)

## Paths w.r.t. repository root
source_folder="code/external"
archive_base_name="code_external"

##############
#### MAIN ####
##############
CreateFolderArchive $source_folder $archive_base_name
exit $?
