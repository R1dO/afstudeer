#!/bin/bash
# Extract the contents of `//data/reports/resources/` from the cloud archives.

# Import maintenance functions
source maintenance_functions.sh

####################################
###        User variables        ###
####################################

# Paths w.r.t. repository root
target_folder="code/external"
archive_base_name="code_external"

##############
#### MAIN ####
##############
ExtractFromCloud $target_folder $archive_base_name

exit $?
