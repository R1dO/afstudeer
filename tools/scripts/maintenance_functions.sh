#!/bin/bash
# Source this file in all scripts that need to do maintenance on the repository.
#
# Missing directories are regarded as fatal errors.

# Error and warning values
# ------------------------
E_NOINPUT=74  # C/C++ style exit code for missing file.
E_NOPERM=77   # C/C++ style exit code for permission denied.

# Default values
# --------------
DEFAULT_ARCHIVE_NAME="tmp"

# Assume user has a separate dir to store dataset archives.
ARCHIVE_FOLDER=~/datasets/archives

# Allows overwriting of existing archives (default is false)
# Override this setting either by specifying it in the caller script or by
# exporting it as a shell variable.
OVERWRITE_ARCHIVES=${OVERWRITE_ARCHIVES:-"false"}

# Physical location of the repository root.
# Assumes this script is stored under "//tools/scripts/" directory.
REPO_PATH=$(dirname $(dirname $(dirname $(readlink -f $0))))

# ------------------------------------------------------------------- #
# CreateFolderArchive ()                                              #
# Create a .tar.gz archive of the specified folder and store it in    #
# the ARCHIVE_FOLDER                                                  #
# Parameters:                                                         #
#  $1 = Source folder (if omitted, assume the complete repository).   #
#  $2 = Archive base name                                             #
# Returns:                                                            #
#  0 = success                                                        #
#  $E_NOINPUT = file(s) or folders(s) not found                       #
#  $E_NOPERM  = It is not allowed to overwrite existing archives.     #
# ------------------------------------------------------------------- #
CreateFolderArchive ()
{
    source_path=${REPO_PATH}/$1
    DoesPathExist $source_path
    echo -e "Archiving:\n  $source_path"

    DoesPathExist $ARCHIVE_FOLDER
    echo -e "Storing the archives under:\n  $ARCHIVE_FOLDER"

    archive_base_name=${2:-$DEFAULT_ARCHIVE_NAME}
    archive_base_path=${ARCHIVE_FOLDER}/${archive_base_name}.tar.gz
    if [ ! $OVERWRITE_ARCHIVES = true ] && [ -f "${archive_base_path}" ]
    then
        echo -e "\nWe are not allowed to overwrite:\n  ${archive_base_path}"
        echo "Force overwriting by setting OVERWRITE_ARCHIVES to 'true'"
        return $E_NOPERM
    fi
    echo "Creating archive(s)."

    archive_time_stamp=$(date -u)
    cd $source_path
    # Update time-stamp inside archive (reStructuredText substitution format)
    echo ".. |${archive_base_name}_local| replace:: $archive_time_stamp" >\
         "${archive_base_name}_time_stamp"
    # Create a single archive relative to source folder.
    tar -czpf $archive_base_path *
    # Update time-stamp in repository (reStructuredText substitution format)
    echo ".. |${archive_base_name}_cloud| replace:: $archive_time_stamp" >\
         "${REPO_PATH}/tools/scripts/last-update_${archive_base_name}"
    echo "Done"
}

# ------------------------------------------------------------------- #
# DoesPathExist ()                                                    #
# Check if requested path exist, exit with error if not               #
# Parameters:                                                         #
#  $1 = A required path                                               #
# Returns:                                                            #
#  0 = success                                                        #
#  $E_NOINPUT = file(s) or folders(s) not found                       #
# ------------------------------------------------------------------- #
DoesPathExist ()
{
    if [ ! -d "$1" ]; then
        echo -e "Directory missing:\n  $1"
        exit $E_NOINPUT
    fi
}

# ------------------------------------------------------------------- #
# ExtractFromCloud ()                                                 #
# Extract the cloud-stored archives into the requested target folder. #
# Parameters:                                                         #
#  $1 = Target folder (if omitted, assume the complete repository).   #
#  $2 = Archive base name                                             #
# Returns:                                                            #
#  0 = success                                                        #
#  $E_NOINPUT = file(s) or folders(s) not found                       #
# ------------------------------------------------------------------- #
ExtractFromCloud ()
{
    target_path=${REPO_PATH}/$1
    DoesPathExist $target_path
    echo -e "Extract to:\n  $target_path"

    DoesPathExist $ARCHIVE_FOLDER
    echo -e "Using the archive(s) under:\n  $ARCHIVE_FOLDER"

    archive_base_name=${2:-$DEFAULT_ARCHIVE_NAME}
    archive_base_path=${ARCHIVE_FOLDER}/${archive_base_name}.tar.gz
    if [ ! -f "${archive_base_path}" ]
    then
        echo -e "\nWe cannot find the archive file:\n  ${archive_base_path}"
        echo "Please make sure it exists and the caller script points to the correct location."
        return $E_NOINPUT
    fi
    echo "Extracting the archive(s)."

    # Extract the archive.
    tar -xzpf ${archive_base_path} --keep-newer-files -C ${target_path}
    echo "Done"
}
