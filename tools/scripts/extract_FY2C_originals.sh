#!/bin/bash
# Extract the contents of `//data/originals/FY2C/` from the cloud archives.

# Import maintenance functions
source "$(dirname $(readlink -f $0))/maintenance_functions.sh"

####################################
###        User variables        ###
####################################

# Paths w.r.t. repository root
target_folder="data/originals/FY2C/"
archive_base_name="FY2C_originals"

##############
#### MAIN ####
##############

# FY2C
ExtractFromCloud ${target_folder} ${archive_base_name}

# Additional ground based data
ExtractFromCloud data/originals/unknown groundstationValidation

exit $?
