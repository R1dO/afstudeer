#!/bin/bash
# Extract the contents of `//data/processed/SSA/` from the cloud archives.

# Import maintenance functions
source "$(dirname $(readlink -f $0))/maintenance_functions.sh"

####################################
###        User variables        ###
####################################

# Paths w.r.t. repository root
target_folder="data/processed/SSA/"
archive_base_name="SSA_processed"

##############
#### MAIN ####
##############
ExtractFromCloud $target_folder $archive_base_name

exit $?
