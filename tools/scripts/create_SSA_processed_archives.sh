#!/bin/bash
# Create archive of `//data/processed/SSA/`.
#
# Be careful when changing script- ENVIRONMENTAL variables.

# Import maintenance functions
source "$(dirname $(readlink -f $0))/maintenance_functions.sh"

####################################
###        User variables        ###
####################################

# Allow archive overwriting
# (Activate only when required due to changed source contents).
OVERWRITE_ARCHIVES=false  # ``true`` or ``false`` (default)

# Paths w.r.t repository root
source_folder="data/processed/SSA/"
archive_base_name="SSA_processed"

##############
#### MAIN ####
##############
CreateFolderArchive $source_folder $archive_base_name

exit $?
