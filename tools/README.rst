============
Helper tools
============
This folder provides tools to improve working experience. Either by automating
tasks or by providing helpful (example) files.

Folder structure
----------------
Organization of the top-level directory ``//tools/``.

==================  ==========================================================
Folder              Contents
==================  ==========================================================
`scripts/`_         Contains (bash) scripts to automate tasks.

                    * In- and exporting project datasets.
                    * TODO: Generate code documentation.

`settings/`_        Collection of files with example settings for diverse
                    software programs.

                    * IDE project management file(s).

``dictionaries/``   Additional spell-checker dictionaries (thesis specific).

==================  ==========================================================

.. Define hyperlinks
.. _`scripts/`: scripts/README.rst
.. _`settings/`: settings/README.rst
