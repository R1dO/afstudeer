==============
Settings files
==============
This folder serves as a container for example software settings files.

Those files are user-specific; hence it is not intended to use those files
directly from this location. Instead copy them to the location [#]_ specified in
the table below. If additional steps are needed those will be listed as well.


Folder contents
---------------
Contents of: ``//tools/settings/``.

===================  =======================================================
Script name          Description
===================  =======================================================
``afstudeer.geany``  Project file for the Geany_ IDE.

                     * Intended location: ``project root``
                     * Specify *working directory* via software GUI [#]_:

                       + Menu: ``Project`` -> ``Properties``
                       + Tab: ``Project``
                       + Setting: ``Base Path``
                     * Includes 2 'build' commands to provide spell checking
                       abilities even if the corresponding plug-in is not
                       available.
===================  =======================================================

.. [#] Intended location respects the convention defined in master README file
       of the project (located at the project root).

.. [#] Needed since it is stored as an absolute path in the setting file.

.. _Geany: http://www.geany.org/
